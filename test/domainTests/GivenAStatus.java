package domainTests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.comp354.domain.Status;

public class GivenAStatus {

	public Status status;
	
	@Before
	public void setUp(){
		status = new Status();
	}
	
	@After
	public void tearDown(){
		status.deleteStatus(status.getId());
	}
	
	@Test
	public void When_I_Create_A_Status_Then_I_Should_Have_The_Right_Data() {
		
		int statusId = status.createStatus("In the process", "Project being created");
		
		status.getStatusFromDB(statusId);
		
		assertEquals("In the process", status.getName());
		assertEquals("Project being created", status.getDescription());		
	}
	
	@Test
	public void When_I_Update_A_Status_Then_I_Should_Have_The_Right_Data() {
		
		int statusId = status.createStatus("In the process", "Project being created");
		
		HashMap newValues = new HashMap();
		
		newValues.put("status_name", "Dancing");
		newValues.put("status_description", "All night");
		
		status.updateStatus(newValues);
		status.getStatusFromDB(statusId);
		
		assertEquals("Dancing", status.getName());
		assertEquals("All night", status.getDescription());	
	}

}
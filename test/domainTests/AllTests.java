package domainTests;

import guiTest.LoginWindowTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import domainTests.GivenAProject;
import domainTests.GivenAStatus;
import domainTests.ProjectTest;
import domainTests.ProjectUnitTest;
import domainTests.TaskUnitTest;
import domainTests.UserTest;

@RunWith(Suite.class)
@SuiteClasses({GivenAProject.class, GivenAStatus.class, ProjectTest.class,
	ProjectUnitTest.class, TaskUnitTest.class, UserTest.class, LoginWindowTest.class})
public class AllTests {

}
 
package domainTests;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.comp354.domain.Project;

public class GivenAProject {

	private Project test;
	
	@Before
	public void setUp() {
		test = new Project();
	}
	
	@Test
	public void When_I_Create_A_Project_Then_I_Should_Have_The_Right_Data(){		
				
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, 2014);
		cal.set(Calendar.MONTH, 5);
		cal.set(Calendar.DAY_OF_MONTH, 10);
		
		Date start = cal.getTime();
		
		Calendar cal2 = new GregorianCalendar();
		
		cal2.set(Calendar.YEAR, 2014);
		cal2.set(Calendar.MONTH, 7);
		cal2.set(Calendar.DAY_OF_MONTH, 10);
		
		Date end = cal2.getTime();		
		
		test = new Project("Flying Machine" , start, end);
		
		assertEquals("Flying Machine", test.getTitle());
		assertEquals(cal.getTime(), test.getstart_Date());
		assertEquals(cal2.getTime(), test.getend_Date());
	}
	
	@Test
	public void When_I_Update_A_Project_Then_I_Should_Have_The_Right_Data(){
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS.SSS");
		
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, 2014);
		cal.set(Calendar.MONTH, 2);
		cal.set(Calendar.DAY_OF_MONTH, 10);
		
		Date start = cal.getTime();
		
		Calendar cal2 = new GregorianCalendar();
		
		cal2.set(Calendar.YEAR, 2014);
		cal2.set(Calendar.MONTH, 3);
		cal2.set(Calendar.DAY_OF_MONTH, 10);
		
		Date end = cal2.getTime();		
		
		test = new Project("Flying Machine" , start, end);
		
		Calendar cal3 = new GregorianCalendar();
		cal3.set(Calendar.YEAR, 2014);
		cal3.set(Calendar.MONTH, 3);
		cal3.set(Calendar.DAY_OF_MONTH, 10);
		
		Calendar cal4 = new GregorianCalendar();
		
		cal4.set(Calendar.YEAR, 2014);
		cal4.set(Calendar.MONTH, 4);
		cal4.set(Calendar.DAY_OF_MONTH, 10);
		
		String time1 = dateFormat.format(cal3.getTime());
		String time2 = dateFormat.format(cal4.getTime());
		
		HashMap<String,String> newValues = new HashMap<String,String>();
		
		newValues.put("title", "Space Machine");
		newValues.put("start_date",time1);
		newValues.put("end_date",time2);
		newValues.put("status_id", "1");
		
		test.updateProject(newValues);
		assertTrue(test.getTitle().equals("Space Machine"));
		assertEquals(time1, dateFormat.format(test.getstart_Date()));
		assertEquals(time2, dateFormat.format(test.getend_Date()));
		assertTrue(test.getstatus_Id() == 1);
	}

	@After
	public void tearDown(){
		test.deleteProject(test.getId());
	}
}
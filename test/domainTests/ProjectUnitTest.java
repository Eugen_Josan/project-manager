/**
 * @author Eugeniu Josan 6632882
 * Tests for project class
 *
 */
package domainTests;

import java.sql.SQLException;
import java.util.Date;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.comp354.database.DbClass;
import ca.concordia.comp354.domain.Project;

public class ProjectUnitTest extends TestCase {

	private DbClass myDB = null;
	private Project myProject = null;
	private Date initialDate = null;
	private Date finalDate = null;
	private int numberOfElements = 0;
	private int userID = 1;
	

	@Before
	protected void setUp() throws Exception {
		myDB = new DbClass();
		initialDate = new Date (2014, 06, 01);
		finalDate = new Date (2014, 06, 30);
		myProject = new Project();

	}
	//implementing delete function from the database
	private void delete_project(int projectId) throws Exception{
		String query = "DELETE FROM projects WHERE id = " + projectId + ";";
		myDB.execute(query);				
		myDB.closeConnection();
	}	
	private void delete_users_projects(int userId) throws Exception{
		String query = "DELETE FROM users_projects WHERE id = " + userId + ";";
		myDB.execute(query);				
		myDB.closeConnection();
	}	
	@Test
	public void test_add_project() throws SQLException {
		numberOfElements = myProject.getProjectList().size();
		myProject.createProject("Test", initialDate, finalDate);
		
		//checking if the project has been added into database
		assertEquals(numberOfElements+1, myProject.getProjectList().size());
	}
	@Test
	public void test_assign_user_to_project(){
		numberOfElements = myProject.getAssignedUsers().size();
		myProject.assignUserToProject(userID);
		
		//checking if the user was assigned to the project
		assertEquals(numberOfElements+1, myProject.getAssignedUsers().size());
		
	}
	@After
	protected void tearDown() throws Exception {
		delete_project(myProject.getId());
		delete_users_projects(userID);
		numberOfElements = 0;
		myDB.closeConnection();
	}
	

}

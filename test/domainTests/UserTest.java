package domainTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.User;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTest {
	private User user;
	private static int idUser1;
	private static int idUser2;
	
	@Before
	public void setUp() throws Exception {
		user = new User();
		user.setName("TestCaseUser aka TCU");
		user.setLogin("TCU");
		user.setPassword("TCU");
		user.setEmail("TCU@tcu.tcu");
		user.setRoleId(1);
	}
	
	@Test
	public void Test_CreateUser1() {
		try {
			idUser1 = UserDAO.createNewUser(user);
			assertNotEquals(idUser1, -1);
			
		}
		catch (Exception e) {
			//don't come here
		}
	}
	
	@Test
	public void Test_CreateUser2() {
		try {
			idUser2 = UserDAO.createNewUser("TestCaseUser aka TCU 2", "TCU2", "TCU2", "TCU2@tcu.org", 1);
			assertNotEquals(idUser2, -1);
			
		}
		catch (Exception e) {
			//don't come here
		}
	}
	
	@Test
	public void Test_GetUserInfo() {
		User u = UserDAO.getUserInfoFromID(idUser1);
		assertEquals(user.getName(), u.getName());
		assertEquals(user.getLogin(), u.getLogin());
		assertEquals(user.getEmail(), u.getEmail());
	}
	
	@Test
	public void Test_GetUserInfo2() {
		User u = UserDAO.getUserInfoFromID(idUser2);
		assertEquals("TestCaseUser aka TCU 2", u.getName());
		assertEquals("TCU2", u.getLogin());
		assertEquals("TCU2@tcu.org", u.getEmail());
	}
	
	@Test
	public void Test_ModifyUserInfoFromId() {
		UserDAO.updateUserFromID(idUser1, "New", "New", "New", 1);
		User u = UserDAO.getUserInfoFromID(idUser1);
		assertEquals("New", u.getName());
		assertEquals("New", u.getEmail());
	}
	
	@Test
	public void Test_ModifyUserInfoFromLogin() {
		UserDAO.updateUserFromLogin("TCU2", "New2", "New2");
		User u = UserDAO.getUserInfoFromID(idUser2);
		assertEquals("New2", u.getName());
		assertEquals("New2", u.getEmail());
	}
	
	@Test
	public void Test_RemoveUserFromId() {
		UserDAO.deleteUserFromID(idUser1);
		User u = UserDAO.getUserInfoFromID(idUser1);
		assertEquals(null, u);
	}
	
	@Test
	public void Test_RemoveUserFromLogin() {
		UserDAO.deleteUserFromLogin("TCU2");
		User u = UserDAO.getUserInfoFromID(idUser2);
		assertEquals(null, u);
	}

}

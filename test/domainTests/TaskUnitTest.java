/**
 * @author Eugeniu Josan 6632882
 * Tests for Task class
 *
 */
package domainTests;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.database.DbClass;
import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.User;

public class TaskUnitTest extends TestCase {

	private DbClass myDB = null;
	private Project myProject = null;
	private Task myTask = null;
	private Task myTask2 = null;
	private Date initialDate = null;
	private Date finalDate = null;
	private int numberOfElements = 0;
	private int userID = 1;
	private User testUser = null;

	@Before
	protected void setUp() throws Exception {
		myDB = new DbClass();
		initialDate = new Date (2014, 06, 01);
		finalDate = new Date (2014, 06, 30);
		myProject = new Project();
		myTask = new Task("Testing the task class", initialDate, 30, 25, 1, 27, 30, 0, 0, 0);
		myTask2 = new Task("Testing 2 the task class", initialDate, 31, 25, 1, 27, 30, 0, 0, 0);

		testUser = new User(1337,"leet", "leet", "leet@email.com", "leet", 1);
	}
	
	//implementing delete function from the database
	private void delete_project(int projectID) throws Exception{
		String query = "DELETE FROM projects WHERE id = " + projectID + ";";
		myDB.execute(query);				
		myDB.closeConnection();
		
	}	
	
	private void delete_task(int taskID) throws Exception{
		String query = "DELETE FROM tasks WHERE id = " + taskID + ";";
		myDB.execute(query);				
		myDB.closeConnection();
		
	}
	
	private void delete_project_tasks(int projectID) throws Exception{
		String query = "DELETE FROM tasks_projects WHERE id_project = " + projectID + ";";
		myDB.execute(query);					
		myDB.closeConnection();
		
	}
	
	private void delete_users_assigned_to_tasks(int userID) throws Exception{
		String query = "DELETE FROM tasks_users WHERE id_user = " + userID + ";";
		myDB.execute(query);					
		myDB.closeConnection();
		
	}
	
	private void delete_parent_relation(int parentID, int childID) throws Exception{
		String query = "DELETE FROM related_tasks WHERE parent_task_id = " + parentID + " AND child_task_id = " 
				+ childID+ ";";
		myDB.execute(query);
		myDB.closeConnection();
	}
	
	
	private boolean check_if_user_exist_in_tasks_users(int userID) throws Exception{
		int count = 0;
		String query = "SELECT COUNT(*)FROM tasks_users WHERE id_user = " + userID + ";";
		try {
			if (myDB.execute(query)){
				ResultSet result = myDB.executeQry(query);
				count = result.getInt(1);
			}
		}catch (Exception e) {
			System.err.print(e.toString());
		}finally{
			this.myDB.closeConnection();
		}
		if (count!=0) return true;
		else return false;
	}
	
	private boolean check_if_related_task_exist(int parentID, int childID){
		
		boolean relatedTasks = false;
		String query = "SELECT * FROM related_tasks WHERE parent_task_id = " + parentID + " AND child_task_id = " 
				+ childID+ ";";
		try {
			if (myDB.execute(query)){
				relatedTasks = myDB.execute(query);
			}
		}catch (Exception e) {
			System.err.print(e.toString());
		}finally{
			this.myDB.closeConnection();
		}
		return relatedTasks;
	}
	
	@Test
	public void test_create_task() throws Exception{
		assertTrue("Assert task exists generated from seup", myTask != null);
	}
	
	@Test
	public void test_assign_task_to_project() throws Exception{
		numberOfElements = Task.getAllTasksByCondition(myProject.getId(), 1).size();
		myProject.createProject("Test", initialDate, finalDate);
		
		//adding 2 tasks to the myProject
		myTask.addTaskToDb(myProject.getId());
		myTask2.addTaskToDb(myProject.getId());
		
		//checking if the tasks were assigned to the project
		assertEquals("The tasks were not assigned to the project", numberOfElements+2, Task.getAllTasksByCondition(myProject.getId(), 1).size());

	}	
	
	@Test
	public void test_assign_task_to_user() throws Exception{
		assertFalse("The task should not yet be assigned", check_if_user_exist_in_tasks_users(testUser.getId()));
		UserDAO.addUserToTask(testUser.getId(), myTask.getId());
		assertTrue("The task should now contain +1 user that is assigned to it.", check_if_user_exist_in_tasks_users(testUser.getId()));
	}
	
	@Test
	public void test_getMembersNotAssignedToTaskWithId_returns_good_data(){
		UserDAO.addUserToTask(testUser.getId(), myTask.getId());
		ArrayList<User> users = UserDAO.getMembersNotAssignedToTaskWithId(myTask.getId());
		assertFalse("users should not contain testUser", users.contains(testUser));
	}
	
	@Test
	public void test_add_parent_relation_to_task(){
		myTask2.addParentTaskRelation(myTask.getId());
		assertTrue("myTask2 should have myTask as parent.", myTask2.getParentTask().contains(myTask.getId()));
	}
	
	@Test
	public void test_endDate_is_right_date(){
		Task testTask = new Task("Testing our sweet endDate function", initialDate, 30, 25, 1, 27, 30, 0, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed Aug 12 12:00:00"));	
	}
	
	//white box
	public void test_when_startdate_falls_on_weekend(){
		Date weekendDate = new Date (2014, 7, 23);
		Task testTask = new Task("Testing when startdate falls on weekend", weekendDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Aug 25 12:00:00"));
	}
	public void test_when_startdate_falls_on_weekday(){
		Date weekdayDate = new Date (2014, 7, 24);
		Task testTask = new Task("Testing when startdate falls on weekday", weekdayDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Aug 25 12:00:00"));
	}
	public void test_when_required_time_is_zero(){
		Date weekdayDate = new Date (2014, 7, 24);
		Task testTask = new Task("Testing when startdate falls on weekday", weekdayDate, 0, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Mon Aug 24 12:00:00"));
	}
	
	//black box
	public void test_endDate_boundary_dateMin_timeRequiredMin(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-01";
		Date minDate = null;
		try {
			minDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate and time required is min", minDate, 0, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Thu Jan 01 12:00:00"));
	}
	
	public void test_endDate_boundary_dateMin_timeRequiredMinPlus(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-01";
		Date minDate = null;
		try {
			minDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is min and time required is min+1", minDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Fri Jan 02 12:00:00"));	
	}
	public void test_endDate_boundary_dateMin_timeRequiredNorm(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-01";
		Date minDate = null;
		try {
			minDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		
		Task testTask = new Task("Testing when startdate is min and time required is norm", minDate, 3, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Jan 06 12:00:00"));	
	}
	public void test_endDate_boundary_dateMin_timeRequiredMaxMinusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-01";
		Date minDate = null;
		try {
			minDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is min and time required is max-1", minDate, 364, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed May 26 12:00:00"));	
	}
	public void test_endDate_boundary_dateMin_timeRequiredMax(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-01";
		Date minDate = null;
		try {
			minDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is min and time required is max", minDate, 365, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);

		assertTrue("Task endDate function should return the right end date.", dateString.equals("Thu May 27 12:00:00"));	
	}
	public void test_endDate_boundary_dateMinPlus_timeRequiredMin(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-02";
		Date minPlusDate = null;
		try {
			minPlusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
	
		Task testTask = new Task("Testing when startdate is min plus one and time required is min", minPlusDate, 0, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Fri Jan 02 12:00:00"));	
	}
	public void test_endDate_boundary_dateMinPlus_timeRequiredMinPlus(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-02";
		Date minPlusDate = null;
		try {
			minPlusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}

		Task testTask = new Task("Testing when startdate is min plus one and time required is min plus one", minPlusDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		//this test fails because start ddate falls on a friday and the end date should be monday, but it's
		//saturday instead
		assertTrue("Task endDate function should return Mon Jan 05 for Friday 1970-01-02 + 1", dateString.equals("Mon Jan 05 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMinPlus_timeRequiredNorm(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-02";
		Date minPlusDate = null;
		try {
			minPlusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is min plus one and time required is min plus one", minPlusDate, 3, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed Jan 07 12:00:00"));	
	}
	public void test_endDate_boundary_dateMinPlus_timeRequiredMaxMinusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-02";
		Date minPlusDate = null;
		try {
			minPlusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is min plus one and time required is max minus one", minPlusDate, 364, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Thu May 27 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMinPlus_timeRequiredMax(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "1970-01-02";
		Date minPlusDate = null;
		try {
			minPlusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is min plus one and time required is min plus one", minPlusDate, 365, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Fri May 28 12:00:00"));	
	}
	
	public void test_endDate_boundary_datNorm_timeRequiredMinPlus(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2014-08-26";
		Date normDate = null;
		try {
			normDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Norm time required is min plus one", normDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed Aug 27 12:00:00"));	
	}
	
	public void test_endDate_boundary_datNorm_timeRequiredNorm(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2014-08-26";
		Date normDate = null;
		try {
			normDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Norm time required is NOrm", normDate, 3, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Fri Aug 29 12:00:00"));	
	}
	
	public void test_endDate_boundary_datNorm_timeRequiredMaxMinusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2014-08-26";
		Date normDate = null;
		try {
			normDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Norm time required is max minus one", normDate, 364, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		//this test fails because start ddate falls on a friday and the end date should be monday, but it's
		//saturday instead
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Mon Jan 18 12:00:00"));	
	}
	
	public void test_endDate_boundary_datNorm_timeRequiredMax(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2014-08-26";
		Date normDate = null;
		try {
			normDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Norm time required is max minus one", normDate, 365, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Jan 19 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMaxMinusOne_timeRequiredMin(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-19";
		Date maxMinusDate = null;
		try {
			maxMinusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max-1 time required is min", maxMinusDate, 0, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Jan 19 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMaxMinusOne_timeRequiredMinPlusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-19";
		Date maxMinusDate = null;
		try {
			maxMinusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max-1 time required is min+1", maxMinusDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed Jan 20 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMaxMinusOne_timeRequiredNorm(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-19";
		Date maxMinusDate = null;
		try {
			maxMinusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max-1 time required is norm", maxMinusDate, 3, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Fri Jan 22 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMaxMinusOne_timeRequiredMaxMinusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-19";
		Date maxMinusDate = null;
		try {
			maxMinusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max-1 time required is max-1", maxMinusDate,364, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		//this test fails because start ddate falls on a friday and the end date should be monday, but it's
		//saturday instead
		assertTrue("Task endDate function should return the right end date as Mon Jun 13.", dateString.equals("Mon Jun 13 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMaxMinusOne_timeRequiredMax(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-19";
		Date maxMinusDate = null;
		try {
			maxMinusDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max-1 time required is max", maxMinusDate, 365, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Jun 14 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMax_timeRequiredMin(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-20";
		Date maxDate = null;
		try {
			maxDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max time required is min", maxDate, 0, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed Jan 20 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMax_timeRequiredMinPlusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-20";
		Date maxDate = null;
		try {
			maxDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max-1 time required is max", maxDate, 1, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Thu Jan 21 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMax_timeRequiredNorm(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-20";
		Date maxDate = null;
		try {
			maxDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max time required is norm", maxDate, 3, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		//this test fails because start ddate falls on a friday and the end date should be monday, but it's
		//saturday instead
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Mon Jan 25 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMax_timeRequiredMaxMinusOne(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-20";
		Date maxDate = null;
		try {
			maxDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max time required is max-1", maxDate, 364, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Tue Jun 14 12:00:00"));	
	}
	
	public void test_endDate_boundary_dateMax_timeRequiredMax(){
		SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateAsString = "2038-01-20";
		Date maxDate = null;
		try {
			maxDate = textFormat.parse(dateAsString);
		} catch (ParseException e) {}
		Task testTask = new Task("Testing when startdate is Max time required is max", maxDate, 365, 0, 1, 1, 1, 1, 0, 0);
		Date theEndDate = testTask.endDate(testTask.getStart_Date(), testTask.getTime_Required());
		SimpleDateFormat expectedDateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss");
		String dateString = expectedDateFormat.format(theEndDate);
		assertTrue("Task endDate function should return the right end date.", dateString.equals("Wed Jun 15 12:00:00"));	
	}
	


	
	@After
	protected void tearDown() throws Exception {
		delete_users_assigned_to_tasks(UserDAO.getUserID("admin", myDB));
		delete_users_assigned_to_tasks(testUser.getId());
		delete_parent_relation(myTask.getId(), myTask2.getId());
		delete_project_tasks(myProject.getId());
		delete_task(myTask.getId());
		delete_task(myTask2.getId());
		delete_project(myProject.getId());

		numberOfElements = 0;
		myDB.closeConnection();
		
	}
	
}

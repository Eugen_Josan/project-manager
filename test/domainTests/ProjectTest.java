package domainTests;

import static org.junit.Assert.*;
import java.util.Date;
import ca.concordia.comp354.domain.*;

import org.junit.Test;
import junit.framework.TestCase;

public class ProjectTest extends TestCase {

	
	private Project testProject;
	
	@Override
	public void setUp() {
		
		Date startDate =  new Date();
		String title =  "Test Project";
		Date endDate = new Date(startDate.getTime() + 10000000);
		testProject =  new Project(title, startDate, endDate);
		
	}
	
	@Test
	public void testProject() {
		assertNotNull(testProject);
	}
	
	@Override
	public void tearDown(){
		testProject =  null;
	}

	

}

/**
 * @author Eugeniu Josan 6632882
 * Tests for login screen
 *
 */
package guiTest;

import java.util.List;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.comp354.*;
import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.database.DbClass;
import ca.concordia.comp354.domain.User;
import ca.concordia.comp354.projectmanager.ui.FrameMaster;
import ca.concordia.comp354.projectmanager.ui.LoginWindow;
import ca.concordia.comp354.projectmanager.ui.ProjectWindow;
import junit.extensions.jfcunit.*;
import junit.extensions.jfcunit.finder.*;
import junit.extensions.jfcunit.eventdata.*;

public class LoginWindowTest extends JFCTestCase{
	
	private FrameMaster frameScreen = null;
	
	private LoginWindow loginScreen = null;
	private ProjectWindow projectScreen = null;
	
	private JButton ok = null;
	private JButton helpButton = null;
	private JTextField username = null;
	private JPasswordField password = null;
	private NamedComponentFinder finder = null;
	
	private DbClass myDB = null;
	
	/**
	 * @throws java.lang.Exception
	 * will set up the basic tools before each test
	 */
	@Before
	protected void setUp() throws Exception {

		setHelper (new JFCTestHelper()); //AWT Event Queue
		//setHelper (new RobotTestHelper()); //uses the OS Event Queue
		 
		frameScreen = new FrameMaster();
		frameScreen.setVisible(true);
		
		loginScreen = new LoginWindow(frameScreen);
		loginScreen.setVisible(true);
		
		finder = new NamedComponentFinder(JComponent.class, "loginMenu");
		loginScreen = (LoginWindow) finder.find(frameScreen, 0);		
			
		projectScreen = new ProjectWindow(frameScreen);
		projectScreen.setVisible(true);

		flushAWT();
	}

	/**
	 * @throws java.lang.Exception
	 * will reset the tools after each test
	 */
	@After
	protected void tearDown() throws Exception {
		
		frameScreen.setVisible(false);
		loginScreen.setVisible(false);
		projectScreen.setVisible(false);
		
		frameScreen.dispose();
		
		loginScreen = null;
		projectScreen = null;

		getHelper();
		TestHelper.cleanUp(this);

    	flushAWT();
    	
        super.tearDown();
	}
	/**
	 * Testing the existence of the OK and helpButton and the 2 fields: username and password
	 */
	@Test
	public void testLoginComponents(){
		
		finder.setName("OK");
    	ok = (JButton) finder.find(loginScreen, 0);
    	assertNotNull("Could not find the OK button", ok);
		
    	finder.setName("login");
    	username = (JTextField ) finder.find( loginScreen, 0);
    	assertNotNull("Could not find the user name field", username);
        assertEquals( "The user name text is not cleared", "", username.getText());
		
		finder.setName("password");
		password = (JPasswordField) finder.find(loginScreen, 0); 
		assertNotNull ("Could not find the password field", password);
		assertEquals ("The password field is not cleard","", password.getText());
	}
	
	@Test
	public void test_fail_login_process_for_empty_password_field() throws Exception{

	    finder.setName("OK");
    	ok = (JButton) finder.find(loginScreen, 0);       	
    	
    	finder.setName("login");
    	username = (JTextField ) finder.find(loginScreen, 0);
		
		finder.setName("password");
		password = (JPasswordField) finder.find(loginScreen, 0); 
		
	    getHelper().sendString(new StringEventData(this, username, "admin"));
		getHelper().sendString(new StringEventData(this, password, ""));
		
	    getHelper().enterClickAndLeave(new MouseEventData(this, ok));
	     	    
	    List showingDialogs = new DialogFinder(null).findAll();
    	JDialog jd = (JDialog)showingDialogs.get(0);
    	
    	assertEquals("Could not get message from Pop-up", "Invalid User!", getHelper().getMessageFromJDialog(jd));
    	
    	Finder buttonFinder = new ComponentFinder(JButton.class);
    	JButton jb = (JButton)buttonFinder.find(jd, 0);
    	assertNotNull(jb);
    	getHelper().enterClickAndLeave(new MouseEventData(this, jb));
    	//jd.dispose();
	    
	}	
	@Test
	public void test_fail_login_process_for_empty_username_field(){
	    
	    finder.setName("OK");
    	ok = (JButton) finder.find(loginScreen, 0);       	
    	
    	finder.setName("login");
    	username = (JTextField ) finder.find(loginScreen, 0);
		
		finder.setName("password");
		password = (JPasswordField) finder.find(loginScreen, 0); 
		
	    getHelper().sendString(new StringEventData(this, username, ""));
		getHelper().sendString(new StringEventData(this, password, "admin"));
		
	    getHelper().enterClickAndLeave(new MouseEventData(this, ok));
	     	    
	    List showingDialogs = new DialogFinder(null).findAll();
    	JDialog jd = (JDialog)showingDialogs.get(0);
    	
    	assertEquals("Could not get message from Pop-up", "Invalid User!", getHelper().getMessageFromJDialog(jd));
    	
    	Finder buttonFinder = new ComponentFinder(JButton.class);
    	JButton jb = (JButton)buttonFinder.find(jd, 0);
    	assertNotNull(jb);
    	getHelper().enterClickAndLeave(new MouseEventData(this, jb));
    	//jd.dispose();
	    
	}	
	@Test
	public void test_fail_login_process_for_empty_fields(){
	    
	    finder.setName("OK");
    	ok = (JButton) finder.find(loginScreen, 0);       	
    	
    	finder.setName("login");
    	username = (JTextField ) finder.find(loginScreen, 0);
		
		finder.setName("password");
		password = (JPasswordField) finder.find(loginScreen, 0); 
		
	    getHelper().sendString(new StringEventData(this, username, ""));
		getHelper().sendString(new StringEventData(this, password, ""));
		
	    getHelper().enterClickAndLeave(new MouseEventData(this, ok));
	     	    
	    List showingDialogs = new DialogFinder(null).findAll();
    	JDialog jd = (JDialog)showingDialogs.get(0);
    	
    	assertEquals("Could not get message from Pop-up", "Invalid User!", getHelper().getMessageFromJDialog(jd));
    	
    	Finder buttonFinder = new ComponentFinder(JButton.class);
    	JButton jb = (JButton)buttonFinder.find(jd, 0);
    	assertNotNull(jb);
    	getHelper().enterClickAndLeave(new MouseEventData(this, jb));
    	//jd.dispose();
	    
	}
	@Test
	public void test_fail_login_process_with_wrong_data(){
	    
		try {
			this.myDB = new DbClass();
		} catch (Exception e) {
		} finally {
			myDB.closeConnection();
		}
		
	    finder.setName("OK");
    	ok = (JButton) finder.find(loginScreen, 0);       	
    	
    	finder.setName("login");
    	username = (JTextField ) finder.find(loginScreen, 0);
		
		finder.setName("password");
		password = (JPasswordField) finder.find(loginScreen, 0); 
		
	    getHelper().sendString(new StringEventData(this, username, "test"));
		getHelper().sendString(new StringEventData(this, password, "test"));
		
	    getHelper().enterClickAndLeave(new MouseEventData(this, ok));
	     	    
	    List showingDialogs = new DialogFinder(null).findAll();
    	JDialog jd = (JDialog)showingDialogs.get(0);
    	
    	assertEquals("Could not get message from Pop-up", "Invalid User!", getHelper().getMessageFromJDialog(jd));
    	
    	Finder buttonFinder = new ComponentFinder(JButton.class);
    	JButton jb = (JButton)buttonFinder.find(jd, 0);
    	assertNotNull(jb);
    	getHelper().enterClickAndLeave(new MouseEventData(this, jb));
    	//jd.dispose();
	    
	}

}


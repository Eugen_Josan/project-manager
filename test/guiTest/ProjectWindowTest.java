/**
 * @author Eugeniu Josan 6632882
 * Tests for project screen
 *
 */
package guiTest;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JToggleButton;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.comp354.projectmanager.ui.FrameMaster;
import ca.concordia.comp354.projectmanager.ui.LoginWindow;
import ca.concordia.comp354.projectmanager.ui.ProjectWindow;
import ca.concordia.comp354.projectmanager.ui.TasksWindow;
import junit.extensions.jfcunit.*;
import junit.extensions.jfcunit.finder.*;
import junit.extensions.jfcunit.eventdata.*;


public class ProjectWindowTest extends JFCTestCase{
	
	private FrameMaster frameScreen = null;
	private LoginWindow loginScreen = null;
	private ProjectWindow projectScreen = null;
	private TasksWindow tasksScreen = null;	
	
	private JPanel pnl_Container = null;
	private JButton task = null;
	private JToggleButton logOut = null;
	private JTable tableProject = null;
	
	private JMenuBar menuBar = null;
	private JMenu menu = null;
	private JMenuItem menuItem = null;
	
	private NamedComponentFinder finder = null;
	
	/**
	 * @throws java.lang.Exception
	 * will set up the basic tools before each test
	 */
	@Before
	protected void setUp() throws Exception {

		setHelper (new JFCTestHelper()); //AWT Event Queue
		//setHelper (new RobotTestHelper()); //uses the OS Event Queue
		 
		frameScreen = new FrameMaster();
		projectScreen = new ProjectWindow(frameScreen);
		tasksScreen = new TasksWindow(frameScreen);
		loginScreen = new LoginWindow(frameScreen);
//		frameScreen.createMenu(FrameMaster.USER);
		frameScreen.showProjectWindow();
		
		flushAWT();
	}

	/**
	 * @throws java.lang.Exception
	 * will reset the tools after each test
	 */
	@After
	protected void tearDown() throws Exception {
		
		frameScreen.setVisible(false);
		loginScreen.setVisible(false);
		projectScreen.setVisible(false);
		tasksScreen.setVisible(false);
		
		frameScreen.dispose();
		
		loginScreen = null;
		projectScreen = null;
		tasksScreen = null;

		getHelper();
		TestHelper.cleanUp(this);

    	flushAWT();
        super.tearDown();
	}
	/**
	 * Testing the existence of the Task and log off buttons
	 */
	@Test
	public void NotWorkingTestButtons(){
		
		finder = new NamedComponentFinder(JComponent.class, "projectMenu");
		projectScreen = (ProjectWindow) finder.find(frameScreen, 0);	
		
		finder.setName("Tasks");
    	task = (JButton) finder.find(projectScreen, 0);
    	assertNotNull("Could not find the Tasks button", task);
		
    	//Deprecated temporarily
    	/*finder.setName("Log out");
    	logOut = (JToggleButton) finder.find(projectScreen, 0);
    	assertNotNull("Could not find the log out button", logOut);	*/
    	
    	finder.setName("Table Project");
    	tableProject = (JTable ) finder.find(projectScreen, 0);
    	assertNotNull("Could not find the Table Project", tableProject);
	}
	
	/**
	 * Testing the log out process, deprecated as log out button was removed
	 * 
	 * Deprecated temporarily!!!
	 * 
	 * This test is skipped
	 */
	@Test
	public void NotWorkingtestLogOutProcess(){

		finder = new NamedComponentFinder(JComponent.class, "projectMenu");
		projectScreen = (ProjectWindow) finder.find(frameScreen, 0);	

	    finder.setName("Log out");
    	logOut = (JToggleButton) finder.find(projectScreen, 0);       	
    		    
	    getHelper().enterClickAndLeave(new MouseEventData(this, logOut));
	    
		finder = new NamedComponentFinder(JComponent.class, "loginMenu");
		loginScreen = (LoginWindow) finder.find(frameScreen, 0);	
	      
	    assertTrue("Login screen is not showing still", loginScreen.isShowing( ));
	    assertNotNull ("Could not find the login frame", loginScreen);	 
	}
	
	/**
	 * Testing the action from pressing the Tasks button
	 * 
	 * Deprecated Task button removed!!!
	 * 
	 * This test is skipped
	 */
	@Test
	public void NotWorkingTestTasksButtonProcess() {
		
		finder = new NamedComponentFinder(JComponent.class, "projectMenu");
		projectScreen = (ProjectWindow) finder.find(frameScreen, 0);
		
		finder.setName("Tasks");
		task = (JButton) finder.find(projectScreen, 0);
		
		getHelper().enterClickAndLeave(new MouseEventData(this, task));
		
		assertTrue("Task screen is not visible", tasksScreen.isVisible());		
		
		assertTrue("Project screen is showing still", !projectScreen.isShowing());		
	}	

	
	@Test
	public void notWorkingtestMenuBar(){
		finder = new NamedComponentFinder(JComponent.class, "menuBar");
		menuBar = (JMenuBar) finder.find(frameScreen, 0);
		assertNotNull( "Could not find menuBar!", menuBar);
		
		finder.setName("Project");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Project!", menu);
		
		finder.setName("Tasks");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Tasks!", menu);
		
		finder.setName("View");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find View!", menu);
		
		finder.setName("Users");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Users!", menu);
		
		finder.setName("Help");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Help!", menu);
	}
	@Test
	public void testProjectMenu(){
		
		finder = new NamedComponentFinder(JComponent.class, "Project");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Project!", menu);
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("View Projects");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find New!", menuItem);
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		//some action must be performed
		//TODO: test for that action, when it will be implemented	
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_V, ActionEvent.CTRL_MASK, 5));
		//same action as above must be performed
		//TODO: test for that action, when it will be implemented
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		
		/*finder.setName("Generate Gant Chart");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Generate Gant Chart!", menuItem);
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_G, ActionEvent.CTRL_MASK, 5));
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("Earned Value Analysis");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Earned Value Analysis!", menuItem);
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_E, ActionEvent.CTRL_MASK, 5));
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("PERT Analysis");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find PERT Analysis!", menuItem);
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_P, ActionEvent.CTRL_MASK, 5));
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("Critical Path Analysis");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Critical Path Analysis!", menuItem);
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_C, ActionEvent.CTRL_MASK, 5));*/
	}
	
	@Test
	public void testTasksMenu(){
		finder = new NamedComponentFinder(JComponent.class, "Tasks");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Tasks!", menu);
		
		/*getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("Create Task");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Create Tasks!", menuItem);	
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_T, ActionEvent.CTRL_MASK, 5));*/
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		
		finder.setName("View Tasks");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		
		assertNotNull( "Could not find Tasklist!", menuItem);	
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
	
		
		
	//	getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_T, ActionEvent.CTRL_MASK, 5));
	}
	@Test
	public void notWorkingtestViewMenu(){
		finder = new NamedComponentFinder(JComponent.class, "View");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find View!", menu);
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("Gant Chart");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Gant Chart!", menuItem);	
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_G, ActionEvent.CTRL_MASK, 5));
	}
	@Test
	public void notWorkingtestUsersMenu(){
		finder = new NamedComponentFinder(JComponent.class, "Users");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Users!", menu);
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("Add");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Add!", menuItem);	
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_A, ActionEvent.CTRL_MASK, 5));
	}
	@Test
	public void testHelpMenu(){
		finder = new NamedComponentFinder(JComponent.class, "Help");
		menu = (JMenu) finder.find(frameScreen, 0);
		assertNotNull( "Could not find Help!", menu);
		
		getHelper().enterClickAndLeave(new MouseEventData(this, menu));
		finder.setName("About Us");
		menuItem = (JMenuItem) finder.find(frameScreen, 0);
		assertNotNull( "Could not find About Us!", menuItem);	
		getHelper().enterClickAndLeave(new MouseEventData(this, menuItem));
		getHelper().sendKeyAction( new KeyEventData(this, menuItem, KeyEvent.VK_A, ActionEvent.CTRL_MASK, 5));
	}
}


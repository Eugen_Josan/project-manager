package EarnedValue;

import org.junit.Test;

import junit.framework.TestCase;
import static org.junit.Assert.*;
import ca.concordia.comp354.domain.*;

import java.util.ArrayList;
import java.util.Date;

/**
 * This class is a collection of unit tests for the EVAnalysis class that were implemented using JUnit. The test cases cover all of the basis paths
 * for each method in the class.
 * @author Sebastien Charbonneau (6427286)
 */
public class EVAnalysisTests extends TestCase {

	EVAnalysis testEVAnalysis; 
	ArrayList<Task> testList;
	Task task1, task2, task3, task4, task5;
	
	private void createTestObjects() {
		testList = new ArrayList<Task>();
		task1 = new Task(1, "Metal Man Stage", null, null, 1, 1, 3, 0, 0, 0, 0, null, 100, 200); 
		task2 = new Task(2, "Bubble Man Stage", null, null, 3, 1, 2, 0, 0, 0, 0, null, 300, 200); 
		task3 = new Task(3, "Wood Man Stage", null, null, 2, 2, 3, 0, 0, 0, 0, null, 250, 200); 
		task4 = new Task(4, "Quick Man Stage", null, null, 2, 0, 1, 0, 0, 0, 0, null, 150, 0); 
		task5 = new Task(5, "Air Man Stage", null, null, 0, 1, 1, 0, 0, 0, 0, null, 200, 0); 
	}
	
	/**
	 * First test case for the calcBAC method: The list of Tasks is empty. This unit test verifies that the method runs correctly when the 
	 * for loop's while statement is initially FALSE.
	 */
	@Test
	public void testCalcBAC_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.getBAC(), 0.0f); // if the list has no Tasks, the calculated BAC should be 0
	}
	
	/**
	 * Second test case for the calcBAC method: The list of Tasks is not empty. This unit test verifies that the method runs correctly when used
	 * in a typical situation, where the for loop's while statement is initially TRUE.
	 */
	@Test
	public void testCalcBAC_typicalList() {
		createTestObjects();
		testList.add(task1);
		testList.add(task2);
		testList.add(task3); 
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.getBAC(), 1000.0f); // the sum of the planned values of the test Tasks created is 1000
	}
	
	/**
	 * First test case for the calcPSC method: The list of Tasks is empty. This unit test verifies that the method runs correctly when the 
	 * for loop's while statement is initially FALSE.
	 */
	@Test
	public void testCalcPSC_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcPSC(), 0.0f); // if the list has no Tasks, the calculated PSC should be 0
	}
	
	/**
	 * Second test case for the calcPSC method: The list contains Tasks, but none of them are in the completed status. 
	 * This unit test verifies that the method runs correctly when the if statement is always FALSE.
	 */
	@Test
	public void testCalcPSC_noCompletedTasks() {
		createTestObjects();
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcPSC(), 0.0f); // if none of the Tasks are in the completed status, the calculated PSC should be 0
	}
	
	/**
	 * Third test case for the calcPSC method: The list contains Tasks and some of them are in the completed status.
	 * This unit test verifies that the method runs correctly when the for loop's while statement and the if statement are TRUE. 
	 */
	@Test
	public void testCalcPSC_typicalList() {
		createTestObjects();
		testList.add(task1); // this Task is in the completed status and has a planned value of 100
		testList.add(task2); 
		testList.add(task3); // this Task is in the completed status and has a planned value of 250
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcPSC(), 0.35f); // the PV of the completed Tasks is 400, so the calculated PSC should be 0.35 (35%)
	}
	
	/**
	 * First test case for the calcEV method: The list of Tasks is empty. This unit test verifies that the method runs correctly when
	 * the for loop's while statement is initially FASLE.
	 */
	@Test
	public void testCalcEV_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.getEarnedValue(), 0.0f); // if the list has no Tasks the Project's earned value should be 0
	}
	
	/**
	 * Second test case for the calcEV method: The list contains Tasks, but one or more of the Task's time required variables is missing or
	 * is incorrectly set to 0. This unit test verifies that the method runs correctly when the if statement that handles this exception
	 * (it would otherwise trigger a division by zero error or Nan value) is TRUE.
	 */
	@Test
	public void testCalcEV_missingTimeRequired() {
		createTestObjects();
		testList.add(task2); // this Task has been partially completed (1 of 3, planned value of 300)
		testList.add(task3); // this Task has been completed (2 of 2, planned value of 250)
		testList.add(task5); // this Task's time_Required was set to 0 and its time_Spent to 1
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.getEarnedValue(), 350.0f); // the Project's earned value should be 350 (1/3rd of 300, plus 250, plus 0)
	}
	
	/**
	 * Third test case for the calcEV method: This list contains Tasks that are at various levels of completion (completed, partially completed
	 * and not started). This unit test verifies that the method runs correctly when used in a normal situation, where the for loop's while
	 * statement is initially TRUE and the if statement is always FALSE.
	 */
	@Test
	public void testCalcEV_typicalList() {
		createTestObjects();
		testList.add(task1); // this Task has been completed (1 of 1, planned value of 100)
		testList.add(task2); // this Task has been partially completed (1 of 3, planned value of 300)
		testList.add(task3); // this Task has been completed (2 of 2, planned value of 250)
		testList.add(task4); // this Task has not been started (0 of 2)
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.getEarnedValue(), 450.0f); // the Project's earned value should be 450 (100, plus 1/3rd of 300, plus 250, plus 0)
	}
	
	/**
	 * First test case for the calcPercentComplete method: The Project's budget at completion is 0 due to it having no Tasks or due to its 
	 * Tasks having not been assigned planned values. This unit test verifies that the method runs correctly when the if statement that 
	 * handles this exception is TRUE.
	 */
	@Test 
	public void testCalcPercentComplete_zeroBAC() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList); // with an empty Task list, the Project's BAC is 0 
		assertEquals(testEVAnalysis.calcPercentComplete(), 0.0f); // if the Project's BAC is 0, the method should return 0
	}
	
	/**
	 * Second test case for the calcPercentComplete method: The Project has a budget at completion value and has Tasks with various 
	 * earned values. This unit test verifies that the method runs correctly when used in a normal situation, where the if statement is FALSE.
	 */
	@Test
	public void testCalcPercentComplete_nonzeroBAC() {
		createTestObjects();
		testList.add(task1);
		testList.add(task2);
		testList.add(task3);
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList); // this Task list has an earned value of 450 and a budget at completion of 1000
		assertEquals(testEVAnalysis.calcPercentComplete(), 0.45f); // the Project's percent complete should be 45%
	}
	
	/**
	 * First test case for the calcCV method: The Project's list of Tasks is empty. This unit test verifies that the method runs correctly
	 * when the for loop's while statement is initially FALSE.
	 */
	@Test
	public void testCalcCV_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcCV(), 0.0f); // with an empty Task list, the Project's CV should be 0
	}
	
	/**
	 * Second test case for the calcCV method: The Project has an earned value and a list of Tasks with various actual cost values. This unit
	 * test verifies that the method runs correctly when used in a normal situation, where the for loop's while statement is initially TRUE.  
	 */
	@Test
	public void testCalcCV_typicalList() {
		createTestObjects();
		testList.add(task1); // this completed Task has an earned value of 100 and an actual cost of 200
		testList.add(task2); // this partially completed Task has an earned value of 100 and an actual cost of 200
		testList.add(task3); // this completed Task has an earned value of 250 and an actual cost of 200
		testList.add(task4); // this pending Task has an earned value of 0 and an actual cost of 0
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcCV(), -150.0f); // the Project's CV should be -150 (450 - 600)
	}
	
	/**
	 * First test case for the calcSV method: The Project's list of Tasks is empty. This unit test verifies that the method runs correctly
	 * when the for loop<s while statement is initially FALSE.
	 */
	@Test
	public void testCalcSV_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcSV(), 0.0f); // with an empty Task list, the Project's cost variance should be 0
	}
	
	/**
	 * Second test case for the calcSV method: The Project has Tasks, but none of them have been started. 
	 * This unit test verifies that the method runs correctly when the for loop's while statement is TRUE and the if statement is always FALSE.
	 */
	@Test
	public void testCalcSV_noStartedTasks() {
		createTestObjects();
		testList.add(task4);
		testList.add(task5); 
		testEVAnalysis = new EVAnalysis(testList); // both of the Project's Tasks have not been started (status at 1)
		assertEquals(testEVAnalysis.calcSV(), 0.0f);
	}
	
	/**
	 * Third test case for the calcSV method: The Project has Tasks and some of them are in progress or completed. This unit test verifies
	 * that the method runs correctly when the for loop's while statement is TRUE and the if statement is TRUE.
	 */
	@Test
	public void testCalcSV_typicalList() {
		createTestObjects();
		testList.add(task1); // this Task is completed and has a planned value of 100
		testList.add(task2); // this Task is in progress and has a planned value of 300
		testList.add(task3); // this Task is completed and has a planned value of 250
		testList.add(task4); // this Task has not been started.
		testList.add(task5); // this Task has not been started
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcSV(), -200.0f); // the Project's SV should be -200 (earned value of 450 less 650)
	}
	
	/**
	 * First test case for the calcCPI method: The Project's list of Tasks is empty.
	 * This unit test verifies that the method runs correctly when the for loop's while statement is initially FALSE and the if statement is TRUE.
	 */
	@Test
	public void testCalcCPI_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.getCPI(), 0.0f); // with an empty Task list, the Project's cost performance index should be 0
	}
	
	/**
	 * Second test case for the calcCPI method: The Project has Tasks, but none of them have been assigned actual cost values.
	 * This unit test verifies that the method runs correctly when the loop's while statement is TRUE, but the if statement is TRUE.
	 */
	@Test
	public void testCalcCPI_noActualValues() {
		createTestObjects();
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList); // neither of the Tasks added to the list have an actual cost value
		assertEquals(testEVAnalysis.getCPI(), 0.0f);
	}
	
	/**
	 * Third test case for the calcCPI method: The Project has Tasks and one of more of them have been assigned actual cost values.
	 * This unit test verifies that the method runs correctly when used in a normal situation, when the for loop's while statement is TRUE
	 * and the if statement is FALSE.
	 */
	@Test
	public void testCalcCPI_typicalList() {
		createTestObjects();
		testList.add(task1); // this Task has an actual cost of 200
		testList.add(task2); // this Task has an actual cost of 200
		testList.add(task3); // this Task has an actual cost of 200
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList); 
		assertEquals(testEVAnalysis.getCPI(), 0.75f); // the Project's CPI should be 0.75 (earned value of 450 divided by AC of 600)
	}
	
	/**
	 * First test case for the calcSPI method: The Project's list of Tasks is empty.
	 * This unit test verifies that the method runs correctly when the for loop's while statement is initially FALSE and the if statement is TRUE.
	 */
	@Test
	public void testCalcSPI_emptyList() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcSPI(), 0.0f); // with an empty Task list, the Project's schedule performance index should be 0
	}
	
	/**
	 * Second test case for the calcSPI method: The Project has Tasks, but none of them have been started (status greater than 1).
	 * This unit test verifies that the method runs correctly when the for loop's while statement is TRUE, the first if statement 
	 * is always FALSE and the second if statement is TRUE.
	 */
	@Test
	public void testCalcSPI_noStartedTasks() {
		createTestObjects();
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList); // both of the Project's Tasks have a status of 1
		assertEquals(testEVAnalysis.calcSPI(), 0.0f); // if the Project has no started Tasks, the schedule performance index should be 0
	}
	
	/**
	 * Third test case for the calcSPI method: The Project has Tasks and at least one of them is in progress or completed.
	 * This unit test verifies that the method runs correctly when used in a normal situation, where the for loop's while statement and the
	 * first if statement are TRUE and the second if statement is FALSE. 
	 */
	@Test
	public void testCalcSPI_typicalList() {
		createTestObjects();
		testList.add(task1); // this Task has a planned value of 100 and has been started
		testList.add(task2); // this Task has a planned value of 300 and has been started
		testList.add(task3); // this Task has a planned value of 250 and has been started
		testList.add(task4); // this Task has not been started
		testList.add(task5); // this Task has not been started
		testEVAnalysis = new EVAnalysis(testList);
		assertEquals(testEVAnalysis.calcSPI(), 0.6923077f); // the Project's schedule performance index should be 450 / 650
	}
	
	/**
	 * First test case for the calcEAC method: The Project's cost performance index (CPI) is 0 due to none of its Tasks having started or
	 * none of its Tasks having been assigned actual costs. This unit test verifies that the method runs correctly when the if statement that 
	 * handles this exception is TRUE.
	 */
	@Test 
	public void testCalcEAC_zeroCPI() {
		createTestObjects();
		testEVAnalysis = new EVAnalysis(testList); // with an empty Task list, the Project's CPI is 0 
		assertEquals(testEVAnalysis.calcEAC(), 0.0f); // if the Project's CPI is 0, the method should return 0
	}
	
	/**
	 * Second test case for the calcEAC method: The Project has a cost performance index value, as well as a budget at completion value. 
	 * This unit test verifies that the method runs correctly when used in a normal situation, where the if statement is FALSE.
	 */
	@Test
	public void testCalcEAC_nonzeroCPI() {
		createTestObjects();
		testList.add(task1);
		testList.add(task2);
		testList.add(task3);
		testList.add(task4);
		testList.add(task5);
		testEVAnalysis = new EVAnalysis(testList); // this Task list has a budget at completion of 1000 and a cost performance index of 0.75 
		assertEquals(testEVAnalysis.calcEAC(), 1333.3333333f); // the Project's estimate at completion should be 1000 / 0.75
	}
}


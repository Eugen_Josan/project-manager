package EarnedValue;

import java.util.ArrayList;
import ca.concordia.comp354.domain.Task;

public class EVAnalysis {
	private ArrayList<Task> tasks;
	private int projectID;
	private final int key = 1;
	private float bac;
	private float earnedValue;
	private float percentScheduledCompletion;
	private float percentComplete;
	private float cVar;
	private float sVar;
	private float cpi;
	private float spi;
	private float eac;
	
	public EVAnalysis(int project)
	{
		this.projectID = project;
		tasks = Task.getAllTasksByCondition(projectID, key);
		bac = calcBAC();
		earnedValue = calcEV();
		percentScheduledCompletion = calcPSC();
		percentComplete = calcPercentComplete();
		cVar = calcCV();
		sVar = calcSV();
		cpi = calcCPI();
		spi = calcSPI();
		eac = calcEAC();
	}
	
	/**
	 * Secondary constructor used to facilitate unit testing by removing the need to populate the database with test data.
	 * @param listOfTasks An ArrayList containing the Project's Tasks
	 */
	protected EVAnalysis(ArrayList<Task> listOfTasks) {
		tasks = listOfTasks;
		bac = calcBAC();
		earnedValue = calcEV();
		cpi = calcCPI();
	}
	
	/**
	 * Calculates and returns the Project's budget at completion (BAC), which is a summation of the planned values (PV) for all of its Tasks.
	 * @return The Project's budget at completion (a float)
	 */
	protected float calcBAC()
	{
		float sum = 0;
		for(int i = 0; i < tasks.size(); i++){ // for each Task in the Project
			sum += tasks.get(i).getPV();
		}
		return sum;
	}
	
	
	/**
	 * Calculates and returns the Project's percent scheduled for completion (PSC), which is the percentage of a Project's budget at completion
	 * that has been completed.
	 * @return the Project's percent scheduled for completion (a float)
	 */
	protected float calcPSC()
	{
		float sum = 0;
		for(int i = 0; i < tasks.size(); i++){ // for each Task in the Project
			if (tasks.get(i).getStatus_Id() == 3) { // if the Task's status is 3 (completed)
				sum += tasks.get(i).getPV(); // the completed Task's planned value is added to the running total
			}	
		}
		if (bac == 0) { // if the Project is empty or planned values have not been assigned to the Tasks, the BAC will be 0
			return 0; // in this case the method simply returns 0 in order to avoid dividing by 0
		} else {
			return (sum/bac);
		}	
	}
	
	/**
	 * Calculates and returns the Project's earned value (EV), which is the amount of a Project's planned value that has been completed,
	 * taking into account both completed and partially completed Tasks. The EV for partially completed Tasks is determined using the 
	 * ratio of time spent on the Task vs. time required to complete it. 
	 * @return the Project's earned value (a float)
	 */
	protected float calcEV()
	{
		float sum = 0;
		for (int i = 0; i < tasks.size(); i++) {
			if (tasks.get(i).getTime_Required() == 0) { // if the time required was not entered, nothing is added to the sum (avoids division by 0)
				sum += 0;
			} else {
				sum += ((tasks.get(i).getTime_Spent()/tasks.get(i).getTime_Required()) * tasks.get(i).getPV());
			}	
		}
		return sum;
	}
	
	/**
	 * Calculates and returns the Project's percentage of completion, taking into consideration partially completed Tasks (vs. the percent
	 * scheduled for completion that only considers fully completed Tasks). 
	 * @return The percentage of the Project that has been completed (a float)
	 */
	protected float calcPercentComplete()
	{
		if (bac == 0) { // if the Project's budget at completion is 0 (due to Project being empty or missing planned values), the PC is 0
			return 0;
		} else {
			return (earnedValue/bac);
		}	
	}
	
	/**
	 * Calculates and returns the Project's cost variance (CV), which is the difference between the planned cost of work that have been
	 * completed and the actual cost of work that has been completed.
	 * @return The Project's cost variance (a float)
	 */
	protected float calcCV()
	{
		float sum = 0;
		for(int i = 0; i < tasks.size(); i++){ // for each of the Project's Tasks
			sum += tasks.get(i).getAC(); // the Task's actual cost is added to the running total
		}
		return (earnedValue - sum);
	}
	
	/**
	 * Calculates and returns the Project's schedule variance (SV), which is the difference between the Project's earned value and its planned
	 * value.	
	 * @return the Project's schedule variance (a float)
	 */
	protected float calcSV()
	{
		float sum = 0;
		for(int i = 0; i < tasks.size(); i++){ // for each of the Project's Tasks
			if(tasks.get(i).getStatus_Id() > 1) // if the Task has been started
				sum += tasks.get(i).getPV(); // it's planned value is added to the running total
		}
		return earnedValue - sum;
	}
	
	/**
	 * Calculates and returns the Project's cost performance index (CPI), which is the ratio of the Project's earned value over the actual
	 * cost of completed work. 
	 * @return the Project's cost performance index (a float)
	 */
	protected float calcCPI()
	{
		float sum = 0;
		for (int i = 0; i < tasks.size(); i++) { // for each of the Project's Tasks
			sum += tasks.get(i).getAC(); // each Task's actual cost is added to the running total
		}
		if (sum == 0) { 
			return 0; // if none of the Project's Tasks have been started or actual costs haven't been reported, the method returns 0
		} else {
			return (earnedValue/sum);
		}	
	}
	
	/**
	 * Calculates and returns the Project's schedule performance index (SPI), which is the ratio of the Project's earned value over its
	 * planned value.
	 * @return the Project's schedule performance index (a float)
	 */
	protected float calcSPI()
	{
		float sum = 0;
		for (int i = 0; i < tasks.size(); i++) { // for each of the Project's Tasks
			if(tasks.get(i).getStatus_Id() > 1) // if the Task has been started
				sum += tasks.get(i).getPV(); // its planned value is added to the running total
		}
		if (sum == 0) {
			return 0; // if none of the Project's Tasks have been started or there are no Tasks, the method returns 0
		} else {
			return earnedValue/sum;
		}	
	}
	
	/**
	 * Calculates and returns the Project's estimate at completion (EAC), which is the Project's budget at completion, divided by its
	 * cost performance index.
	 * @return The Project's estimate at completion (a float)
	 */
	protected float calcEAC()
	{
		if (cpi == 0) {
			return 0; // if the CPI is 0 (no Tasks have been started or no actual costs), the method returns 0
		} else {
			return bac/cpi;
		}	
	}
	
	/**
	 * Equivalent of a toString method for the EVAnalysis class' metrics.
	 * @return A String listing all of the Project performance metrics calculated by the EVAnalysis class.
	 */
	public String[] compileResults(){
		String[] results = {"Budget At Completion is: " + bac + "\n", "Earned Value is: " + earnedValue + "\n", 
				"Percent Scheduled Completion is: " + percentScheduledCompletion + "\n", "Percent Complete is: " + percentComplete + "\n", "Cost Variance is: " + cVar + "\n", "Schedule Variance is: " + sVar + "\n",
				"Cost Performance Index is: " + cpi + "\n", "Schedule Performance Index is: " + spi + "\n", "Estimate At Completion is: " + eac + "\n"};
		return results;
	}
	
	/**
	 * Get method for the bac variable.
	 * @return The Project's calculated budget at completion (BAC) as a float
	 */
	public float getBAC() {
		return bac;
	}
	
	/**
	 * Get method for the earnedValue variable.
	 * @return The Project's calculated earned value (EV) as a float
	 */
	public float getEarnedValue() {
		return earnedValue;
	}
	
	/**
	 * Get method for the cpi variable.
	 * @return The Project's cost performance index (CPI) as a float
	 */
	public float getCPI() {
		return cpi;
	}
}

package projectmanager.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import ca.concordia.comp354.projectmanager.ui.EstimateTaskFrame;
import ca.concordia.comp354.projectmanager.ui.FrameMaster;
import ca.concordia.comp354.projectmanager.ui.PERTFrame;
import ca.concordia.comp354.projectmanager.ui.TasksWindow;

public class ManageTasksAction implements ActionListener {

	FrameMaster frameMaster;
	TasksWindow tasksWindow;

	public ManageTasksAction(FrameMaster fm, TasksWindow tw) {
		frameMaster = fm;
		tasksWindow = tw;
	}

	public void actionPerformed(ActionEvent evt) {
		System.out.println(evt.getActionCommand());

		if (evt.getActionCommand().toString().equalsIgnoreCase("Add")) {
			frameMaster.setTasksWindow(tasksWindow);
		}

		else if (evt.getActionCommand().toString().equalsIgnoreCase("Go Back")) {
			frameMaster.setTasksWindow(tasksWindow);
			frameMaster.showProjectWindow();
		}

		else if (evt.getActionCommand().toString().equalsIgnoreCase("Tasklist")) {
			if (frameMaster.setUpTaskWindow()) {
				frameMaster.showTasksWindow();
			}
		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Estimate Time for PERT")) {
			if (frameMaster.getTaskClicked() != -1) {
				new EstimateTaskFrame(frameMaster.getTaskClicked())
						.setVisible(true);
			} else {
				showErrorMessage();
			}

		}

		else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Create Task")) {
			frameMaster.showAddTaskWindow();
			
		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("PERT Analysis")) {
			if (frameMaster.getTaskClicked() != -1) {
				new PERTFrame(frameMaster.getTaskClicked()).setVisible(true);
			} else {
				showErrorMessage();
			}
		}
	}

	private void showErrorMessage() {
		JOptionPane
				.showMessageDialog(
						null,
						"Nothing has been selected. "
								+ "Please select the task, by clicking once on the desired task");
	}

}

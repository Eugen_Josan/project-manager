package projectmanager.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import SwiftGantt.GanttChart;
import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.projectmanager.ui.AboutUsWindow;
//import ca.concordia.comp354.projectmanager.ui.AboutUsWindow;
import ca.concordia.comp354.projectmanager.ui.CPAFrame;
import ca.concordia.comp354.projectmanager.ui.EVAWindow;
import ca.concordia.comp354.projectmanager.ui.FrameMaster;
import ca.concordia.comp354.projectmanager.ui.PERTFrame;

public class ManageProjectsAction implements ActionListener {

	FrameMaster frameMaster;
	private ArrayList<Project> projects;

	public ManageProjectsAction(FrameMaster fm, ArrayList<Project> projectsIn) {
		frameMaster = fm;
		projects = projectsIn;
	}

	public ManageProjectsAction(FrameMaster fm) {
		frameMaster = fm;
		projects = new ArrayList<Project>(0);
	}

	private void showErrorMessage() {
		JOptionPane
				.showMessageDialog(
						null,
						"Nothing has been selected. "
								+ "Please select the project, by clicking once on the desired project");
	}

	public void actionPerformed(ActionEvent evt) {

		if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Create Projects")) {
			frameMaster.showAddProjectWindow();
		}
		if (evt.getActionCommand().toString().equalsIgnoreCase("Tasks")
				&& projects.size() > 0) {
			frameMaster.showTasksWindow();
			
		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("About Us")) {
			 new AboutUsWindow().setVisible(true);

		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Log Out")) {

			frameMaster.showLoginWindow();

		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("PERT Analysis")) {
			System.out.println(evt.getActionCommand());
			if (frameMaster.getClickedProjectId() != -1) {
				new PERTFrame(frameMaster.getClickedProjectId())
						.setVisible(true);
			} else {
				showErrorMessage();
			}

		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Generate Gant Chart")) {

			if (frameMaster.getClickedProjectId() != -1) {
				try {
					new GanttChart(frameMaster.getClickedProjectId())
							.setVisible(true);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				showErrorMessage();
			}

		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Critical Path Analysis")) {
			System.out.println(evt.getActionCommand());
			if (frameMaster.getClickedProjectId() != -1) {
				new CPAFrame(frameMaster.getClickedProjectId())
						.setVisible(true);
			} else {
				showErrorMessage();
			}

		} else if (evt.getActionCommand().toString()
				.equalsIgnoreCase("Earned Value Analysis")) {
			System.out.println(evt.getActionCommand());
			if (frameMaster.getClickedProjectId() != -1) {
				new EVAWindow(frameMaster.getClickedProjectId())
						.setVisible(true);
			} else {
				showErrorMessage();
			}
		}
	}

}

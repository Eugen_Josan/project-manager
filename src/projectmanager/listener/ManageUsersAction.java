package projectmanager.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.projectmanager.ui.FrameMaster;
import ca.concordia.comp354.domain.LoginUserSingleton;
import ca.concordia.comp354.domain.User;

public class ManageUsersAction implements ActionListener {
	
	FrameMaster frameMaster;
	private ArrayList<User> users;
	
	public ManageUsersAction(FrameMaster fm, ArrayList<User> usersIn) {
		frameMaster = fm;
		users = usersIn;
	}
	
	public ManageUsersAction(FrameMaster fm) {
		frameMaster = fm;
		users = new ArrayList<User>(0);
	}

	public void actionPerformed(ActionEvent evt) {
		
		if(evt.getActionCommand().toString().equalsIgnoreCase("Log Out")){
			frameMaster.showLoginWindow();
		}
		else if(evt.getActionCommand().toString().equalsIgnoreCase("Edit")){
			frameMaster.setAddUserWindowForAdd(false);
		}
		else if(evt.getActionCommand().toString().equalsIgnoreCase("Add")){
			frameMaster.setAddUserWindowForAdd(true);
		}
		else if(evt.getActionCommand().toString().equalsIgnoreCase("Go Back")){
			frameMaster.showProjectWindow();
		}
	}
}

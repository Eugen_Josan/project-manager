package SwiftGantt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JScrollPane;

import org.swiftgantt.common.Time;
import org.swiftgantt.model.GanttModel;
import org.swiftgantt.model.Task;
import org.swiftgantt.ui.TimeUnit;
import org.swiftgantt.GanttChart;

import ca.concordia.comp354.domain.Project;

/**
 * this class is used to convert the tasks from database to the tasks of swiftgantt package, and creating the data for GanttChart
 * 
 * @author Eugeniu Josan
 * 6632882
 * 
 */
public final class GanttChartData extends GanttChart{

	private static final Logger logger = Logger.getLogger( GanttChartData.class.getName() );
	
	Time startTime = null;
	Time possibleEndTime = null;
	
	Task project = null;
	
	//Gantt chart data constructor; removing the default logo space from Gantt Chart class
	public GanttChartData(){
		this.setCorner(JScrollPane.UPPER_LEFT_CORNER, null);
	}
	
	public static String dateConvertToString(Date date){
		SimpleDateFormat dateType = new SimpleDateFormat ("yyyy/MM/dd");
		return dateType.format(date);
	}
	
	
	//converter string to swiftgantt Time
	private Time stringToTime(String date){
		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(5, 7));
		int day = Integer.parseInt(date.substring(8, 10));
		
		return new Time(year, month, day);
	}
	
	/**
	 * This method will initialize the main Task (The Project) with time limits, title and tasks.
	 * @param projectId the working project id
	 * @throws ParseException.
	 */
	
	public void initializeProject(int projectId) throws ParseException {
		String title;
		setTimeUnit(TimeUnit.Day);
		
		Project tempProject = new Project(projectId);
				
		String start_date = dateConvertToString(tempProject.getstart_Date());
		startTime = stringToTime(start_date);
		
		String end_date = dateConvertToString(tempProject.getend_Date());
		possibleEndTime = stringToTime(end_date);
		
		title = tempProject.getTitle();
		
		//creation of project/main task
		project = new Task(title, startTime, possibleEndTime, null);
		populateTasksWithDbData(projectId);
	}

	/**
	 * This method will populate the main Task (the project) with data from database by converting them into swiftgantt standard.
	 * @param projectId the working project id
	 * @throws ParseException.
	 */
	private void populateTasksWithDbData(int projectId) throws ParseException {
		GanttModel model = new GanttModel();
		ArrayList<Integer> list = new ArrayList<Integer>(50);
		
		Map<Integer, Task> taskMap = new HashMap<Integer,Task>();
		
		model.removeAll();
		model.setKickoffTime(startTime);
		model.setDeadline(possibleEndTime);

		ArrayList<ca.concordia.comp354.domain.Task> taskList = ca.concordia.comp354.domain.Task.getAllTasksByCondition(projectId, 1);
		for(int i = 0; i != taskList.size() ; ++i){	
			
			String start_date = dateConvertToString(taskList.get(i).getStart_Date());
			Time start_time = stringToTime(start_date);
				
			String end_date = dateConvertToString(taskList.get(i).getEnd_Date());
			Time end_time = stringToTime(end_date);

			taskMap.put(taskList.get(i).getId(),
					(new Task(taskList.get(i).getDescription(), start_time, end_time, (int) taskList.get(i).getTime_Spent())));
		}
		
		for (Map.Entry<Integer, Task> entry : taskMap.entrySet()) {
			
			list = ca.concordia.comp354.domain.Task.getTaskById(entry.getKey()).getParentTask();
			
			if(!list.isEmpty()){
				int k = 0;
				while(k!=list.size()){
					entry.getValue().addPredecessor(taskMap.get(list.get(k)));
					++k;
				}	
			}
			project.add(entry.getValue());
		}
		
		model.addTask(project);
		setModel(model);
	}
}


package SwiftGantt;

/**
 * This class is used to create the interface of the ganttchart. 
 * It is based on the SwiftGantt framework 
 * 
 * @author Eugeniu Josan 
 * 6632882
 *
 */

import java.awt.GridLayout;
import java.text.ParseException;
import java.util.logging.Logger;

import org.jdesktop.layout.GroupLayout;

public class GanttChart extends javax.swing.JFrame {
	private static final Logger logger = Logger.getLogger( GanttChart.class.getName() );
	private GanttChartData ganttData = new GanttChartData();
	private javax.swing.JPanel pnlContent;
		
	// @throws ParseException
	public GanttChart(int idProject) throws ParseException{
		initializingComponents();

		this.ganttData.initializeProject(idProject);
		
		pnlContent.setLayout(new GridLayout());
		pnlContent.add(ganttData);
	}

	/**This class will initialize the gantt chart diagram
	 */
  private void initializingComponents() {

    ganttData = new GanttChartData();
    pnlContent = new javax.swing.JPanel();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Gantt Chart");

    GroupLayout pnlContentLayout = new GroupLayout(pnlContent);
    pnlContent.setLayout(pnlContentLayout);
    pnlContentLayout.setHorizontalGroup(
      pnlContentLayout.createParallelGroup()
      .add(0, 805, Short.MAX_VALUE)
    );
    pnlContentLayout.setVerticalGroup(
      pnlContentLayout.createParallelGroup()
      .add(0, 350, Short.MAX_VALUE)
    );

    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
      .add(layout.createSequentialGroup()
        .addContainerGap()
        .add(layout.createParallelGroup()
          .add(pnlContent, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
      .add(layout.createSequentialGroup()
        .addContainerGap()
        .add(pnlContent, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addContainerGap())
    );

    pack();
  }
}

package PERT;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;

import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.Task;

public class PERTAnalysis {

	private int ProjectId;
	private ArrayList<PertNode> nodeList;
	private ArrayList<Task> taskList;

	public PERTAnalysis(int projectId) {
		ProjectId = projectId;
	}

	public void BuildPERTNetwork(){
		
		taskList = Task.getAllTasksByCondition(ProjectId, 1);
		nodeList = new ArrayList<PertNode>();
		
		//PART A: this creates the nodes and puts them in the nodeList 
		for(Task task : taskList){
			PertActivity edge =  new PertActivity(task.getExpected_time_attribute(), task.getStandard_deviation_attribute());
			PertNode node =  new PertNode(task.getId());
			node.setEdge(edge);
			nodeList.add(node);
			
		}
		
		//For each node in PART A we need to setup the the parent node
		for(int i = 0; i < taskList.size(); i++){
			Task task = taskList.get(i);
			ArrayList<Integer> parentIdList = task.getParentTask();
			for(PertNode node : nodeList){
				for(Integer id : parentIdList){
					if(node.getEventNumber() == id){
						nodeList.get(i).addParent(node);
						continue;
					}
				}
			}
		}
		
		calculateExpectedDatesOfEvents();
		calculateStandardDeviationForEvents();
	}
	
	public void calculateExpectedDatesOfEvents()
	{
		nodeList.get(0).setExpectedDate(0);
		for(int i = 1; i < nodeList.size(); i++){
			PertNode node = nodeList.get(i);
			ArrayList<PertNode> parentList = node.getParentList();
			double maxExpectedDate = -1;
			for(PertNode parentNode : parentList){
				double newMaxExpectedDate =  calculateExpectedDateForNodeWithParent(node, parentNode);
				if(newMaxExpectedDate > maxExpectedDate){
					maxExpectedDate =  newMaxExpectedDate;
				}
			}
			node.setExpectedDate(maxExpectedDate);
		}
	}
	
	private double calculateExpectedDateForNodeWithParent(PertNode node, PertNode parentNode){
		return node.getEdge().getExpectedTime() + parentNode.getExpectedDate();
	}
	
	public void calculateStandardDeviationForEvents(){
		nodeList.get(0).setStandardDeviation(0);
		for(int i = 1; i < nodeList.size(); i++){
			PertNode node = nodeList.get(i);
			ArrayList<PertNode> parentList = node.getParentList();
			double maxStandarDeviation = -1;
			for(PertNode parentNode : parentList){
				double newMaxStandardDeviation = calculateStandardDeviationForNodeWithParent(node, parentNode);
				if(newMaxStandardDeviation > maxStandarDeviation){
					maxStandarDeviation =  newMaxStandardDeviation;
				}
			}
			node.setStandardDeviation(maxStandarDeviation);
		}
	}
	
	private double calculateStandardDeviationForNodeWithParent(PertNode node, PertNode parentNode){
		double nodeStandardDeviation = node.getEdge().getStandardDeviation();
		double parentStandarDeviation = parentNode.getStandardDeviation();
		return Math.sqrt(nodeStandardDeviation *  nodeStandardDeviation + parentStandarDeviation * parentStandarDeviation);
	}
	
	public ArrayList<String> gatherData() {
		ArrayList<String> calculatedPertForTask = new ArrayList<String>();
		for(int i = 0; i < taskList.size() ; i++){
			NumberFormat formatter = new DecimalFormat("#0.00");
			double zValue =  nodeList.get(i).calculateZValues(nodeList.get(i).getExpectedDate() + 3);
			calculatedPertForTask.add("" + taskList.get(i).getDescription() + ": "  + formatter.format(zValue));
		}
		return calculatedPertForTask;
	}
}

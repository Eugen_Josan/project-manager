package PERT;

import java.util.ArrayList;

public class PertNode {
	
	private int EventNumber;
	private double ExpectedDate;
	//TargetDate should be an input from the user instead of calculated by the program.
	//private double TargetDate;
	private double StandardDeviation;
	private PertActivity Edge;
	private ArrayList<PertNode> ParentList;
	
	public PertNode(int eventNumber)
	{
		EventNumber = eventNumber;
		ExpectedDate = 0;
		//TargetDate = 0;
		StandardDeviation = 0;
		ParentList =  new ArrayList<PertNode>();
		Edge =  null;
	}
	
	public double calculateZValues(double TargetDate){
		if(StandardDeviation == 0){
			return 0;
		}
		return (TargetDate - ExpectedDate) / StandardDeviation;
	}
	
	
	public void addParent(PertNode parent){
		ParentList.add(parent);
	}
	
	// Setters
	
	public void setEventNumber(int eventNumber)
	{
		EventNumber = eventNumber;
	}
	
	public void setExpectedDate(double expectedDate)
	{
		ExpectedDate = expectedDate;
	}
	
	/*public void setTargetDate(double targetDate)
	{
		TargetDate = targetDate;
	}*/
	
	public void setStandardDeviation(double standardDeviation)
	{
		StandardDeviation = standardDeviation;
	}
	
	public void setEdge(PertActivity edge){
		Edge = edge;
	}

	// Getters
	
	public int getEventNumber()
	{
		return EventNumber;
	}
	
	public double getExpectedDate()
	{
		return ExpectedDate;
	}
	
	/*public double getTargetDate()
	{
		return TargetDate;
	}*/
	
	public double getStandardDeviation()
	{
		return StandardDeviation;
	}
	
	public PertActivity getEdge(){
		return Edge;
	}
	
	public ArrayList<PertNode> getParentList(){
		return ParentList;
	}
	
}

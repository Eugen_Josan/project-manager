package PERT;

public class PertActivity {
	double expectedTime;
	double standardDeviation;
	
	public PertActivity(double exTime, double stDeviation) {
		this.expectedTime = exTime;
		this.standardDeviation = stDeviation;
	}
	
	double getExpectedTime(){
		return expectedTime;
	}
	
	double getStandardDeviation(){
		return standardDeviation;
	}
	
	void setExpectedTime(double exTime){
		this.expectedTime = exTime;
	}
	
	void setStandarDeviation(double stDeviation){
		this.standardDeviation = stDeviation;
	}
}

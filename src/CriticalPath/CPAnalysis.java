package CriticalPath;

import java.util.ArrayList;
import java.util.Date;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.DAO.TaskDAO;

public class CPAnalysis {

	private ArrayList<Task> tasks; // stores an ArrayList of the Project's Tasks
	private int projectID; // Project number
	private final int key = 1; // used to set the behavior of the constructor's getAllTasksByCondition method call (return an ArrayList of the Project's Tasks) 
	private ArrayList<Integer> list; // stores the IDs of the Tasks that are along the Project's critical path
	
	public CPAnalysis(int project){
		projectID = project;
		tasks = Task.getAllTasksByCondition(projectID, key); // gets an ArrayList of all the Tasks in the Project
		list = new ArrayList<Integer>();
		if (tasks.size() > 0) {
			list.add(tasks.get(tasks.size()-1).getId()); // adds the ID of the last Task in the Project (necessarily along the critical path) to the list of critical path Tasks
		}
	}
	
	/**
	 * Secondary constructor used to facilitate unit testing by removing the need to populate the database with test data.
	 * @param taskList An ArrayList of dummy Task objects to be used for unit testing
	 */
	protected CPAnalysis(ArrayList<Task> taskList) {
		tasks = taskList;
		list = new ArrayList<Integer>();
		list.add(tasks.get(tasks.size()-1).getId());
	}

	/**
	 * This method searches for the specified Task's parent Tasks, then if multiple parent Tasks are found, it finds the parent with the latest end_Date
	 * and adds it to the list of Task IDs that are along the Project's critical path. This method is designed to be initially called using a Project's last Task,
	 * which is necessarily at the end of the critical path. It then recursively adds each Task that is also along the critical path until it reaches 
	 * the Project's first Task, which has no parent Task.
	 * @param childID A Task ID number (Integer)
	 * @return The int 0
	 */
	protected int analyze(int childID)
	{
		ArrayList<Integer> parents = TaskDAO.getParentTasks(childID); // queries the database for the ID numbers of the specified Task's parent Tasks 
		ArrayList<Task> parentTasks = getParentObjects(parents); // turns the list of parent Task IDs into a list of parent Task objects
		Date latestDate = new Date(1900, 0, 1);
		int latestIndex = 0;
		if(parents.size()>1){ // if the Task has multiple parents
			for(int i = 0; i < parentTasks.size(); i++)
			{
				if(latestDate.before(parentTasks.get(i).getEnd_Date()))
				{
					latestDate = parentTasks.get(i).getEnd_Date();
					latestIndex = i;
				}
			} // finds the parent with the latest end_Date, then adds it to the list of Tasks along the Project's critical path
			list.add(parentTasks.get(latestIndex).getId());
			analyze(parentTasks.get(latestIndex).getId());
		}
		else if(parents.size() == 1) // if the Task has a single parent, it is necessarily along the critical path
		{
			list.add(parentTasks.get(latestIndex).getId());
			analyze(parentTasks.get(latestIndex).getId());	
		}
		return 0;
	}
	
	/**
	 * Takes a list of Task IDs and returns an ArrayList of the corresponding Task objects. 
	 * @param ids An ArrayList of Task IDs (Integers)
	 * @return An ArrayList of Task objects
	 */
	protected ArrayList<Task> getParentObjects(ArrayList<Integer> ids)
	{
		ArrayList<Task> parentTask = new ArrayList<Task>();
		if(ids.size() >0){ // if the list has at least 1 Task ID
			for(int i = 0; i < ids.size(); i++){
				for(int j = 0; j < tasks.size(); j++) // it searches the list of the Project's Tasks for a matching Task object
				{
					if(ids.get(i) == tasks.get(j).getId()){
						parentTask.add(tasks.get(j)); // if a match is found it adds it to the list of Task objects to be returned
						break;
					}
				}
			}
		}
		return parentTask;
	}
	
	/**
	 * Determines which of a Project's Tasks are part of its critical path using the analyze method, then returns a list of their descriptions,
	 * which is then used to display those Tasks on the screen.
	 * @return An ArrayList of Task descriptions (String)
	 */
	public ArrayList<String> gatherData() {
		ArrayList<String> descriptions = new ArrayList<String>();
		if (tasks.size() < 1) {
			return descriptions;
		} else {
			analyze(tasks.get(tasks.size()-1).getId()); // call the analyze method passing the Project's last Task
			for(int i = 0; i < list.size(); i++) {
				for(int j = 0; j < tasks.size(); j++) {
					if(list.get(i) == tasks.get(j).getId()) {
						descriptions.add(tasks.get(j).getDescription()); // creates a list of the critical path Tasks' descriptions
						break;
					}	
				}
			}			
		}
		return descriptions;
	}
	
	/**
	 * Returns a copy of the list of Task IDs that are along the Project's critical path. Used primarily to facilitate unit testing.
	 * @return An ArrayList of Task IDs (Integer)
	 */
	protected ArrayList<Integer> getList() {
		ArrayList<Integer> returnList = new ArrayList<Integer>();
		for (Integer taskId : list) {
			returnList.add(taskId);
		}
		return returnList;
	}
}

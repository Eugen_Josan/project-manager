package CriticalPath;

import org.junit.Test;
import junit.framework.TestCase;
import static org.junit.Assert.*;

import ca.concordia.comp354.domain.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * This class is a collection of unit tests for the CPAnalysis class that were implemented using JUnit. The test cases cover all of the basis paths
 * for each method in the class.
 * @author Sebastien Charbonneau (6427286)
 */
public class CPAnalysisTests extends TestCase {
	
	private CPAnalysis testCPAnalysis; // dummy object used for the method calls
	
	private Task id1; // the dummy Project's 1st task
	private Task id2; // must be done after Task 1 and before Task 4; has an earlier end_Date than Task 3
	private Task id3; // must be done after Task 1 and before Task 4; along the critical path
	private Task id4; // has 2 parent tasks: Tasks 2 and 3
	private Task id5; // the dummy Project's last task
	ArrayList<Task> dummyTaskList; 
	ArrayList<Integer> task1Parents; // an empty list since this Task has no parents
	ArrayList<Integer> task2Parents; // contains the ID of Task 2's parent Tasks: 1
	ArrayList<Integer> task3Parents; // contains the ID of Task 3's parent Task: 1
	ArrayList<Integer> task4Parents; // contains the IDs of Task 4's parent Tasks: 2 and 3
	ArrayList<Integer> task5Parents; // contains the ID of Task 5's parent Task: 4
	
	public static final Integer TYPICAL_PROJECT_ID = 5; // a typical Project used for tests that require querying the database, mirrors the one above
	public static final Integer TYPICAL_PROJECT_FIRST_TASK_ID = 30; // mirrors Task 1
	public static final Integer TYPICAL_PROJECT_CRITICAL_PATH_TASK_ID = 32; // mirrors Task 3
	public static final Integer TYPICAL_PROJECT_DUAL_PARENT_TASK_ID = 33; // mirrors Task 4
	public static final Integer TYPICAL_PROJECT_LAST_TASK_ID = 34; // mirrors Task 5
	
	public static final Integer EMPTY_PROJECT_ID = 6; // a Project with no Tasks
	
	public static final Integer SINGLE_TASK_PROJECT_ID = 7; // a Project with a single Task used for tests that require querying the database
	public static final Integer SINGLE_TASK_ID = 35;
	
	public static final Integer SINGLE_PARENT_PROJECT_ID = 8; // a linear Project with 2 Tasks used for tests that require querying the database
	public static final Integer SINGLE_PARENT_TASK_ID = 36; // the Project's first Task
	private String singleParentTaskDescription = "Crash Man Stage"; // the parent Task's description
	public static final Integer SINGLE_PARENT_CHILD_TASK_ID = 37; // the Project's last Task
	private String singleParentChildTaskDescription = "Flash Man Stage"; // the child Task's description
	
	/**
	 * Creates a list of Tasks for use in the unit tests that don't require testing data from the database. The Project and Tasks created mirror
	 * the ones in the "typical" Project in the database.
	 */
	private void createDummyTaskList() {
		dummyTaskList = new ArrayList<Task>();
		task1Parents = new ArrayList<Integer>();
		task2Parents = new ArrayList<Integer>();
		task2Parents.add(1);
		task3Parents = new ArrayList<Integer>();
		task3Parents.add(1);
		task4Parents = new ArrayList<Integer>();
		task4Parents.add(2);
		task4Parents.add(3);
		task5Parents = new ArrayList<Integer>();
		task5Parents.add(4);
		id1 = new Task(1, "Metal Man Stage", new Date(2014, 0, 1), new Date(2014, 0, 2), 0, 0, 0, 0, 0, 0, 0, task1Parents, 0, 0); 
		id2 = new Task(2, "Bubble Man Stage", new Date(2014, 0, 2), new Date(2014, 0, 4), 0, 0, 0, 0, 0, 0, 0, task2Parents, 0, 0); 
		id3 = new Task(3, "Wood Man Stage", new Date(2014, 0, 2), new Date(2014, 0, 5), 0, 0, 0, 0, 0, 0, 0, task3Parents, 0, 0); 
		id4 = new Task(4, "Quick Man Stage", new Date(2014, 0, 5), new Date(2014, 0, 6), 0, 0, 0, 0, 0, 0, 0, task4Parents, 0, 0); 
		id5 = new Task(5, "Air Man Stage", new Date(2014, 0, 6), new Date(2014, 0, 8), 0, 0, 0, 0, 0, 0, 0, task5Parents, 0, 0); 
		dummyTaskList.add(id1);
		dummyTaskList.add(id2);
		dummyTaskList.add(id3); 
		dummyTaskList.add(id4);
		dummyTaskList.add(id5);
	}
	
	/**
	 * First test case of the getParentObjects method: The list of Task IDs is empty
	 * This test cast verifies that the method runs correctly when the the first if statement is FALSE.
	 */
	@Test
	public void testGetParentObjects_emptyList() {
		createDummyTaskList();
		testCPAnalysis = new CPAnalysis(dummyTaskList);
		ArrayList<Integer> emptyList = new ArrayList(); // empty ArrayList used to trigger FALSE at the method's first if statement
		ArrayList<Task> emptyListReturn = testCPAnalysis.getParentObjects(emptyList);
		assertEquals(emptyListReturn.size(), 0); // when the first if statement is FALSE, the method returns an empty ArrayList
	}
	
	/**
	 * Second test case of the getParentObjects method: The list of Task IDs is not empty, but none of the IDs match any of the Tasks in the list
	 * This test case verifies that the method runs correctly when the first if statement is TRUE and the second is always FALSE.
	 */
	@Test
	public void testGetParentObjects_noMatchesFound() {
		createDummyTaskList();
		testCPAnalysis = new CPAnalysis(dummyTaskList); // creates a project with Tasks numbered 1 through 5
		ArrayList<Integer> nonMatchingIdList = new ArrayList<Integer>(); 
		nonMatchingIdList.add(6); // the list is non-empty, so the first if statement will be TRUE
		nonMatchingIdList.add(7); // but the ID values added will not find any matching Task IDs, so the second if statement will always be FALSE
		ArrayList<Task> noMatchesReturn = testCPAnalysis.getParentObjects(nonMatchingIdList); // searches for Tasks numbered 4 and 5
		assertEquals(noMatchesReturn.size(), 0); // when the second if statement only returns FALSE, the method returns an empty ArrayList
	}
	
	/**
	 * Third test case of the getParentObjects method: The list of Task IDs is not empty and at least one of the IDs matches one of the Tasks in the list
	 * This test case verifies that the method runs correctly when the first if statement is TRUE and the second is TRUE at least once.
	 */
	@Test
	public void testGetParentObjects_matchFound() {
		createDummyTaskList();
		testCPAnalysis = new CPAnalysis(dummyTaskList); // creates a project with Tasks numbered 1, 2 and 3
		ArrayList<Integer> matchingIdList = new ArrayList<Integer>();
		matchingIdList.add(6); // the list is non-empty, so the first if statement will be TRUE
		matchingIdList.add(2); // this ID value will find a matching Task ID, so the second if statement will be TRUE for one of the loop iterations
		ArrayList<Task> matchesFoundReturn = testCPAnalysis.getParentObjects(matchingIdList); // searches for Tasks numbered 4 and 2
		assertEquals(matchesFoundReturn.size(), 1); // when the second if statement returns TRUE, the method returns the matching Task in an ArrayList
		assertEquals(matchesFoundReturn.get(0), dummyTaskList.get(1)); // verifies the correct Task object was returned 
	}	
	
	/** 
	 * First test case for the analyze method: The Project has a single Task, so it is necessarily along the critical path.
	 * This test case verifies that the method runs correctly when both of its if statements return FALSE.
	 */
	@Test
	public void testAnalyze_noParents() {
		testCPAnalysis = new CPAnalysis(SINGLE_TASK_PROJECT_ID); 
		testCPAnalysis.analyze(SINGLE_TASK_ID);
		assertEquals(testCPAnalysis.getList().size(), 1); // verifies that there is only one Task ID along the critical path
		assertEquals(testCPAnalysis.getList().get(0), SINGLE_TASK_ID); // verifies that the correct Task ID was added to the critical path list
	}	
	
	/**
	 * Second test case for the analyze method: The Project has 2 Tasks, 1 child and 1 parent, so they are necessarily both along the critical path.
	 * This test case verifies that the method runs correctly when its first if statement is FALSE and its second one is TRUE.
	 */
	@Test
	public void testAnalyze_singleParent() {
		testCPAnalysis = new CPAnalysis(SINGLE_PARENT_PROJECT_ID);
		testCPAnalysis.analyze(SINGLE_PARENT_CHILD_TASK_ID);
		assertEquals(testCPAnalysis.getList().size(), 2); // verifies that there are 2 Task IDs along the critical path
		assertEquals(testCPAnalysis.getList().get(0), SINGLE_PARENT_CHILD_TASK_ID); // verifies that the child Task was added to the critical path
		assertEquals(testCPAnalysis.getList().get(1), SINGLE_PARENT_TASK_ID); // verifies that the parent Task was added to the critical path
	}
	
	/**
	 * Third test case for the analyze method: The Project has 5 Tasks; after the first Task, the Project branches out into 2 Tasks, only one of
	 * which is along the critical path. This test case verifies that the method runs correctly when its first if statement is TRUE.
	 */
	@Test
	public void testAnalyze_multipleParents() {
		testCPAnalysis = new CPAnalysis(TYPICAL_PROJECT_ID);
		testCPAnalysis.analyze(TYPICAL_PROJECT_LAST_TASK_ID);
		assertEquals(testCPAnalysis.getList().size(), 4); // verifies that there are 4 Task IDs along the critical path
		assertEquals(testCPAnalysis.getList().get(0), TYPICAL_PROJECT_LAST_TASK_ID); // verifies that the Project's last Task was added 
		assertEquals(testCPAnalysis.getList().get(1), TYPICAL_PROJECT_DUAL_PARENT_TASK_ID); // verifies the last Task's parent was added
		assertEquals(testCPAnalysis.getList().get(2), TYPICAL_PROJECT_CRITICAL_PATH_TASK_ID); // verifies the parent with the latest end_Date was added
		assertEquals(testCPAnalysis.getList().get(3), TYPICAL_PROJECT_FIRST_TASK_ID); // verifies the Project's first Task was added
	}
	
	/**
	 * First test case for the gatherData method: The Project is empty, so there are no Tasks along the critical path.
	 * This test case verifies that the method runs correctly when the first if statement is TRUE.
	 */
	@Test
	public void testGatherData_noTasks() {
		testCPAnalysis = new CPAnalysis(EMPTY_PROJECT_ID);
		ArrayList<String> listOfDescriptions = testCPAnalysis.gatherData();
		assertEquals(listOfDescriptions.size(), 0); // verifies that the list of critical path Task descriptions returned is empty
	}
	
	/**
	 * Second test case for the gatherData method: The Project has 1 or more Tasks that are along the critical path.
	 * This test case verifies that the method runs correctly when the first if statement is FALSE.
	 */
	@Test
	public void testGatherData_hasTasks() {
		testCPAnalysis = new CPAnalysis(SINGLE_PARENT_PROJECT_ID); // this Project has 2 Tasks that are both along the critical path
		ArrayList<String> listOfDescriptions = testCPAnalysis.gatherData();
		assertEquals(listOfDescriptions.size(), 2); // verifies that both of the Tasks' descriptions were added to the list
		assertEquals(listOfDescriptions.get(0), singleParentChildTaskDescription); // verifies that the correct description was added for the child Task
		assertEquals(listOfDescriptions.get(1), singleParentTaskDescription); // verifies that the correct description was added for the parent Task
	}
}
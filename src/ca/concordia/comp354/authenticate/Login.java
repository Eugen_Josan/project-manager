package ca.concordia.comp354.authenticate;

import java.util.logging.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.Formatter;

import ca.concordia.comp354.authenticate.AuthenticateStatus.Status;
import ca.concordia.comp354.database.*;
import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.LoginUserSingleton;

/**
 * Class to validate user information
 * @author LouisMaxime
 *
 */

public class Login {
	//create the logger
	private static final Logger logger = Logger.getLogger( Login.class.getName() );
	
	/**
	 * Validate that the login information provided by the user are correct.
	 * @param username
	 * @param password
	 * @return AuthenticateStatus
	 */
	public static AuthenticateStatus authenticateUser(String username, char[] password) {
		/*Steps to authenticate a users
		  1- Validate that the username exists
		  2- Encrypt the password entered
		  3- Get the encypted password from the DB for the username
		  4- Compare that the entered encrypted password matches the encrypted one in the DB
		  5a- If 3 ok, return the information about the user
		  5b- If 3 not ok, Inform the user about wrong authentication values entered
		*/
		
		setLoggerLevel("");
		logger.log(Level.INFO, "Trying the login with username: " + username);
		DbClass myDB = null;
		try {
			myDB = new DbClass();
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception throw in authenticateUser.");
		} 
			
		if (isUserExists(username, myDB)) {
			String enc_pass = encryptPassword(new String(password));
			String dbPassword = getUserPassword(username, myDB);
			if (comparePassword(enc_pass, dbPassword)) {
				logger.log(Level.INFO, "Authentication success with username: " + username + ".");
				//After user is authenticaed, we setup the LoginUserSingleon class.
				LoginUserSingleton.getInstance().setUser(UserDAO.getUserInfoFromID(UserDAO.getUserID(username, myDB)));
				AuthenticateStatus authStatus = new AuthenticateStatus(Status.OK);
				return authStatus;
			}
			else {
				logger.log(Level.INFO, "Authentication failure with username: " + username + ". Wrong password.");
				AuthenticateStatus authStatus = new AuthenticateStatus(Status.FAILED, "Wrong password");
				return authStatus;
			}
		}
		else {
			logger.log(Level.INFO, "Authentication failure with username: " + username + ". Username not found.");
			AuthenticateStatus authStatus = new AuthenticateStatus(Status.FAILED, "Wrong username");
			return authStatus;
		}
	}
	
	private static void setLoggerLevel(String s){
		if (s.equals("fine")){
			logger.setLevel(Level.FINE);
		} else if (s.equals("info")){
			logger.setLevel(Level.INFO);
		} else {
			logger.setLevel(Level.SEVERE);
		}
	}
	
	// Step 1- Validating that the username exists
	/**
	 * This method will verify in the database if the username entered by the user exists.
	 * @param username
	 * @param db
	 * @return boolean True if username exists, else False
	 */
	private static boolean isUserExists(String username, DbClass db) {
		
		//access the database with something like
		String query = "SELECT COUNT(login) FROM USERS WHERE login = '" + username + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbUsername = -1;
		
		try {
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next())
				{
					nbUsername = result.getInt(1);
				}
				
				if (nbUsername > 1) {
					logger.log(Level.WARNING, "The username " + username + " appear more than once in the database.");
				}
				
			}
			else {
				logger.log(Level.WARNING, "Unable to execute query: " + query + " .");
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception throw in isUserExists.");
		} finally {
			db.closeConnection();
		}
		//if we have one results, return true
		//if we have 0 results, return false
		//if we have more than one results, we should return false and logged that we have some data corrupted in the DB.
		//Normally, it should not happen
		if (nbUsername == 1){
			logger.log(Level.INFO, "Username: " + username + " found.");
			return true;
		}
		else {
			logger.log(Level.INFO, "Username: " + username + " not found.");
			return false;
		}
			
	}
	
	//Step 2- String the entered password
	/**
	 * This method encrypt the password entered by the user.
	 * @param password
	 * @return The encrypted password entered by the user.
	 */
	private static String encryptPassword(String password) {
		//I personnally prefer SHA-1 encryption method
		
		//code from petrnohejl on stackoverflow for this function and byteToHex function
		//http://stackoverflow.com/questions/4895523/java-string-to-sha1
		
		MessageDigest crypt = null;
		String enc_pass = null;
		try {
			//get the crypter
			crypt = MessageDigest.getInstance("SHA-1");
			//clear of junk that could be there
			crypt.reset();
			//get the new value
			crypt.update(password.getBytes("UTF-8"));
			//get the encrypted string
			enc_pass = byteToHex(crypt.digest());
		}
		catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		
		return enc_pass;
	}
	
	//The MessageDigest will create byte and we want a string
	/**
	 * This method will convert an hash to an encrypted string.
	 * @param hash
	 * @return An encrypted string.
	 */
	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String encrypted = formatter.toString();
		formatter.close();
		return encrypted;
	}
	
	/**
	 * This method get the encrypted password of the user associated with the username provided.
	 * @param username
	 * @param db
	 * @return The encrypted password of the user from the database.
	 */
	private static String getUserPassword(String username, DbClass db){
		//access the database with something like:
		String database_password = "";
		String query = "SELECT password FROM USERS WHERE login = '" + username + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		try {
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next()) {
					database_password = result.getString(1);
				}
				
				if (database_password.isEmpty()) {
					logger.log(Level.WARNING, "There is not password for username: " + username + " .");
				}
					
			}
			else {
				logger.log(Level.WARNING, "Unable to execute query: " + query + " .");
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception throw in getUserPassword.");
		} finally {
			db.closeConnection();
		}
			
		
		
		return database_password;
	}
	
	/**
	 * This method compare the encrypted password entered by the user with the one in the database to see
	 * if they match.
	 * @param encryptedPassword
	 * @param dbPassword
	 * @return boolean True if encrypted password matches the password in db, else False
	 */
	private static boolean comparePassword(String encryptedPassword, String dbPassword) {
		return encryptedPassword.equals(dbPassword);
	}
}
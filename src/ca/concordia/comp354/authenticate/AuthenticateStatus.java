package ca.concordia.comp354.authenticate;

/**
* This class is used to return the status of an authentication.
* @author LouisMaxime
*/

public class AuthenticateStatus {
	/* This emum contains both status of an authenticate attempt
	*  OK: This is a success attempt
	*  FAILED: The authentication attempt did not succeed.
	*/
	public static enum Status { OK, FAILED	}
	private Status _status;
	
	//The message is for the user. It gives information why the authentication failed.
	private String _message;
	
	/**
	* Parameter constructor for an AuthenticateStatus
	* @param status Status of the authentication
	*/
	public AuthenticateStatus(Status status) {
		_status = status;
		_message = "";
	}
	
	/**
	* Parameter constructor for an AuthenticateStatus
	* @param status From emum Status; Status of the authentication
	* @param message string; Message about a failed authentication
	*/
	public AuthenticateStatus(Status status, String message) {
		_status = status;
		_message = message;
	}
	
	/**
	* Get the message of the failed authentication
	* @return The message
	*/
	public String getMessage() { return _message; }
	
	/**
	* Get the status of the failed authentication
	* @return The status from enumeratio Status
	*/
	public Status getStatus() { return _status; }
}

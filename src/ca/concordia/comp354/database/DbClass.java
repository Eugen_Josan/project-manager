package ca.concordia.comp354.database;
//should we make them part of proper packages
//using the conventions? such as ca.concordia.comp354.projectName.database

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.*;

 
/**
 * Database connection class
 * @author Amit
 *
 */
public class DbClass {
	//obtain a logger
    private static Logger logger = Logger.getLogger(DbClass.class.getName());
    
	//TODO: create a main or write unit-tests to test this class
	
	//declaring all required parameters
    private String sDriver = "org.sqlite.JDBC";  
    private String sJdbc = "jdbc:sqlite";
    private String dbName = "skhema.db";
    private String sUrl = sJdbc + ":" + dbName; //will give -> jdbc:sqlite:skhema.db	
    private int iTimeout = 30;
    private Connection conn = null;
    private Statement statement = null;

    /**
     * Default constructor, allows quick instantiation of the class with default params and other stuff if required
     * By Default the constructor will create a SQLite connection using the org.sqlite.JDBC driver and will name
     * the database "skhema.db"
     * @throws Exception 
     */
    public  DbClass() throws Exception {
    	logger.log(Level.INFO, "Instantiating DbClass object");
    	this.setConnection();
    	this.setStatement();
    	logger.fine("Object instantiated");
    	setLoggerLevel("");
    }
    
	private static void setLoggerLevel(String s){
		if (s.equals("fine")){
			logger.setLevel(Level.FINE);
		} else if (s.equals("info")){
			logger.setLevel(Level.INFO);
		} else {
			logger.setLevel(Level.SEVERE);
		}
	}
    
    
    /**
     * Parametrized constructor requires "DriverManager name if not using the default sqlite driver
     * and the fully loaded url, if not using the default sqlite url declared in the class
     * can be used for example: 
     * DbClass mydb = new DbClass("org.sqlite.JDBC","jdbc:sqlite:skhema.db");
     * DbClass db = new DbClass("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/feedback?user=sqluser&password=sqluserpw"); 
     * @param sDriverToLoad, a string
     * @param sUrlToLoad, a string 
     * @throws Exception
     */
    public DbClass(String sDriverToLoad, String sUrlToLoad) throws Exception
    {
    	logger.info("Instantiating DbClass object with driver: " + sDriverToLoad + 
    			" url: " + sUrlToLoad) ;
        this.init(sDriverToLoad, sUrlToLoad);
        logger.fine("Object instantiated");
    }
 
    /**
     * The main method used by the constructors to initialize the database connection. 
     * Example: init("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/feedback?user=sqluser&password=sqluserpw");
     * If required, this method can be used in conjunction of setDriver and setUrl to change the database 
     * connection of the instance from one database type to another (such as from SQLite to MySQL)
     * @param sDriverVar a string
     * @param sUrlVar a string
     * @throws Exception
     */
    public void init(String sDriverVar, String sUrlVar) throws Exception
    {
    	logger.fine("Initializing a db conn"
    			+ "ection with new params");
        this.setDriver(sDriverVar); //set the driver to the supplied driver
        this.setUrl(sUrlVar); //set the url to the supplied url
        this.setConnection(); //create the database connection
        this.setStatement(); //create the statement 
    }
    
    /**
     * This method will reinitialize our connection especially if it is null or
     * if it has been closed
     * @throws Exception
     */
    public void reInit() throws Exception{
    	logger.fine("Checking db connection");
    	{
    		if (this.conn == null){
    			logger.info("No connection found, setting connection again");
    			this.setConnection();
    			this.setStatement();
    		} else if (this.conn.isClosed()){
    			logger.info("The connection was closed, reopening connection");
    			this.conn = DriverManager.getConnection(sUrl);
    			this.setStatement();
    		} else {
    			logger.info("Connection open and active");
    			return;
    		}
    	}
    }
 
    /**
     * Setter. Allows changing the driver from default SQLite to whatever is desired. 
     * @param sDriverVar
     */
    private void setDriver(String sDriverVar)
    {
    	logger.fine("Setting new DB Driver " + sDriverVar);
        this.sDriver = sDriverVar;
    }
 
    /**
     * Setter.  Allows changing the url from the default SQLite skhema.db to whatever is desired
     * @param sUrlVar
     */
    private void setUrl(String sUrlVar)
    {
    	logger.fine("Setting new DB url " + sUrlVar);
        this.sUrl = sUrlVar;
    }
    
    /**
     * Creates a database connection using the driver and url parameter
     * by default, will create a SQLite connection and save it as an instance parameter
     * can throw a ClassNotFound exception
     * @throws Exception
     */
    public  void setConnection() throws Exception {
    	logger.fine("Registering the driver " + this.sDriver);
    	// common way to register a driver is to use Java's Class.forName() 
    	// method to dynamically load the driver's class file into memory, 
    	// which automatically registers it. can throw an exception
        Class.forName(this.sDriver);
        
        logger.fine("Establishing a new database connection " + this.sUrl);
        this.conn = DriverManager.getConnection(this.sUrl);
        logger.fine("Database connection object created");
    }
 
    /**
     * Returns the connection instance parameter to the caller.  A user
     * can get the connection using this method and use prepared statements
     * for common tasks that he/she may need to carry out.
     * @return an object of type Connection
     * @throws Exception 
     */
    public  Connection getConnection() throws Exception {
    	logger.fine("Returning database connection object");
    	this.reInit();
        return this.conn;
    }
 
    /**
     * Creates the statement of the database connection.  A statement is used as a general-purpose access
     * to the database. It is useful when using static SQL statements at runtime. 
     * This method allows the creation of the Statement object.  
 
     * @throws Exception
     */
    public  void setStatement() throws Exception {
        if (this.conn == null) {
        	logger.fine("There was no connection, setting new connection with default values");
            this.setConnection();
        }
        logger.fine("Creating a new Statement object to interact with the database");
        this.statement = this.conn.createStatement();
        logger.fine("Setting query timeout to " + this.iTimeout);
        this.statement.setQueryTimeout(this.iTimeout);  // set timeout to 30 sec.
    }
 
    /**
     * Returns the statement instance parameter to the caller
     * @return an object of type Statement
     */
    public  Statement getStatement() {
    	logger.fine("Returning the statement object");
        return this.statement;
    }
    
    /**
     * Returns a PreparedStatement object which can be used to manipulate the database
     * This type of statement can be used to execute repetitious queries
     * string should be like "insert into people values(?,?);"
     * and then one can use prep.setString(1, "blabla"); prep.setString(2, "politics");
     * prep.addBatch(); prep.executeBatch(); prep.executeUpdate() (which returns an int);
     * prep.executeQuery() returns a resultset
     * @param s  is the string representing the query with parameters 
     * @return  a PreparedStatement object
     * @throws Exception
     */
    public PreparedStatement getPreparedStatement(String s) throws Exception{

    	logger.fine("Returning a prepared statement with the query " + s);
    	this.reInit();
    	PreparedStatement prep = this.conn.prepareStatement(s);
    	return prep;
    }
    
    
    /**
     * Returns a boolean value of true if a ResultSet object can be retrieved; 
     * otherwise, it returns false. 
     * Use this method to execute SQL DDL statements or when you need to use truly dynamic SQL. 
     * 
     * @param instruction, a string of SQL
     * @return boolean True (if a resultset can be retrived) else False
     * @throws Exception 
     */
    public boolean execute(String instruction) throws Exception{
    	logger.info("Executing query: " + instruction);
    	this.reInit();
    	return this.statement.execute(instruction);
    }
 
    
    /**
     * Returns the numbers of rows affected by the execution of the SQL statement. 
     * Use this method to execute SQL statements for which you expect to get a number of 
     * rows affected - for example, an INSERT, UPDATE, or DELETE statement. 
     * 
     * @param instruction ex: "INSERT INTO dummy VALUES(1,'Hello from the database'"
     * @throws Exception 
     */
    public int executeStmt(String instruction) throws Exception {
    	logger.fine("Executing query: " + instruction);
    	this.reInit();
        int numRows =  this.statement.executeUpdate(instruction);
        logger.fine("Returning the number of rows affected by this query " + numRows);
        return numRows;
        
    }
    
 
    /**
     * Processes an array of instructions, for example a set of SQL command strings passed
     * from a file.  If this method is used to pass strings from a file, empty lines in the file
     * must be handled by either removing them or parsing them out as they will generate SQLExceptions
     * Overloaded method to handle an array of Insert, update, delete sql statements. 
     * @param instructionSet  strings of SQL commands
     * @throws Exception 
     */
    public void executeStmt(String[] instructionSet) throws Exception {
    	logger.fine("Executing query instructions " );
    	this.reInit();
        for (int i = 0; i < instructionSet.length; i++) {
        	logger.finer("Instruction " + i + ": " + instructionSet[i]);
            this.executeStmt(instructionSet[i]);
        }
    }
 
    /**
     * Returns a ResultSet object. Use this method when you expect to get a result set,
     * as you would with a SELECT statement.
     * A resultset can be read easily using a while loop.  Here's an example:
     * 
     *  ResultSet rs = statement.executeQuery("select * from person");
	 * 	while(rs.next()) {
	 *       // read the result set
	 *       System.out.println("name = " + rs.getString("name"));
	 *       System.out.println("id = " + rs.getInt("id"));
	 *  }
	 *  
     * @param instruction ex: "SELECT response from dummy"
     * @return  an object of type ResultSet
     * @throws Exception 
     */
    public ResultSet executeQry(String instruction) throws Exception {
    	logger.info("Executing query: " + instruction);
    	this.reInit();
        return this.statement.executeQuery(instruction);
    } 
    
    /**
     * Will check to see if the tablename being passed in the parameter
     * exists in the database connection that's open
     * @param tableName string
     * @return boolean true if table exists, false otherwise. 
     * @throws Exception 
     */
    public boolean tableExists(String tableName) throws Exception{
    	logger.info("Checking if tablename " + tableName + " exists");
    	DatabaseMetaData dbm;
    	ResultSet tables;
    	
    	//is the connection open?
    	this.reInit();
    	
    	//get MetaData
    	logger.fine("Getting connection Metadata");
		dbm = this.conn.getMetaData();
		//get resultset that includes the tableName we want to check
		logger.fine("Acquiring tables name and verifying for existence");
		tables = dbm.getTables(null, null, tableName, null);
		
		//check if there's anything in the tables resultset
		if(tables.next()){
			logger.fine("Found table, returning true");
			return true;
		} else {
			logger.fine("Table not found in the database, returning false");
			return false;
		}
    }
    
    /**
     * This method allows closing the connection object and the statement object. Closing 
     * a connection allows saving database resources.  The method explicitly closes the statement
     * object before closing the connection (which will automatically close the statement object)
     * to ensure proper cleanup.      
     *  
     */
    public void closeConnection() {
    	//close the statement first
    	try {
    		logger.fine("Closing the statement " );
    		if (this.statement == null){
    			logger.fine("Statement is null");
    		} else {
    			this.statement.close();
    		}
    		
		} catch (SQLException e) {
			
			logger.log( Level.SEVERE, e.toString(), e );
		}
    	finally {
    		logger.fine("Statement closed");
    	}
    	
    	//close the database connection first
        try { 
        	logger.fine("Closing the database connection ");
        	if (this.conn == null){
        		logger.fine("Connection is null");
        	} else {
        		this.conn.close();
        	}
        	 
       	} catch (Exception e) {
			logger.log( Level.SEVERE, e.toString(), e );       	
		} finally {
			logger.fine("Connecton closed");
		}
    }
 
}
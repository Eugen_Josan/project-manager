package ca.concordia.comp354.projectmanager.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.User;

public class AddTaskWindow extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6694530268994659271L;

	// reference for the Frame Container
	FrameMaster frameMaster;
	Project project;
	Task previousTask;

	public AddTaskWindow(FrameMaster fm) {
		frameMaster = fm;
		initComponents();
	}

	public AddTaskWindow(FrameMaster fm, Project projectIn, Task taskIn) {
		frameMaster = fm;
		project = projectIn;
		previousTask = taskIn;

		initComponents();
	}

	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JTextField jTextField1;
	private javax.swing.JTextField jTextField2;
	private javax.swing.JTextField jTextField3;
	private javax.swing.JTextField jTextField4;
	private JSpinner spinner;
	private JSpinner spinner2;
	private JSpinner spinner3;
	private JComboBox memberList;
	
	private String[] previousTasks;
	private void initComponents() {
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jTextField1 = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		jTextField2 = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();
		jTextField3 = new javax.swing.JTextField();
		jTextField4 = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jButton1 = new javax.swing.JButton();
		jButton2 = new javax.swing.JButton();

		if (project != null) {

			jLabel1.setText("Adding Task for Project: " + project.getTitle());
		}

		jLabel2.setText("Description");

		jLabel3.setText("Start Date");
		
		//display default current day 
		DateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd");
		Date today = new Date();		
		jTextField2.setText(formatDate.format(today));

		jLabel4.setText("Duration in days");
		

		jLabel5.setText("");
		jTextField1.setText("Description here");
		jButton1.setText("Save");
		
		jLabel6.setText("Planned Value");
		

		//Adding spinner for predecessor Task
		//@Eugeniu Josan 6632882 (Modified 2014/07/15)
		JLabel lbl_PrevTask = new JLabel("Predecessor Task: ");
 
		previousTasks = null;
		
		if (project != null) {
			
			Map<Integer, String> tasksMap =  new HashMap<Integer,String>();
			int key = 0;
			String value = "no predecessor Task";
			tasksMap.put(key, value);
			ArrayList<Task> temp = Task.getAllTasksByCondition(project.getId(), 1);
						
			for(int i = 0; i!=temp.size(); ++i){
				key = temp.get(i).getId();
				value = temp.get(i).getDescription();
				tasksMap.put(key, value);
			}
			
			previousTasks = new String[tasksMap.size()]; 
			
			int k = 0;
			for (Entry<Integer, String> entry : tasksMap.entrySet()) {
				previousTasks[k] = "["+entry.getKey()+"] "+ entry.getValue();
				++k;
			}
				
		}else{
			previousTasks = new String[1];
			previousTasks[0] = "no predecessor Task";
		}
		
		JLabel lbl_Temp = new JLabel("");

		final SpinnerListModel spnn_PrevTask = new SpinnerListModel(previousTasks);
		spinner = new JSpinner(spnn_PrevTask);
		
		final SpinnerListModel spnn_PrevTask2 = new SpinnerListModel(previousTasks);
		spinner2 = new JSpinner(spnn_PrevTask2);
		
		final SpinnerListModel spnn_PrevTask3 = new SpinnerListModel(previousTasks);
		spinner3 = new JSpinner(spnn_PrevTask3);
		
		JLabel lbl_RespMember = new JLabel("Responsable member: ");
		ArrayList<User> temp = UserDAO.getAllMembers();
		
		String[] members = new String [temp.size()+1];
		final int[] indexMembers = new int [temp.size()+1];
		for (int i = 1; i != temp.size()+1; ++i){
			members[0] = "Not assigned";
			members[i] = temp.get(i-1).getName();
			indexMembers[0] = 0;
			indexMembers[i] = temp.get(i-1).getId();
		}
		
		new JLabel("");
		
		memberList = new JComboBox(members);
		memberList.setSelectedIndex(0);
		
		
		jButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					saveButtonActionPerformed(e);
				} catch (ParseException e1) {
					e1.printStackTrace();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

			private void saveButtonActionPerformed(ActionEvent evt) throws Exception {
				//converting text fields to Date format for project creation
				//@author Eugeniu Josan 6632882 (modified 2014/06/26)

				String date = jTextField2.getText().toString();
				Date startDate = new SimpleDateFormat("yyyy/MM/dd").parse(date);
				float duration = Float.parseFloat(jTextField3.getText().toString());

				
				String descriptionTxt = jTextField1.getText().toString();
				float planned_value = Float.parseFloat(jTextField4.getText().toString());
				
				if (descriptionTxt != "") {
					Task task = new Task(descriptionTxt, startDate, duration, 0, 1, 0, 0, 0, planned_value, 0);
					task.addTaskToDb(project.getId());
					
					
					//Adding spinner result to Database
					//@Eugeniu Josan 6632882 (Modified 2014/07/15)
					//**************************************************************************
					
					
					String predecessor = (String) spinner.getValue();
					int temp = getParentTaskId(predecessor);
					
					if(temp!=0) {
						task.addParentTaskRelation(temp);
					}
						
					predecessor = (String) spinner2.getValue();
					int temp2 = getParentTaskId(predecessor);
					if(temp2!=0 && temp != temp2){
						task.addParentTaskRelation(temp2);
					}
					
					predecessor = (String) spinner3.getValue();
					int temp3 = getParentTaskId(predecessor);
					if(temp3!=0 && temp != temp3 & temp2 != temp3 ){
						task.addParentTaskRelation(temp3);
					}
										
					//Adding ComboBox result to Database
					//@Eugeniu Josan 6632882 (Modified 2014/07/15)
					//**************************************************************************
					int index = memberList.getSelectedIndex();
					String memberName =  (String) memberList.getSelectedItem();
					
					if (memberName != "Not assigned"){
						UserDAO.addUserToTask(indexMembers[index], task.getId());
					}
				} 
				TasksWindow tw = new TasksWindow(frameMaster, project);
				frameMaster.setTasksWindow(tw);

				frameMaster.showTasksWindow();
			}			
		});

		jButton2.setText("Go Back");
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				TasksWindow tw = new TasksWindow(frameMaster, project);
				frameMaster.setTasksWindow(tw);
		
				frameMaster.showTasksWindow();
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						javax.swing.GroupLayout.Alignment.LEADING,
						layout.createSequentialGroup()
								.addContainerGap(134, Short.MAX_VALUE)
								.addComponent(jLabel1).addGap(125, 125, 125))
				.addGroup(
						layout.createSequentialGroup()
								.addGap(71, 71, 71)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		jButton1,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		106,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jButton2,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		117,
																		javax.swing.GroupLayout.PREFERRED_SIZE))
												.addGroup(
														layout.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																false)
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						lbl_PrevTask)
																				.addGap(7,
																						7,
																						7)
																				.addComponent(
																						spinner,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						120,
																						170))
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						spinner2,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						120,
																						170))
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						spinner3,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						120,
																						170))
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						lbl_RespMember)
																				.addGap(18,
																						18,
																						18)
																				.addComponent(
																						memberList,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						120,
																						200))
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						jLabel6)
																				.addGap(18,
																						18,
																						Short.MAX_VALUE)
																				.addComponent(
																						jTextField4,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						30,
																						40))
																.addGroup(
																		layout.createSequentialGroup()
																				.addGroup(
																						layout.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																								.addComponent(
																										jLabel3)
																								.addComponent(
																										jLabel4)
																								.addComponent(
																										jLabel2)
																								.addComponent(
																										jLabel5))
																				.addGap(18,
																						18,
																						18)
																				.addGroup(
																						layout.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING,
																								false)
																								.addComponent(
																										jTextField1)
																								.addComponent(
																										jTextField2,
																										javax.swing.GroupLayout.DEFAULT_SIZE,
																										169,
																										Short.MAX_VALUE)
																								.addComponent(
																										jTextField3)
																								.addComponent(
																										lbl_Temp)))))
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(43, 43, 43)
								.addComponent(jLabel1)
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(
														jTextField1,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel3)
												.addComponent(
														jTextField2,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jLabel4)
												.addComponent(
														jTextField3,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel5)
												.addComponent(
														lbl_Temp,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lbl_PrevTask)
												.addComponent(
														spinner,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														spinner2,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
																
									.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														spinner3,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))	
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lbl_RespMember)
												.addComponent(
														memberList,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel6)
												.addComponent(
														jTextField4,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										17, Short.MAX_VALUE)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jButton1)
												.addComponent(jButton2)
										)
								
												
								.addGap(27, 27, 27)));
								
	}
	private int getParentTaskId(String predecessor){
		String idString = "";
			int m = 1;
				while (predecessor.charAt(m) != ']'){		
					idString += predecessor.charAt(m);
						++m;
					}
				
		return Integer.parseInt(idString);
	}
}

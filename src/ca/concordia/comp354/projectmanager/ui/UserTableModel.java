package ca.concordia.comp354.projectmanager.ui;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ca.concordia.comp354.domain.User;

public class UserTableModel extends DefaultTableModel {

	FrameMaster frameMaster;
	ArrayList<User> users;
	public static int selectedColumn;

	public UserTableModel(FrameMaster fm, ArrayList<User> u) {
		frameMaster = fm;
		users = u;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Object getValueAt(int row, int col) {
		User user = users.get(row);

		if (col == 0) {
			System.out.println("Current Id of task = " + user.getId());
			return user.getId();
		}
		if (col == 1) {
			return user.getName();
		}
		if (col == 2) {
			return user.getEmail();
		}

		if (col == 3) {
			return null;
		}
		if (col == 4) {
			return null;
		}
		System.out.println("got user = " + user);

		return null;
	}

	public class ColumnListener implements ListSelectionListener {

		private final JTable table;

		public ColumnListener(JTable table) {
			this.table = table;
		}

		@Override
		public void valueChanged(ListSelectionEvent event) {
			if (event.getValueIsAdjusting()) {
				return;
			}
			selectedColumn = table.getSelectedRow();

		}
	}

}

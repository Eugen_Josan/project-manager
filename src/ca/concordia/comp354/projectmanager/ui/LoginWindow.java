package ca.concordia.comp354.projectmanager.ui;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import ca.concordia.comp354.authenticate.AuthenticateStatus.Status;
import ca.concordia.comp354.authenticate.Login;
import ca.concordia.comp354.domain.LoginUserSingleton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/***
 * @author Francisco Guerreiro
 * @date modified: 05/14/2014
 * 
 *       This is the class for the Login Panel UI
 */
public class LoginWindow extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2955324728328922673L;
	private static final Logger logger = Logger.getLogger(LoginWindow.class.getName());
	// All components must be set as global variables
	private static JLabel loginLabel;
	private static JTextField loginField;
	private static JPasswordField passwordField;
	private static JLabel passwordLabel;

	// reference for the Frame Container
	FrameMaster frameMaster;

	/**
	 * Creates Login Pane and initialize components
	 * 
	 * @param fm
	 *            - the parent frame, which is the Frame Master holding this
	 *            window
	 */
	public LoginWindow(FrameMaster fm) {
		
		this.frameMaster = fm;
		initComponents();
		setLoggerLevel("");
	}
	
	private static void setLoggerLevel(String s){
		if (s.equals("fine")){
			logger.setLevel(Level.FINE);
		} else if (s.equals("info")){
			logger.setLevel(Level.INFO);
		} else {
			logger.setLevel(Level.SEVERE);
		}
	}

	protected void initComponents() {
		
		// initialize components
		logger.log(Level.INFO, " Initializing Components");
		
		// creates a sub-pane for the buttons
		JComponent buttonPane = createButtonPanel();

		// Create and populate the panel.
		JPanel pnl_Login = new JPanel(new SpringLayout());
		
		//add the logo to the login window
		BufferedImage myPicture = null;
		try {
			myPicture = ImageIO.read(new File("assets/logo.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
		
		GridBagConstraints gbc_picLabel = new GridBagConstraints();
		gbc_picLabel.insets = new Insets(0, 0, 5, 5);
		gbc_picLabel.gridx = 1;
		gbc_picLabel.gridy = 0;
		add(picLabel, gbc_picLabel);

		// ==== LOGIN COMPONENT ==== //
		loginLabel = new JLabel("Login:", JLabel.TRAILING);
		pnl_Login.add(loginLabel);
		loginField = new JTextField(10);
		loginField.setName("login");
		loginLabel.setLabelFor(loginField);
		pnl_Login.add(loginField);
		// ==== end LOGIN COMPONENT ==== //

		// ==== PASSWORD COMPONENT ==== //
		passwordLabel = new JLabel("Enter the password: ", JLabel.TRAILING);
		pnl_Login.add(passwordLabel);
		passwordField = new JPasswordField(10);
		passwordField.setName("password");
		passwordLabel.setLabelFor(passwordField);
		pnl_Login.add(passwordField);
		// ==== end PASSWORD COMPONENT ==== //

		// Lay out the panel.
		SpringUtilities.makeCompactGrid(pnl_Login, 2, 2, // rows, cols
				6, 6, // initX, initY
				6, 6); // xPad, yPad

		pnl_Login.setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Lay out everything.
		GridBagLayout layout = new GridBagLayout();
		layout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0};
		this.setLayout(layout);
		GridBagConstraints gbc_pnl_Login = new GridBagConstraints();
		gbc_pnl_Login.insets = new Insets(0, 0, 5, 5);
		gbc_pnl_Login.gridx = 2;
		gbc_pnl_Login.gridy = 0;
		this.add(pnl_Login, gbc_pnl_Login);
		GridBagConstraints gbc_pnl_buttons = new GridBagConstraints();
		gbc_pnl_buttons.insets = new Insets(0, 0, 5, 0);
		gbc_pnl_buttons.gridx = 3;
		gbc_pnl_buttons.gridy = 0;
		this.add(buttonPane, gbc_pnl_buttons);

	}
	protected JComponent createButtonPanel() {

		JPanel pnl_buttons = new JPanel(new GridLayout(1, 1));
		
		logger.fine("Creating Login button");
		JButton btn_login = new JButton("OK");
		btn_login.setName("OK");

		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				logger.log(Level.INFO, " Login btn clicked");
				okButtonActionPerformed(evt);
			}

			private void okButtonActionPerformed(ActionEvent evt) {
				logger.log(Level.INFO, "Login : "
						+ loginField.getText().toString());

				
				if(Login.authenticateUser(loginField.getText().toString(), passwordField.getPassword()).getStatus() == Status.OK)
				{
					logger.log(Level.INFO,
							"Login sucessfull, calling Show Project Window");
					frameMaster.createMenu();
					
					if(LoginUserSingleton.getInstance().isProjectMember()) 
						frameMaster.showTasksMemberWindow(LoginUserSingleton.getInstance().getUserId());	
					else frameMaster.showProjectWindow();
				} 
			else {
					logger.log(Level.INFO, "Login failed");
					JOptionPane.showMessageDialog(null,
							"Invalid User!",
							"Invalid user", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		logger.fine("Creating Help Button");
		JButton helpButton = new JButton("Help");
		helpButton.setName("Help");
		helpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				helpButtonActionPerformed(evt);
			}

			private void helpButtonActionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(null,
						"Try login:admin and password:admin to enter", "Help",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		pnl_buttons.add(btn_login);
		pnl_buttons.setBorder(new EmptyBorder(10, 10, 10, 10));

		return pnl_buttons;
	}

}

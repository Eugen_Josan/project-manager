package ca.concordia.comp354.projectmanager.ui;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;

public class SplashWindow extends JWindow {
	
	    public SplashWindow(String filename, Frame f){
	        super(f);
	        JWindow window = new JWindow();
	        JLabel l = new JLabel(new ImageIcon(filename));
	        window.getContentPane().add(l, SwingConstants.CENTER);
	        Dimension screenSize =
	  	          Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension labelSize = l.getPreferredSize();
	        
	        window.setBounds(screenSize.width/2 - (labelSize.width/2),
            screenSize.height/2 - (labelSize.height/2), 409 , 200);
	        window.setVisible(true);
	        pack();
	        
	    	try {
	    	    Thread.sleep(5000);
	    	} catch (InterruptedException e) {
	    	    e.printStackTrace();
	    	}
	    	window.setVisible(false);
	    }
	    
}
package ca.concordia.comp354.projectmanager.ui;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import projectmanager.listener.ManageProjectsAction;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.Project;

public class TasksMemberWindow extends JPanel {

	// Hold the reference for the Frame Master Container
	FrameMaster frameMaster;

	// Table which will display Projects according to user logged
	private JTable tbl_Tasks;
	// Scroll Pane to display the table in it
	private JScrollPane spn_Tasks;
	ArrayList<Task> tasks;
	Project project;
	private int id;
	private JButton btn_LogOut;

	public TasksMemberWindow(FrameMaster fm, int id) {
		this.id = id;
		frameMaster = fm;
		initComponents();
	}

	public TasksMemberWindow(FrameMaster fm, Project proj) {
		frameMaster = fm;
		project = proj;
		initComponents();
	}

	private void initComponents() {

		BorderLayout layout = new BorderLayout();
		setLayout(layout);

		// initialize components
		spn_Tasks = new JScrollPane();
		tbl_Tasks = new JTable();
		new JLabel();
		new JToggleButton();
		new JButton();
		btn_LogOut = new JButton("Log Out");
		btn_LogOut.addActionListener(new ManageProjectsAction(frameMaster));
		
		tasks = new ArrayList<Task>();
		tasks = Task.getAllTasksByCondition(id, 2);		
		
		// Creating the table
		tbl_Tasks = new JTable();
		tbl_Tasks = showTableData(tbl_Tasks);
		
		spn_Tasks.setViewportView(tbl_Tasks);

		
		// add all the components to this panel
		add(new JLabel("Tasks List"), BorderLayout.NORTH);
		add(spn_Tasks, BorderLayout.CENTER);
	}

	private JTable showTableData(JTable tbl_Tasks2) {

		String[] columnNames = new String[] { "N#", "Title", "Start Date",
				"Due Date", "Status"};

		TaskTableModel model = new TaskTableModel(frameMaster, tasks);
		model.setColumnIdentifiers(columnNames);
	
		tbl_Tasks = new JTable();
		tbl_Tasks.setModel(model);
		
		tbl_Tasks.setFillsViewportHeight(true);
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		tbl_Tasks.getColumnModel().getColumn(0).setMaxWidth(50);

		for(int i = 0; i <  columnNames.length; ++i ){
			if (i!=1) tbl_Tasks.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);
		}
			
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		int id = 0;

		String description = "";
		String start ;
		String end ;
		double progress=0;

		for (int i = 0; i < tasks.size(); i++) {
			id = tasks.get(i).getId();
			description = tasks.get(i).getDescription();
			start = dateFormat.format(tasks.get(i).getStart_Date());
			end = dateFormat.format(tasks.get(i).getEnd_Date());

			model.addRow(new Object[] { id, description, start, end, progress});
		}

		return tbl_Tasks;
	}

}

package ca.concordia.comp354.projectmanager.ui;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.Status;

public class ProjectTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	FrameMaster frameMaster;
	private ArrayList<Project> projects;
	public static int selectedColumn;

	public ProjectTableModel(FrameMaster fm, ArrayList<Project> projectsIn) {
		frameMaster = fm;
		projects = projectsIn;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Object getValueAt(int row, int col) {
		Project project = projects.get(row);

		if (col == 0) {
			System.out.println("Current Id of project = " + project.getId());
			return project.getId();
		}
		if (col == 1) {
			return project.getTitle();
		}
		if (col == 2) {
			return project.getstart_Date();
		}

		if (col == 3) {
			return project.getend_Date();
		}
		if (col == 4) {
			Status tempStatus = new Status(project.getstatus_Id());
			return tempStatus.getName();
		}
		System.out.println("got project = " + project);

		return null;
	}

	public class ColumnListener implements ListSelectionListener {

		private final JTable table;

		public ColumnListener(JTable table) {
			this.table = table;
		}

		@Override
		public void valueChanged(ListSelectionEvent event) {
			if (event.getValueIsAdjusting()) {
				return;
			}
			selectedColumn = table.getSelectedRow();

		}
	}

}

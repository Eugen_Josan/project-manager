package ca.concordia.comp354.projectmanager.ui;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import ca.concordia.comp354.authenticate.Login;
import ca.concordia.comp354.database.DbClass;

public class App {
	private DbClass myDB = null;
	private final Logger logger = Logger.getLogger( App.class.getName() );

	public static void main(String[] args) {
		
		SplashWindow sp = new SplashWindow("assets/logo.png", new JFrame());
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					
					UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceDustCoffeeLookAndFeel");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				// calls a new instance of this class and runs constructor,
				// initializing everything else
				new FrameMaster();
			}
		});
	}
	
	
	
	
	/**
	 * This method is used to automatically populate the database with the right data. 
	 * TODO: store in a file
	 * @throws FileNotFoundException 
	 */
	public void dataForDB() throws FileNotFoundException{		
		
		try {
			this.myDB = new DbClass();
		} catch (Exception e) {
			logger.log( Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		};
		
		Scanner scan = new Scanner(new FileReader("assets/scriptForDB.txt"));
		String query = "";
		
		while(scan.hasNextLine()){
			query += scan.nextLine();
		}
				
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 
		
		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Project inserted into database");

			} else {
				logger.warning("There was an error inserting new project into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
	}	
	
}

package ca.concordia.comp354.projectmanager.ui;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import PERT.PERTAnalysis;

public class PERTFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8663205145550528462L;
	private JLabel lbl_title;
	private JScrollPane scroll_pane;
	private JLabel status;
	private JTextArea textArea;

	final static String NEW_LINE = "\n";
	final static String TITLE_WINDOW = "PERT ANALYSIS";
	final static String FONT_SERIF = "Serif";

	private PERTAnalysis pert;
	private ArrayList<String> pertData;

	public PERTFrame(int projectId) {

		pert = new PERTAnalysis(projectId);
		pert.BuildPERTNetwork();
		pertData = pert.gatherData();

		initComponents();
	}

	private void initComponents() {
		textArea = new JTextArea();
		status = new JLabel();
		lbl_title = new JLabel();

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("PERT Analysis");

		textArea.setColumns(20);
		textArea.setLineWrap(true);
		textArea.setRows(5);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		scroll_pane = new JScrollPane(textArea);

		lbl_title.setText(TITLE_WINDOW);
		lbl_title.setFont(new Font(FONT_SERIF, Font.ITALIC, 18));
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		// Create a parallel group for the horizontal axis
		ParallelGroup hGroup = layout
				.createParallelGroup(GroupLayout.Alignment.LEADING);

		// Create a sequential and a parallel groups
		SequentialGroup h1 = layout.createSequentialGroup();
		ParallelGroup h2 = layout
				.createParallelGroup(GroupLayout.Alignment.TRAILING);

		// Add a container gap to the sequential group h1
		h1.addContainerGap();

		// Add a scroll pane and a label to the parallel group h2
		h2.addComponent(scroll_pane, GroupLayout.Alignment.LEADING,
				GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE);
		h2.addComponent(status, GroupLayout.Alignment.LEADING,
				GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE);

		// Create a sequential group h3
		SequentialGroup h3 = layout.createSequentialGroup();
		h3.addComponent(lbl_title);
		h3.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);

		for (int i = 0; i < pertData.size(); i++) {
			int nr = i + 1;
			String cpa = nr + ") " + pertData.get(i);
			textArea.append(cpa + NEW_LINE);
		}

		textArea.setFont(new Font(FONT_SERIF, Font.BOLD, 20));
		textArea.setBackground(Color.LIGHT_GRAY);
		

		// Add the group h3 to the group h2
		h2.addGroup(h3);
		// Add the group h2 to the group h1
		h1.addGroup(h2);

		h1.addContainerGap();

		// Add the group h1 to the hGroup
		hGroup.addGroup(GroupLayout.Alignment.TRAILING, h1);
		// Create the horizontal group
		layout.setHorizontalGroup(hGroup);

		// Create a parallel group for the vertical axis
		ParallelGroup vGroup = layout
				.createParallelGroup(GroupLayout.Alignment.LEADING);
		// Create a sequential group v1
		SequentialGroup v1 = layout.createSequentialGroup();
		// Add a container gap to the sequential group v1
		v1.addContainerGap();
		// Create a parallel group v2
		ParallelGroup v2 = layout
				.createParallelGroup(GroupLayout.Alignment.CENTER);
		v2.addComponent(lbl_title, GroupLayout.DEFAULT_SIZE,
				GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE);
		
		// Add the group v2 tp the group v1
		v1.addGroup(v2);
		v1.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED);
		v1.addComponent(scroll_pane, GroupLayout.DEFAULT_SIZE, 233,
				Short.MAX_VALUE);
		v1.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED);
		v1.addComponent(status);
		v1.addContainerGap();

		// Add the group v1 to the group vGroup
		vGroup.addGroup(v1);
		// Create the vertical group
		layout.setVerticalGroup(vGroup);
		pack();
	}
}
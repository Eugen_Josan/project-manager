package ca.concordia.comp354.projectmanager.ui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import projectmanager.listener.ManageProjectsAction;
import projectmanager.listener.ManageTasksAction;
import projectmanager.listener.ManageUsersAction;
import ca.concordia.comp354.domain.LoginUserSingleton;
import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.Task;

/***
 * @author Francisco Guerreiro
 * @date modified: 14/05/2014
 * 
 *       This is the Frame which will be the Container for all the other Panels.
 *       For this, it will have a JPanel which will be manipulating the current
 *       Panels.
 * 
 *       * Frames are view components which could be thought as 'windows'. *
 *       Panels are modal view components which could be thought as the current
 *       view inside that Window/Frame.
 * 
 *       Each Panel will have its unique class, designed by me as <panel>Window.
 *       Examples of classes for panels: - LoginWindow - ProjectWindow -
 *       TaskWindow - GantWindow - AboutUsWindow - HelpWindow
 * 
 *       Each one of these contains the layout for a specific view in the GUI.
 * 
 */
public class FrameMaster extends JFrame {

	private static final long serialVersionUID = -5221345502029887681L;
	private static final Logger logger = Logger.getLogger(FrameMaster.class
			.getName());

	// The Panel Container which will manage the current Panel view
	JPanel pnl_Container = new JPanel();
	// Constant to map the LoginWindow Panel to the key "loginWindow"
	public static final String LOGIN = "loginWindow";
	// Constant to map the LoginWindow Panel to the key "projectWindow"
	public static final String PROJECTS = "projectWindow";
	// Constant to map the LoginWindow Panel to the key "projectWindow"
	public static final String TASKS = "taskWindow";
	public static final String TASKSMEMBER = "taskMemberWindow";
	public static final String ADD_TASK = "addTaskWindow";
	public static final String MODIFY_TASK = "modifyTaskWindow";
	public static final String ADD_PROJECT = "addProjectWindow";
	public static final String ADD_USER = "addUserWindow";
	public static final String ASSIGN_USER = "assign_user";
	public static final String ESTIMATE_TASK = "estimate_task";

	// Reference of the login panel
	private LoginWindow pnl_Login;
	// Reference of the project panel
	private ProjectWindow pnl_Project;
	// Reference of the task panel
	private TasksWindow pnl_Task;
	private TasksMemberWindow pnl_TaskMember;
	// REference..
	private AssignUserToTaskWindow pnl_AssignUserToTask;
	
	private AddTaskWindow pnl_AddTask;
	
	private ModifyTaskWindow pnl_ModifyTask;

	private AddUserWindow pnl_AddUser;

	ArrayList<Project> projects;
	Task task;

	// That is the layout used by the pnl_Container manager to manipulate other
	// panels
	// Note that panel inside a panel can have a different layout
	//
	CardLayout cl = new CardLayout();

	private AddProjectWindow pnl_AddProject;

	/**
	 * Creates the Frame Master, which will hold the other panels unto it
	 */
	public FrameMaster() {
		projects = new ArrayList<Project>(0);
		setTitle("Skhema Project Management");
		// Method used to initialize all the components in the current views

		// Other Panels are instantiated, passing this FrameMaster class as
		// parameter
		// reason-> FrameMaster must be an active reference within all Panels to
		// work properly
		pnl_Login = new LoginWindow(this);
		pnl_Project = new ProjectWindow(this);
		pnl_Task = new TasksWindow(this);
		pnl_AddTask = new AddTaskWindow(this);
		pnl_AddProject = new AddProjectWindow(this);
		pnl_AddUser = new AddUserWindow(this);

		pnl_Login.setName("loginMenu");
		pnl_Project.setName("projectMenu");
		pnl_Task.setName("taskMenu");

		// sets Layout of the pnl_Container to CardLayout
		pnl_Container.setLayout(cl);

		// Adds to the pnl_Container the other 2 existent panels, mapping them
		// to the keys
		// note -> Keys are necessary when we want to tell java Swing to switch
		// between panels
		pnl_Container.add(pnl_Login, LOGIN);
		pnl_Container.add(pnl_Project, PROJECTS);
		pnl_Container.add(pnl_Task, TASKS);
		pnl_Container.add(pnl_AddTask, ADD_TASK);
		pnl_Container.add(pnl_AddProject, ADD_PROJECT);
		pnl_Container.add(pnl_AddUser, ADD_USER);

		// tell the layout to show the Login Panel
		cl.show(pnl_Container, LOGIN);

		// add the pnl_Container to this Frame class
		this.add(pnl_Container);

		// Sets the close operation of the Window
		// -> further we could set a message "Are you sure you want to leave"

		this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		this.setSize(1024, 868);
		// Tell the Swing that we want the window to arrange itself by the size
		// of its components
		this.pack();
		// set the frame visible
		this.setVisible(true);
		setLoggerLevel("");
	}
	
	private static void setLoggerLevel(String s){
		if (s.equals("fine")){
			logger.setLevel(Level.FINE);
		} else if (s.equals("info")){
			logger.setLevel(Level.INFO);
		} else {
			logger.setLevel(Level.SEVERE);
		}
	}

	/**
	 * Creates visually the menu bars #TODO: - actions performed by each click -
	 * Validate Menu Design/Buttons with stakeholders (you guys)
	 */
	protected void createMenu() {

		JMenuBar menuBar = new JMenuBar();
		JMenu menu;
		JMenuItem menuItem;

		menuBar.setName("menuBar");
		menuBar.setOpaque(true);
		menuBar.setBackground(new Color(192, 192, 192));
		menuBar.setPreferredSize(new Dimension(200, 30));

		if (LoginUserSingleton.getInstance().isProjectManager()) {
			// Build the first menu - 'PROJECT'

			menu = new JMenu("Project");
			menu.setName("Project");
			// Set a shortcutKey for this menu option
			menu.setMnemonic(KeyEvent.VK_P);
			menuBar.add(menu);

			// An item inside the Project Menu - New
			menuItem = new JMenuItem("Create Projects", KeyEvent.VK_C);
			menuItem.setName("View Projects");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
					ActionEvent.CTRL_MASK));
			menuItem.addActionListener(new ManageProjectsAction(this));
			menu.add(menuItem);

			// An item inside the Project Menu - Generate Gant Chart
			menuItem = new JMenuItem("Generate Gant Chart", KeyEvent.VK_G);
			menuItem.setName("Generate Gant Chart");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,
					ActionEvent.CTRL_MASK));
			menuItem.addActionListener(new ManageProjectsAction(this));

			menu.add(menuItem);
			// An item inside the Project Menu - Earned Value Analysis
			menuItem = new JMenuItem("Earned Value Analysis", KeyEvent.VK_E);
			menuItem.setName("Earned Value Analysis");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
					ActionEvent.CTRL_MASK));
			menuItem.addActionListener(new ManageProjectsAction(this));
			menu.add(menuItem);

			// An item inside the Project Menu - Critical Path Analysis
			menuItem = new JMenuItem("Critical Path Analysis", KeyEvent.VK_C);
			menuItem.setName("Critical Path Analysis");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
					ActionEvent.CTRL_MASK));

			menuItem.addActionListener(new ManageProjectsAction(this));
			menu.add(menuItem);

			// An item inside the Project Menu - PERT Analysis
			menuItem = new JMenuItem("PERT Analysis", KeyEvent.VK_P);
			menuItem.setName("PERT Analysis");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,
					ActionEvent.CTRL_MASK));
			menuItem.addActionListener(new ManageProjectsAction(this));
			menu.add(menuItem);

			// Build the second menu - 'Tasks'
			menu = new JMenu("Tasks");
			menu.setMnemonic(KeyEvent.VK_T);
			menu.getAccessibleContext().setAccessibleDescription("Tasks");
			menu.setName("Tasks");
			menuBar.add(menu);

			// An item inside the Tasks Menu - Create Task
			menuItem = new JMenuItem("Create Task", KeyEvent.VK_C);
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T,
					ActionEvent.CTRL_MASK));
			menuItem.setName("Create Task");
			menuItem.addActionListener(new ManageTasksAction(this, pnl_Task));
			menu.add(menuItem);

			// An item inside the Tasks Menu - Tasklist
			menuItem = new JMenuItem("Tasklist", KeyEvent.VK_L);
			menuItem.setName("Tasklist");
			menuItem.addActionListener(new ManageTasksAction(this, pnl_Task));
			menu.add(menuItem);

			// An item inside the Project Menu - PERT Analysis
			menuItem = new JMenuItem("Estimate Time for PERT", KeyEvent.VK_S);
			menuItem.setName("Estimate Time for PERT");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
					ActionEvent.CTRL_MASK));
			menuItem.addActionListener(new ManageTasksAction(this, pnl_Task));
			menu.add(menuItem);
		}

		// IF USER IS ADMIN HE/SHE SHOULD ONLY SEE ADD AND EDIT USER
		// IF USER IS PM HE/SHE SHOULD ONLY SEE ASSIGN TASK
		// IF USER IS MEMBER HE/SHE SHOULD NOT SEE THIS MENU
		if (LoginUserSingleton.getInstance().isAdmin()) {
			// Build the third menu - 'Users'
			menu = new JMenu("Users");
			menu.setName("Users");
			menu.setMnemonic(KeyEvent.VK_U);
			menuBar.add(menu);

			// An item inside the Users Menu - Add
			menuItem = new JMenuItem("Add", KeyEvent.VK_A);
			menuItem.setName("Add");
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
					ActionEvent.CTRL_MASK));
			menuItem.addActionListener(new ManageUsersAction(this));
			menu.add(menuItem);

			menuItem = new JMenuItem("Edit");
			menuItem.setName("Edit");
			menuItem.addActionListener(new ManageUsersAction(this));
			menu.add(menuItem);
		}

		// Build the fourth menu - 'Help'
		menu = new JMenu("Help");
		menu.setMnemonic(KeyEvent.VK_H);
		menu.getAccessibleContext().setAccessibleDescription("About the Team");
		menu.setName("Help");
		menuBar.add(menu);

		// An item inside the Help Menu - About Us
		menuItem = new JMenuItem("About Us", KeyEvent.VK_A);
		menuItem.setName("About Us");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
				ActionEvent.CTRL_MASK));
		menuItem.addActionListener(new ManageProjectsAction(this));
		menu.add(menuItem);

		menuItem = new JMenuItem("Log out");
		menuItem.setName("Log out");
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				ActionEvent.CTRL_MASK));
		menuItem.addActionListener(new ManageUsersAction(this));
		menu.add(menuItem);

		// Set the menu bar and add the label to the content pane.
		this.setJMenuBar(menuBar);
	}

	public void showProjectWindow() {
		logger.log(Level.INFO, "CardLayout .show projectWindow");
		cl.show(pnl_Container, "projectWindow");
	}

	public void showLoginWindow() {
		logger.log(Level.INFO, "CardLayout .show loginWindow");
		// cl.show(pnl_Container, "loginWindow");
		// close existing Frame and open a new one in order to reload all data
		// /security reasons
		this.dispose();
		new FrameMaster();
	}

	// call this method only from ManageTaskAction otherwise the application
	public boolean setUpTaskWindow() {
		return pnl_Project.showTaskListWindowByProjectId();
	}

	public void showTasksWindow() {
		logger.log(Level.INFO, "CardLayout .show taskWindow");
		cl.show(pnl_Container, "taskWindow");
	}

	public void showAddTaskWindow() {
		if (pnl_Project.getProjectId() > 0) {

			Project project = new Project(pnl_Project.getProjectId());
			logger.log(Level.INFO, "CardLayout .show addTask");
			pnl_AddTask = new AddTaskWindow(this, project, null);
			pnl_Container.add(pnl_AddTask, ADD_TASK);
			cl.show(pnl_Container, "addTaskWindow");
		} else {
			JOptionPane
					.showMessageDialog(
							null,
							"Nothing has been selected. "
									+ "Please one click the row you are interested to add a task.");
		}
	}

	public void showModifyTaskWindow() {
		if (pnl_Task.getTaskId() > 0) {

			Task task = Task.getTaskById(pnl_Task.getTaskId());
			pnl_ModifyTask = new ModifyTaskWindow(this, task);
			pnl_Container.add(pnl_ModifyTask, MODIFY_TASK);
			cl.show(pnl_Container, "modifyTaskWindow");
		} else {
			JOptionPane
					.showMessageDialog(
							null,
							"Nothing has been selected. "
									+ "Please one click the row you are interested to add a task.");
		}
	}

	public void showAssignUsersToTaskWindow(Project project, Task tsk) {
		task = tsk;
		logger.log(Level.INFO, "CardLayout .show assignUserToTask");
		pnl_AssignUserToTask = new AssignUserToTaskWindow(this, task, project);
		pnl_Container.add(pnl_AssignUserToTask, ASSIGN_USER);
		cl.show(pnl_Container, ASSIGN_USER);
	}

	public void showAddProjectWindow() {
		pnl_AddProject = new AddProjectWindow(this);
		pnl_Container.add(pnl_AddProject, ADD_PROJECT);
		cl.show(pnl_Container, ADD_PROJECT);
	}

	public void setProjectWindow(ProjectWindow pw) {
		pnl_Project = pw;
		pnl_Container.add(pnl_Project, PROJECTS);
	}

	public void setTasksWindow(TasksWindow tw) {
		pnl_Task = tw;
		pnl_Container.add(pnl_Task, TASKS);

	}

	public void setAddUserWindowForAdd(boolean addOption) {

		pnl_AddUser.initDataWithUserBeenAdded(addOption);
		cl.show(pnl_Container, ADD_USER);
	}

	public void setAssignUserToTaskWindow(AssignUserToTaskWindow myWindow) {
		pnl_AssignUserToTask = myWindow;
		pnl_Container.add(pnl_AssignUserToTask, ASSIGN_USER);
	}

	private static void initLookAndFeel() {
		// lnfName is the look and feel name
		// frame is the window's JFrame
		// try/catch block is omitted here
		// UIManager.setLookAndFeel(lnfName);
		// SwingUtilities.updateComponentTreeUI(frame);
		// frame.pack();

		// javax.swing.plaf.metal.MetalLookAndFeel
		// UIManager.getSystemLookAndFeelClassName()
		// com.sun.java.swing.plaf.motif.MotifLookAndFeel
		// UIManager.getCrossPlatformLookAndFeelClassName()
		// javax.swing.plaf.nimbus.NimbusLookAndFeel

		// SubstanceAutumnLookAndFeel
		// SubstanceBusinessBlackSteelLookAndFeel
		// org.pushingpixels.substance.api.skin.SubstanceTwilightLookAndFeel
		//

		String lookAndFeel = null;
		try {
			UIManager.setLookAndFeel(lookAndFeel);

		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (InstantiationException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			
			e.printStackTrace();
		}

	}

	/**
	 * @return the clicked Project id
	 */
	public int getClickedProjectId() {
		return pnl_Project.getProjectId();
	}

	public void showTasksMemberWindow(int id) {
		logger.log(Level.INFO, "CardLayout .show taskMemberWindow");
		pnl_TaskMember = new TasksMemberWindow(this, id);
		pnl_Container.add(pnl_TaskMember, TASKSMEMBER);
		this.add(pnl_Container);

		cl.show(pnl_Container, "taskMemberWindow");

	}

	public int getTaskClicked() {
		return pnl_Task.getTaskId();
	}

}

package ca.concordia.comp354.projectmanager.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ca.concordia.comp354.domain.Task;

public class EstimateTaskFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8663205145550528462L;
	final static String NEW_LINE = "\n";
	final static String TITLE_WINDOW = "Estimate Task Duration";
	final static String FONT_SERIF = "Serif";

	private Task task;

	JPanel panel;

	public EstimateTaskFrame(int taskId) {
		task = Task.getTaskById(taskId);
		this.setSize(350, 300);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		panel = new JPanel();
		this.add(panel);
		placeComponents(panel);

		this.setVisible(true);
	}

	private void placeComponents(JPanel panel2) {
		panel.setLayout(null);
		this.setTitle("Estimate Duration of Task");

		if (task != null) {
			this.setTitle("Estimate Duration of Task: " + task.getDescription());
		}

		JLabel lbl_Optimistic = new JLabel("Optimistic Duration");
		lbl_Optimistic.setBounds(10, 10, 140, 25);
		panel.add(lbl_Optimistic);

		final JTextField txt_Optimistic = new JTextField(3);
		txt_Optimistic.setBounds(10, 35, 25, 25);
		panel.add(txt_Optimistic);

		JLabel lbl_days = new JLabel("days");
		lbl_days.setBounds(40, 35, 140, 25);
		panel.add(lbl_days);

		JLabel lbl_Pessimistic = new JLabel("Pessimistic Duration");
		lbl_Pessimistic.setBounds(10, 60, 140, 25);
		panel.add(lbl_Pessimistic);

		final JTextField txt_Pessimistic = new JTextField(3);
		txt_Pessimistic.setBounds(10, 90, 25, 25);
		panel.add(txt_Pessimistic);

		lbl_days = new JLabel("days");
		lbl_days.setBounds(40, 90, 140, 25);
		panel.add(lbl_days);

		JLabel lbl_MostLikely = new JLabel("Most Likely Duration");
		lbl_MostLikely.setBounds(10, 120, 140, 25);
		panel.add(lbl_MostLikely);

		final JTextField txt_MostLikely = new JTextField(3);
		txt_MostLikely.setBounds(10, 150, 25, 25);
		panel.add(txt_MostLikely);

		lbl_days = new JLabel("days");
		lbl_days.setBounds(40, 150, 140, 25);
		panel.add(lbl_days);

		JButton btn_Save = new JButton("Save");
		btn_Save.setBounds(10, 190, 140, 25);
		panel.add(btn_Save);

		btn_Save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					saveButtonActionPerformed(e);
				} catch (ParseException e1) {
					e1.printStackTrace();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

			private void saveButtonActionPerformed(ActionEvent evt)
					throws Exception {
				
				String optimistic_duration = txt_Optimistic.getText()
						.toString().trim();
				int parsed_str = Integer.parseInt(optimistic_duration);
				task.setOptimistic_duration(parsed_str);

				String pessimistic_duration = txt_Pessimistic.getText()
						.toString().trim();
				parsed_str = Integer.parseInt(pessimistic_duration);
				task.setPessimistic_duration(parsed_str);
				
				String mostLikely_duration = txt_MostLikely.getText()
						.toString().trim();
				parsed_str = Integer.parseInt(mostLikely_duration);
				task.setMostLikely_duration(parsed_str);
				

				show_test_msg("Saved it");

				close_window();
			}
		});

	}

	protected void close_window() {
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	public void show_test_msg(String msg) {
		JOptionPane.showMessageDialog(null, msg);

	}
}
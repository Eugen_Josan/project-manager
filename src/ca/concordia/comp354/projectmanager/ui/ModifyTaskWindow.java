package ca.concordia.comp354.projectmanager.ui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.Project;

public class ModifyTaskWindow extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6694530268994659271L;

	// reference for the Frame Container
	private FrameMaster frameMaster;
	private Task task;
	private Project project;

	public ModifyTaskWindow(FrameMaster fm) {
		frameMaster = fm;
		initComponents2();
	}

	public ModifyTaskWindow(FrameMaster fm, Task task) {
		frameMaster = fm;
		this.task = task;
		project = new Project(task.getProjectId());
		initComponents2();
	}

	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	//private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JTextField jTextField1;
	//private javax.swing.JTextField jTextField2;
	private javax.swing.JTextField jTextField3;
	private javax.swing.JTextField jTextField4;
	private javax.swing.JTextField jTextField5;
	private JSpinner spinner;
	private JSpinner spinner2;
	private JSpinner spinner3;
	private JComboBox memberList;
	
	private String[] previousTasks;
	private void initComponents2() {
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jTextField1 = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();
		jTextField3 = new javax.swing.JTextField();
		jTextField4 = new javax.swing.JTextField();
		jTextField5 = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		jButton1 = new javax.swing.JButton();
		jButton2 = new javax.swing.JButton();

		if (task != null) {

			jLabel1.setText("Modifying Task: " + task.getDescription());
		}

		jLabel2.setText("Description");

		
		//display default current day 
		DateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd");	
		jTextField1.setText(task.getDescription());
		jTextField3.setText(String.valueOf((int)task.getTime_Spent()));
		jTextField4.setText(String.valueOf(task.getPV()));
		jTextField5.setText(String.valueOf(task.getAC()));

		jLabel4.setText("Time spent, in days");
		
		jLabel5.setText("");
		
		jButton1.setText("Save");
		
		jLabel6.setText("Planned Value");
		jLabel7.setText("Actual Cost");
		
		
		
		jButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					saveButtonActionPerformed(e);
				} catch (ParseException e1) {
				
					e1.printStackTrace();
				} catch (Exception e1) {
					
					e1.printStackTrace();
				}
			}

			private void saveButtonActionPerformed(ActionEvent evt) throws Exception {
				//converting text fields to Date format for project creation
				//@author Eugeniu Josan 6632882 (modified 2014/06/26)

			
				float spent = Float.parseFloat(jTextField3.getText().toString());

				
				String descriptionTxt = jTextField1.getText().toString();
				float planned_value = Float.parseFloat(jTextField4.getText().toString());
				float actual_cost = Float.parseFloat(jTextField5.getText().toString());
				
				
				if (descriptionTxt != task.getDescription()) {
					task.setDescription(descriptionTxt);
				}
				
				if (spent > task.getTime_Required()){
					task.setTime_Spent(task.getTime_Required());
				}else{
					if (spent != task.getTime_Spent()) {
						task.setTime_Spent(spent);
					}
				}
				if (planned_value != task.getPV()) {
					task.setPV(planned_value);
				}
				if (actual_cost != task.getAC()) {
					task.setAC(actual_cost);
				}
				
				TasksWindow tw = new TasksWindow(frameMaster, project);
				frameMaster.setTasksWindow(tw);

				frameMaster.showTasksWindow();
			}			
		});

		jButton2.setText("Go Back");
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				TasksWindow tw = new TasksWindow(frameMaster, project);
				frameMaster.setTasksWindow(tw);
		
				frameMaster.showTasksWindow();
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						javax.swing.GroupLayout.Alignment.LEADING,
						layout.createSequentialGroup()
								.addContainerGap(134, Short.MAX_VALUE)
								.addComponent(jLabel1).addGap(125, 125, 125))
				.addGroup(
						layout.createSequentialGroup()
								.addGap(71, 71, 71)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		jButton1,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		106,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		jButton2,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		117,
																		javax.swing.GroupLayout.PREFERRED_SIZE))
												.addGroup(
														layout.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																false)
													
																.addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						jLabel6)
																				.addGap(18,
																						18,
																						Short.MAX_VALUE)
																				.addComponent(
																						jTextField4,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						30,
																						40))
															    .addGroup(
																		layout.createSequentialGroup()
																				.addComponent(
																						jLabel7)	
																				.addGap(18,
																						18,
																						Short.MAX_VALUE)
																				.addComponent(
																						jTextField5,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						30,
																						40))
																.addGroup(
																		layout.createSequentialGroup()
																				.addGroup(
																						layout.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																								
																								.addComponent(
																										jLabel4)
																								.addComponent(
																										jLabel2)
																								.addComponent(
																										jLabel5))
																				.addGap(18,
																						18,
																						18)
																				.addGroup(
																						layout.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING,
																								false)
																								.addComponent(
																										jTextField1,
																										javax.swing.GroupLayout.DEFAULT_SIZE,
																										169,
																										Short.MAX_VALUE)
																								
																								.addComponent(
																										jTextField3)
																								))))
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(43, 43, 43)
								.addComponent(jLabel1)
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(
														jTextField1,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
							
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jLabel4)
												.addComponent(
														jTextField3,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel5))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel6)
												.addComponent(
														jTextField4,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel7)
												.addComponent(
														jTextField5,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										17, Short.MAX_VALUE)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jButton1)
												.addComponent(jButton2)
										)
								
												
								.addGap(27, 27, 27)));
								
	}
}

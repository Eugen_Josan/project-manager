package ca.concordia.comp354.projectmanager.ui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;

import projectmanager.listener.ManageProjectsAction;
import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.Status;

//import ca.concordia.comp354.projectmanager.listener.ManageTasksAction;
// User ID = 1 admin, 2 manager, 3 member, 4 guest

/***
 * 
 * @author Francisco Guerreiro
 * @date modified: 05/14/2014
 * 
 *       This is the class of the Project Panel
 */
public class ProjectWindow extends JPanel {

	private static final long serialVersionUID = -7104792895455815545L;
	// All components must be set as global variables
	// the Button which will switch to the Task Panel - not working yet
	private JButton btn_ViewTasks;
	// Table which will display Projects according to user logged
	private JTable tbl_Projects;
	// Button to Log Out
	private JToggleButton btn_LogOut;
	// Scroll Pane to display the table in it
	private JScrollPane spn_Projects;
	// label to show the title of the Pane
	private JLabel lbl_Title;

	private JButton btn_GenerateGant;

	// reference for the Frame Container
	FrameMaster frameMaster;
	ArrayList<Project> projects;
	private int projectId = -1;

	/**
	 * Creates Project Pane and initialize components
	 * 
	 * @param fm
	 *            - the parent frame, which is the Frame Master holding this
	 *            window
	 */
	public ProjectWindow(FrameMaster fm) {
		this.frameMaster = fm;
		
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		projects = getProjectList();
		initComponents();

	}

	private void initComponents() {
		// initialize components
		spn_Projects = new JScrollPane();
		tbl_Projects = new JTable();
		lbl_Title = new JLabel();
		btn_ViewTasks = new JButton();
		// Action performed to go to Task Panel
		btn_ViewTasks.addActionListener(new ManageProjectsAction(frameMaster,
				projects));

		btn_LogOut = new JToggleButton();
		// Creating the table
		tbl_Projects = new JTable();
		tbl_Projects.setName("Table Project");
		btn_ViewTasks.setName("Tasks");

		tbl_Projects = showTableData(tbl_Projects);

		tbl_Projects.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {

				if (e.getClickCount() == 1) {
					final JTable target = (JTable) e.getSource();
					final int row = target.getSelectedRow();
					final int column = 0;
					
					final int _projectId = (Integer) target.getValueAt(row,
							column);
					System.out.println("mouseClicked Got project Id = "
							+ _projectId);
					projectId = _projectId;

				}
				if (e.getClickCount() == 2) {
					final JTable target = (JTable) e.getSource();
					final int row = target.getSelectedRow();
					final int column = 0;
					// Cast to object type
					projectId = (Integer) target.getValueAt(row, column);
					System.out.println("mouseClicked Got project Id = "
							+ projectId);
				}
				if (e.getClickCount() == 2) {
					showTaskListWindowByProjectId();
				}
			}
		});

		// sets the table to viewed in the scroll pane
		spn_Projects.setViewportView(tbl_Projects);

		// spn_Projects.setScrollableHeight(ScrollableSizeHint.STRETCH);

		lbl_Title.setText("Projects");
		btn_ViewTasks.setText("Tasks");

		btn_LogOut.setText("Log out");

		btn_LogOut.addActionListener(new ManageProjectsAction(frameMaster));

		// add all the components to this panel
		add(new JLabel("Projects List - Double Click on ID"),
				BorderLayout.NORTH);
		add(spn_Projects, BorderLayout.CENTER);
	
	}

	public boolean showTaskListWindowByProjectId() {
		if (projectId > 0) {
			Project projectQuery = new Project();
			projectQuery.getProjectFromDB(projectId);

			TasksWindow tw = new TasksWindow(frameMaster, projectQuery);
			frameMaster.setTasksWindow(tw);
			frameMaster.showTasksWindow();
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Nothing has been selected. "
					+ "Please one click the row you are interested to see.");
			return false;
		}
	}

	public int getProjectId() {
		return this.projectId;
	}


	public JTable showTableData(JTable tbl_Projects) {

		String[] columnNames = new String[] { "ID", "Title", "Start Date",
				"Due Date", "Status" };

		ArrayList<Project> projects = new ArrayList<Project>();
		projects = getProjectList();

		ProjectTableModel model = new ProjectTableModel(frameMaster, projects);
		model.setColumnIdentifiers(columnNames);
		tbl_Projects = new JTable();
		tbl_Projects.setModel(model);
		
		tbl_Projects.setFillsViewportHeight(true);
		
		int id = 0;
		String title = "";
		String start = "";
		String end = "";
		int status;

		try {

			Project project = new Project();

			ArrayList list;
			list = (ArrayList) project.getProjectList();

			for (int i = 0; i < list.size(); i++) {

				HashMap<?, ?> map = (HashMap) list.get(i);

				id = (Integer) map.get("id");
				title = (String) map.get("title");
				start = (String) map.get("start_date");
				end = (String) map.get("end_date");

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date startDt = dateFormat.parse(start);
				Date endDt = dateFormat.parse(end);
				status = (Integer) map.get("status_id");
				Status tempStatus = new Status(status);

				model.addRow(new Object[] { id, title, startDt, endDt,
						tempStatus.getName() });
			}

		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return tbl_Projects;
	}

	private ArrayList<Project> getProjectList() {
		
		Project project = new Project();
		ArrayList list;
		list = (ArrayList) project.getProjectList();

		Project new_project = new Project();
		ArrayList<HashMap<?, ?>> list2 = (ArrayList<HashMap<?, ?>>) new_project
				.getProjectList();

		ArrayList<Project> list3 = new ArrayList<Project>(list2.size());

		int id = 0;

		for (int i = 0; i < list2.size(); i++) {
			HashMap<?, ?> map = (HashMap) list2.get(i);
			id = (Integer) map.get("id");
			list3.add(new Project(id));
		}
		return list3;
	}
}

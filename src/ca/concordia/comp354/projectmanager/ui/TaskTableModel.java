package ca.concordia.comp354.projectmanager.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.Status;
import ca.concordia.comp354.domain.User;

public class TaskTableModel extends DefaultTableModel {

	FrameMaster frameMaster;
	ArrayList<Task> tasks;
	public static int selectedColumn;

	public TaskTableModel(FrameMaster fm, ArrayList<Task> t) {
		frameMaster = fm;
		tasks = t;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Object getValueAt(int row, int col) {
		Task task = tasks.get(row);
		SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd yyyy");

		if (col == 0) {
			return row+1;
		}
		if (col == 1) {
			return task.getDescription();
		}
		if (col == 2) {
			return dateFormat.format(task.getStart_Date());
		}

		if (col == 3) {
			return dateFormat.format(task.getEnd_Date());
		}
		if (col == 4) {
			
			Status tempStatus = new Status(task.getStatus_Id());
			return tempStatus.getName();
		}
		
		if (col == 5) {
			String text = "";
			ArrayList<Integer> tempInt = task.getParentTask();
			
			if (tempInt.size() != 0){
				for (int i = 0; i != tempInt.size(); ++i){
					if (i!=tempInt.size()-1) 
						text += Task.getTaskById(tempInt.get(i)).getDescription()+", ";
					else text += Task.getTaskById(tempInt.get(i)).getDescription();
				}
			}else text = "";
			return text; 
		}
		if (col == 6) {
			String text = "";
			System.out.println(task.getId());
			ArrayList<User> tempUser = UserDAO.getMembersAssignedToTask(task.getId());
			
			if(tempUser.size() != 0){
				for (int i = 0; i != tempUser.size(); ++i){
					if (i!=tempUser.size()-1) {
						text += tempUser.get(i).getName()+", ";
					}else{
						text += tempUser.get(i).getName();
					}
				}
			}else text = "Not assigned";
			return text;
		}

		return null;
	}

	public class ColumnListener implements ListSelectionListener {

		private final JTable table;

		public ColumnListener(JTable table) {
			this.table = table;
		}

		@Override
		public void valueChanged(ListSelectionEvent event) {
			if (event.getValueIsAdjusting()) {
				return;
			}
			selectedColumn = table.getSelectedRow();

		}
	}
}

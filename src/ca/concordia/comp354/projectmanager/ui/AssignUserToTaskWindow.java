package ca.concordia.comp354.projectmanager.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;

import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.Project;
import ca.concordia.comp354.domain.User;

public class AssignUserToTaskWindow extends JPanel {

	private javax.swing.JButton btn_assign;
	private javax.swing.JLabel lbl_assign;
	private JScrollPane spn_Users;
	private JTable tbl_Users;
	FrameMaster frameMaster;
	private JToggleButton btn_Back;
	private Task task;
	private Project project;
	ArrayList<User> users;
	
	public AssignUserToTaskWindow(FrameMaster fm, Task tsk, Project proj) {
		frameMaster = fm;
		task = tsk;
		project = proj;
		initComponents();
	}

	private void initComponents() {
		lbl_assign = new javax.swing.JLabel();
		btn_assign = new javax.swing.JButton();
		btn_Back = new javax.swing.JToggleButton();
		
		spn_Users = new JScrollPane();
		users = new ArrayList<User>();
		
		users = UserDAO.getMembersNotAssignedToTaskWithId(task.getId());
		
		lbl_assign.setText("Assign Resorces");
		btn_assign.setText("Assign");
		btn_Back.setText("Go back");
		btn_Back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TasksWindow tw = new TasksWindow(frameMaster, project);
                frameMaster.setTasksWindow(tw);
                frameMaster.showTasksWindow();
        }});
		
			// Creating the table
		tbl_Users = new JTable();
		tbl_Users = showTableData(tbl_Users);
		spn_Users.setViewportView(tbl_Users);
		
		// add all the components to this panel
		add(new JLabel("Tasks List"), BorderLayout.NORTH);
		add(spn_Users, BorderLayout.CENTER);
		JPanel pnl_Buttons = new JPanel();
		pnl_Buttons.setLayout(new BorderLayout());
		pnl_Buttons.add(btn_assign, BorderLayout.NORTH);
		pnl_Buttons.add(btn_Back, BorderLayout.CENTER);
		add(pnl_Buttons, BorderLayout.SOUTH);

		btn_assign.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				int selectedRowIndex = tbl_Users.getSelectedRow();
				int userIdColumn = 0;
				if(selectedRowIndex >= 0){//A row is selected
					int selectedUserId = (Integer) tbl_Users.getValueAt(selectedRowIndex, userIdColumn);
					UserDAO.addUserToTask(selectedUserId, task.getId());
					users = UserDAO.getMembersNotAssignedToTaskWithId(task.getId());
					tbl_Users = showTableData(tbl_Users);
					spn_Users.setViewportView(tbl_Users);
				}
			}
		});
		
	}

	private JTable showTableData(JTable tbl_Tasks2) {
		String[] columnNames = new String[] { "ID", "Name", "Email" };
		UserTableModel model = new UserTableModel(frameMaster, users);
		model.setColumnIdentifiers(columnNames);
		tbl_Users = new JTable();
		tbl_Users.setModel(model);

		tbl_Users.setFillsViewportHeight(true);

		int id = 0;
		String name = "";
		String email = "";

		for (int i = 0; i < users.size(); i++) {
			id = users.get(i).getId();
			name = users.get(i).getName();
			email = users.get(i).getEmail();
			model.addRow(new Object[] { id, name, email });
		}

		return tbl_Users;
	}

}
package ca.concordia.comp354.projectmanager.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import projectmanager.listener.ManageTasksAction;
import projectmanager.listener.ManageUsersAction;
import net.miginfocom.swing.MigLayout;
import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.User;
import ca.concordia.comp354.projectmanager.ui.FrameMaster;

public class AddUserWindow extends JPanel {
	

	private static final long serialVersionUID = 1L;
	private FrameMaster frameMaster;
	private Vector<User> userVector;
	private ItemListener userBoxListener;
	
	private javax.swing.JButton jButtonSave;
	private javax.swing.JButton jButtonBack;
	private javax.swing.JComboBox jComboUser;
	private javax.swing.JComboBox jComboRole;
	private javax.swing.JLabel jLabelEmail;
	private javax.swing.JLabel jLabelLogin;
	private javax.swing.JLabel jLabelName;
	private javax.swing.JLabel jLabelPassword;
	private javax.swing.JLabel jLabelRole;
	private javax.swing.JLabel jLabelUser;
	private javax.swing.JPasswordField jPassword;
	private javax.swing.JTextField jTextFieldEmail;
	private javax.swing.JTextField jTextFieldLogin;
	private javax.swing.JTextField jTextFieldName;
	
	public AddUserWindow(FrameMaster fm){
		this.frameMaster =  fm;
		this.initComponents();
	}
	
	private void initComponents(){
		
		final int capacity = 3;
		Vector<String> roleVector =  new Vector<String>(capacity);
		roleVector.add("Select Role");
		roleVector.add("Project Manager");
		roleVector.add("Project Member");
		
		jComboUser =  new javax.swing.JComboBox();
		
		jComboRole =  new javax.swing.JComboBox(roleVector);
		jLabelEmail =  new javax.swing.JLabel("Email:");
		jLabelLogin =  new javax.swing.JLabel("Login Username:");
		jLabelName =  new javax.swing.JLabel("Name:");
		jLabelPassword =  new javax.swing.JLabel("Password:");
		jLabelRole =  new javax.swing.JLabel("User Role");
		jLabelUser =  new javax.swing.JLabel("User:");
		jPassword =  new javax.swing.JPasswordField();
		jTextFieldEmail = new javax.swing.JTextField();
		jTextFieldLogin = new javax.swing.JTextField();
		jTextFieldName = new javax.swing.JTextField();
		
		jButtonSave = new javax.swing.JButton("Save Info");
		jButtonSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(	jTextFieldEmail.getText().trim().isEmpty() ||
					jTextFieldLogin.getText().trim().isEmpty() ||
					jTextFieldName.getText().trim().isEmpty() ||
					jPassword.getPassword().length == 0 ||
					jComboRole.getSelectedIndex() == 0){
					JOptionPane.showMessageDialog(null, "Please complete the form correctly");
				}
				else{
					try {
						if(saveInfoForUser()){
							JOptionPane.showMessageDialog(null, "DB was correctly modified.");
						}
						else{
							JOptionPane.showMessageDialog(null, "There was a problem with the DB.");
						}
						frameMaster.showProjectWindow();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		
		jButtonBack = new javax.swing.JButton("Go Back");
		jButtonBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				frameMaster.showProjectWindow();
			}
		});
		
		//MigLayout documentation http://www.miglayout.com WAY EASIER THAN JAVA LAYOUT!
		this.setLayout(new MigLayout("",  //Layout Constrains
				"[200] [175] [100]", //Column Constraints
				"[50]")); //Row Constraints
		this.add(jLabelUser, "cell 1 0");// cell column row
		this.add(jComboUser, "cell 2 0, width 150"); // cell column row, widht, otherArguments are separated by ,
		this.add(jLabelName, "cell 1 1");
		this.add(jTextFieldName, "cell 2 1, width 150");
		this.add(jLabelLogin, "cell 1 2");
		this.add(jTextFieldLogin, "cell 2 2, width 150");
		this.add(jLabelEmail, "cell 1 3");
		this.add(jTextFieldEmail, "cell 2 3, width 150");
		this.add(jLabelPassword, "cell 1 4");
		this.add(jPassword, "cell 2 4, width 150");
		this.add(jLabelRole, "cell 1 5");
		this.add(jComboRole, "cell 2 5, width 150");
		this.add(jButtonSave, "cell 1 6");
		this.add(jButtonBack, "cell 2 6");
	}
	
	/**
	 * This method is in charge to fill the form according to the info of the users and wetherer we are creating
	 * a new user o editing an existing one.
	 * @param userIsBeenAdded if an existing user is been edited pass false, otherwise pass true
	 */
	public void initDataWithUserBeenAdded(boolean userIsBeenAdded){
		//fill array of Users
		this.fillTheUserArrayWithTheDBInfo();
		
		//We need to remove the listener before removing all the items from the combobox otherwise we get an exception.
		jComboUser.removeItemListener(userBoxListener);
		jComboUser.removeAllItems();
		
		jComboUser.addItem("New User");
		for(int i = 0; i < userVector.size(); i++){
			jComboUser.addItem(userVector.elementAt(i).getName());
		}
		
		//adds the listener necessary for combobox
		userBoxListener = new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				fillTheFormForUser(jComboUser.getSelectedIndex());
				
			}
		};
		jComboUser.addItemListener(userBoxListener);
		
		if(userIsBeenAdded){
			jComboUser.setSelectedIndex(0);
			fillTheFormForUser(0);
		}
		else{
			jComboUser.setSelectedIndex(1);
			fillTheFormForUser(1);
		}
	}
	
	private void fillTheUserArrayWithTheDBInfo(){
		
		userVector = new Vector<User>();
		int userCont = 2; //starts at 2 because admin is always going to be the first user and it is the only one who cannot be modified
		User auxUser = UserDAO.getUserInfoFromID(userCont);
		while(auxUser != null){
			userVector.add(auxUser);
			userCont++;
			auxUser = UserDAO.getUserInfoFromID(userCont); 
		}
	}
	
	private void fillTheFormForUser(int position){
	
		jPassword.setText("");
		if(position == 0){
			jTextFieldEmail.setText("");
			jTextFieldLogin.setEditable(true);
			jTextFieldLogin.setText("");
			jTextFieldName.setText("");
			jComboRole.setSelectedIndex(0);
		}
		else{
			//If an already existant user is selected, then position from the combobox starts at 1,however
			//because from the vector the user starts with an index 0 we have to substract 1 from position
		
			User auxUser = userVector.elementAt(position - 1);
			jTextFieldEmail.setText(auxUser.getEmail());
			jTextFieldLogin.setEditable(false);
			jTextFieldLogin.setText(auxUser.getLogin());
			jTextFieldName.setText(auxUser.getName());
			//roles are 2 for PManager and 3 for PMember we need to substract 1 from the roleId for the combobox
			jComboRole.setSelectedIndex(auxUser.getRoleId() - 1); 
		}
	}
	
	private boolean saveInfoForUser() throws Exception{

		if(jComboUser.getSelectedIndex() == 0){//create a new user in the DB
			return UserDAO.createNewUser(jTextFieldName.getText(), 
					jTextFieldLogin.getText(), 
					new String(jPassword.getPassword()), 
					jTextFieldEmail.getText(), 
					jComboRole.getSelectedIndex() + 1) > 0;
		}
		else{
			UserDAO.updateUserFromID(jComboUser.getSelectedIndex() + 1, 
					jTextFieldName.getText(), 
					jTextFieldEmail.getText(), 
					new String(jPassword.getPassword()),
					jComboRole.getSelectedIndex() + 1);
			return true;
		}
	}
}

package ca.concordia.comp354.projectmanager.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import projectmanager.listener.ManageTasksAction;
import ca.concordia.comp354.domain.Task;
import ca.concordia.comp354.domain.Project;

public class TasksWindow extends JPanel {

	// Hold the reference for the Frame Master Container
	FrameMaster frameMaster;

	// Table which will display Projects according to user logged
	private JTable tbl_Tasks;
	// Button to Log Out
	private JToggleButton btn_Back;

	private JButton btn_Add;
	private JButton btn_Modify;
	// Scroll Pane to display the table in it
	private JScrollPane spn_Tasks;
	// label to show the title of the Pane
	private JLabel lbl_Title;
	private JLabel lbl_Project;
	ArrayList<Task> tasks;
	Project project;
	private int taskId = -1;
	private int projectId;
	private Task clickedTask;

	public TasksWindow(FrameMaster fm) {

		frameMaster = fm;
		initComponents();
	}

	public TasksWindow(FrameMaster fm, Project proj) {
		frameMaster = fm;
		project = proj;
		projectId = proj.getId();
		initComponents();
	}

	private void initComponents() {

		BorderLayout layout = new BorderLayout();
		setLayout(layout);

		// initialize components
		spn_Tasks = new JScrollPane();
		tbl_Tasks = new JTable();
		lbl_Title = new JLabel();
		btn_Back = new JToggleButton();
		btn_Add = new JButton();
		btn_Modify = new JButton();

		tasks = new ArrayList<Task>();

		lbl_Project = new JLabel();
		lbl_Project.setText("Project ID:");

		if (project != null) {
			lbl_Project.setText("Project ID: _" + project.getId());

			tasks = Task.getAllTasksByCondition(project.getId(), 1);
		}
		btn_Back.addActionListener(new ManageTasksAction(frameMaster, this));
		btn_Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				addTaskButtonActionPerformed(evt);
			}

			private void addTaskButtonActionPerformed(ActionEvent evt) {
				frameMaster.showAddTaskWindow();
				tasks = Task.getAllTasksByCondition(project.getId(), 1);
			}
		});
		btn_Modify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				modifyTaskButtonActionPerformed(evt);
			}

			private void modifyTaskButtonActionPerformed(ActionEvent evt) {
				frameMaster.showModifyTaskWindow();
				tasks = Task.getAllTasksByCondition(project.getId(), 1);
			}
		});
		
		
		

		// Creating the table
		tbl_Tasks = new JTable();
		tbl_Tasks = showTableData(tbl_Tasks);
		tbl_Tasks.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getClickCount() == 1) {
					final JTable target = (JTable) e.getSource();
					final int row = target.getSelectedRow();
					final int column = 0;

					final int order = (Integer) target.getValueAt(row, column) - 1;
					
					taskId = tasks.get(order).getId();
				}
				if (e.getClickCount() == 2) {
					final JTable target = (JTable) e.getSource();
					final int row = target.getSelectedRow();
					final int column = 0;

					final int order = (Integer) target.getValueAt(row, column) - 1;

					assignUserToTaskWindow(tasks.get(order));
				}
			}

			private void assignUserToTaskWindow(Task task) {

				AssignUserToTaskWindow myWindow = new AssignUserToTaskWindow(
						frameMaster, task, project);
				frameMaster.setAssignUserToTaskWindow(myWindow);
				frameMaster.showAssignUsersToTaskWindow(project, task);
			}
		});

		// sets the table to viewed in the scroll pane
		spn_Tasks.setViewportView(tbl_Tasks);

		lbl_Title.setText("Tasks");
		btn_Back.setText("Go back");
		btn_Add.setText("Add");
		btn_Modify.setText("Modify");
		// add all the components to this panel
		add(new JLabel("Tasks List"), BorderLayout.NORTH);
		add(spn_Tasks, BorderLayout.CENTER);
		JPanel pnl_Buttons = new JPanel();
		pnl_Buttons.setLayout(new BorderLayout());
		pnl_Buttons.add(btn_Add, BorderLayout.NORTH);
		pnl_Buttons.add(btn_Modify, BorderLayout.CENTER);
		pnl_Buttons.add(btn_Back, BorderLayout.SOUTH);
		add(pnl_Buttons, BorderLayout.SOUTH);
	}

	private JTable showTableData(JTable tbl_Tasks2) {

		String[] columnNames = new String[] { "N#", "Title", "Start Date",
				"Due Date", "Status", "Predecessors", "Responsable" };

		TaskTableModel model = new TaskTableModel(frameMaster, tasks);
		model.setColumnIdentifiers(columnNames);

		tbl_Tasks = new JTable();
		tbl_Tasks.setModel(model);

		tbl_Tasks.setFillsViewportHeight(true);
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		tbl_Tasks.getColumnModel().getColumn(0).setMaxWidth(50);

		for (int i = 0; i < columnNames.length; ++i) {
			if (i != 1)
				tbl_Tasks.getColumnModel().getColumn(i)
						.setCellRenderer(rightRenderer);
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		int id = 0;

		String description = "";
		String start;
		String end;

		for (int i = 0; i < tasks.size(); i++) {

			id = tasks.get(i).getId();
			description = tasks.get(i).getDescription();
			start = dateFormat.format(tasks.get(i).getStart_Date());
			end = dateFormat.format(tasks.get(i).getEnd_Date());

			model.addRow(new Object[] { id, description, start, end, });
		}

		return tbl_Tasks;
	}

	public int getTaskId() {
		return taskId;
	}

	public int getProjectId() {
		return projectId;
	}

	public Task getTask(){
		return clickedTask;
	}

}

package ca.concordia.comp354.DAO;

/**
 * This class is to manage Users.
 * 
 * @author LouisMaxime
 */

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.concordia.comp354.authenticate.Login;
import ca.concordia.comp354.database.DbClass;
import ca.concordia.comp354.domain.*;

public class UserDAO {
	//create the logger
	private static final Logger logger = Logger.getLogger( Login.class.getName() );
	private static DbClass db = null;
	
	
	/**
	 * Create a new user from a User object.
	 * @param user The user to add into the DB.
	 * @return The ID of the new user.
	 * @throws Exception
	 */
	public static int createNewUser(User user) throws Exception {
		if (user.getName().isEmpty() || user.getLogin().isEmpty() || user.getEmail().isEmpty() || 
			user.getPassword().isEmpty() || user.getRoleId() > 0) {
			
			return createNewUser(user.getName(), user.getLogin(), user.getPassword(), user.getEmail(), user.getRoleId());
		}
		return -1;
	}
	
	/**
	 * This method will insert a new user in the database with the provided data.
	 * @param name The name of the user.
	 * @param login The login (username) of the user.
	 * @param password The raw password of the user.
	 * @return Boolean - True if success; False if Failure.
	 */
	public static int createNewUser(String name, String login, String password, String email, int roleID) throws Exception {
		/* Step for creating a new member:
		 * 1- Validate that the user does not exists already.
		 * 	1a- Verify that the login does not exists already.
		 * 	1b- Verify that the email does not exists already.
		 * 2- Validate that the permission ID is valid.
		 * 3- Encrypt the password.
		 * 4- Insert the new user with the data.
		 * 5- Get the ID of the new user. 
		 * 6- Associate a role to the user. 
		 * 7- Return the ID of the new user.
		 */
		int idNewUser = -1;
		DbClass myDB = null;
		
		try {
			myDB = new DbClass();
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in createNewMember: " + e.toString(), e);
		}
		
		//First Step: Make sure we do not have duplicate
		if (!isUserExists(login, myDB) && !isEmailExists(email, myDB)) {
			//Step 2: Validate the role ID
			if (validateRole(roleID, myDB)) {
				//Step 3: Encrypt the password
				String encPassword = encryptPassword(password);
				//Step 4: Insert the data
				insertNewUser(name, login, encPassword, email, myDB);
				//Step 5: Get the ID
				idNewUser = getUserID(login, myDB);
				if (idNewUser > 0) {
					//Step 6: Associate the role.
					associateRoleToUser(idNewUser, roleID, myDB);
				}
				else {
					logger.log(Level.SEVERE, "Unable to get the new user ID");
					throw new Exception("Unable to get the new user ID");
				}
			}
		}
		else {
			//The ID or Email is already in the DB!
			return idNewUser; //here it will return -1
		}
		
		//closing connection
		myDB.closeConnection();
		//Step 7: Return the ID of the new user.
		return idNewUser;
	}
	
	private static boolean validateRole(int role, DbClass db) {
		String query = "SELECT COUNT(*) FROM ROLES WHERE id = " + role + ";";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		int nbRole = -1;
		try {
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next()){
					nbRole = result.getInt(1);
				}
				
				if (nbRole == 1) {
					return true;
				}
				else{
					logger.log(Level.WARNING, "No role found for roleID: " + role + ".");
					return false; //one role found
				}
				
			}
			else {
				logger.log(Level.WARNING, "Unable to execute query: " + query + " .");
				return false;
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception throw in validateRole.");
			db.closeConnection();
			return false;
		}
	}
	
	/**
	 * Delete a user in the DB by using its login
	 * @param login The login (username) of the user
	 * @param db
	 */
	public static void deleteUserFromLogin(String login) {
		String query = "DELETE FROM USERS WHERE login = '" + login + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			db.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in deleteUserFromLogin: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
	}
	
	/**
	 * Delete a user in the DB by using its id
	 * @param id The id of the user
	 * @param db
	 */
	public static void deleteUserFromID(int id) {
		String query = "DELETE FROM USERS WHERE id = " + id + ";";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			db.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in deleteUserFromID: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
	}
	
	/**
	 * Update a user by using the login. When updating a user, only the name and the email can be modified.
	 * @param login The login of the user to modify
	 * @param name The new name of the user.
	 * @param email The new email of the user.
	 */
	public static void updateUserFromLogin(String login, String name, String email) {
		String query = "UPDATE USERS SET name = '" + name + "', email = '" + email + "' WHERE login = '" + login + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			db.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in updateUserFromLogin: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
	}
	
	/**
	 * Update a user by using the id. When updating a user, only the name and the email can be modified.
	 * @param id The id of the user to modify
	 * @param name The new name of the user.
	 * @param email The new email of the user.
	 */
	public static void updateUserFromID(int id, String name, String email, String password, int newRole) {
		String query = "UPDATE USERS SET name = '" + name + 
				"', email = '"+ email + 
				"', password = '" + encryptPassword(password)
				+ "' WHERE id = " + id + ";";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			updateRoleToUser(id, newRole, db);
			db.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in updateUserFromLogin: " + e.toString(), e);
			db.closeConnection();
		}
	}
	
	/**
	 * This method will verify in the database if the username already exists
	 * @param username
	 * @param db
	 * @return boolean True if username exists, else False
	 */
	//Step 1a
	private static boolean isUserExists(String username, DbClass db) {
		//access the database with something like
		String query = "SELECT COUNT(login) FROM USERS WHERE login = '" + username + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbUsername = -1;
		
		try {
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next())
				{
					nbUsername = result.getInt(1);
				}
				
				if (nbUsername > 1) {
					logger.log(Level.WARNING, "The username " + username + " appears more than once in the database.");
				}
				
			}
			else {
				logger.log(Level.WARNING, "Unable to execute query: " + query + " .");
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in isUserExists: " + e.toString(), e);
		} finally {
			db.closeConnection();
		}
		//if we have one results, return true
		//if we have 0 results, return false
		//if we have more than one results, we should return false and logged that we have some data corrupted in the DB.
		//Normally, it should not happen
		if (nbUsername == 1){
			logger.log(Level.INFO, "Username: " + username + " found.");
			return true;
		}
		else {
			logger.log(Level.INFO, "Username: " + username + " not found.");
			return false;
		}
	}
	
	/**
	 * This method will verify in the database if the email already exists
	 * @param email The email we want to check
	 * @param db
	 * @return Boolean - True if it exists; False if not
	 */
	//Step 1b
	private static boolean isEmailExists(String email, DbClass db) {
		//Searching for the email in the database
		String query = "SELECT COUNT(email) FROM USERS WHERE email = '" + email + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbEmail = -1;
		
		try {
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next()) {
					nbEmail = result.getInt(1);
				}
				
				//If the count is > 1, then we have duplicate already in the DB and we should clean it
				if (nbEmail > 1) {
					logger.log(Level.WARNING, "The email " + email + " appears more than once in the database.");
				}
				
			}
			else {
				logger.log(Level.WARNING, "Unable to execute query: " + query + " .");
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in isEmailExists: " + e.toString(), e);
		} finally {
			db.closeConnection();
		}
		//if we have one results, return true
		//if we have 0 results, return false
		//if we have more than one results, we should return false and logged that we have some data corrupted in the DB.
		//Normally, it should not happen
		if (nbEmail == 1){
			logger.log(Level.INFO, "Email" + email + " found.");
			return true;
		}
		else {
			logger.log(Level.INFO, "Email: " + email + " not found.");
			return false;
		}
	}
	
	/**
	 * This method encrypt the password of the new user
	 * @param password
	 * @return The encrypted password entered by the user.
	 */
	//Step 3:
	private static String encryptPassword(String password) {
		//code from petrnohejl on stackoverflow for this function and byteToHex function
		//http://stackoverflow.com/questions/4895523/java-string-to-sha1
		
		MessageDigest crypt = null;
		String enc_pass = null;
		try {
			//get the crypter
			crypt = MessageDigest.getInstance("SHA-1");
			//clear of junk that could be there
			crypt.reset();
			//get the new value
			crypt.update(password.getBytes("UTF-8"));
			//get the encrypted string
			enc_pass = byteToHex(crypt.digest());
		}
		catch(NoSuchAlgorithmException e){
			logger.log(Level.SEVERE, "Exception in encryptPassword: " + e.toString(), e);
		}
		catch(UnsupportedEncodingException e){
			logger.log(Level.SEVERE, "Exception in encryptPassword: " + e.toString(), e);
		}
		
		return enc_pass;
	}
	
	/**
	 * This method will convert an hash to an encrypted string.
	 * @param hash
	 * @return An encrypted string.
	 */
	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String encrypted = formatter.toString();
		formatter.close();
		return encrypted;
	}
	
	/**
	 * Insert a new user in the database with the data provided
	 * @param name The name of the user
	 * @param login The login (username) of the user
	 * @param encPassword The encypted password of the user
	 * @param email The email of the user
	 * @param db
	 */
	//Step 4:
	private static void insertNewUser(String name, String login, String encPassword, String email, DbClass db) {
		//Query to insert
		String query = "INSERT INTO USERS (name, login, password, email) VALUES ('" + 
				name + "', '" + login + "', '" + encPassword + "', '" + email + "');";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		try {
			db.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in insertNewUser: " + e.toString(), e);
			db.closeConnection();
		}
	}
	
	/**
	 * Associate a role ID to a certain user.
	 * @param userID The ID of the user.
	 * @param roleID The ID of the role.
	 * @param db
	 */
	//Step 6:
	private static void associateRoleToUser(int userID, int roleID, DbClass db) {
		String query = "INSERT INTO USERS_ROLES (id_user, id_roles) VALUES (" +
				userID + ", " + roleID + ");";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		try {
			db.executeStmt(query);
		}
		catch (Exception e) {
			db.closeConnection();
			logger.log(Level.SEVERE, "Exception in associateRoleToUser: " + e.toString(), e);
		}
	}
	
	/**
	 * Associate a role ID to a certain user.
	 * @param userID The ID of the user.
	 * @param roleID The ID of the role.
	 * @param db
	 */
	private static void updateRoleToUser(int userID, int roleID, DbClass db) {
		String query = "UPDATE USERS_ROLES SET id_roles = " + roleID + " WHERE  id_user = "+ userID + ";";
				
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		try {
			db.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in associateRoleToUser: " + e.toString(), e);
			db.closeConnection();
		}
	}
	
	/**
	 * Get the ID of a user.
	 * @param login The login of the user we want the ID.
	 * @param db
	 * @return int - The ID of the user associated with the login.
	 */
	//Step 5: 
	public static int getUserID(String login, DbClass db) {
		//The query
		String query = "SELECT id FROM USERS WHERE login = '" + login + "';";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		int idUser = -1;
		try {
			//If we can't execute the query the problem
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next())
				{
					idUser = result.getInt(1);
				}
			}
			else {
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getNewUserID: " + e.toString(), e);
		}
		return idUser;
	}
	
	/**
	 * Get the user information from its ID
	 * @param id The ID of the user you want info
	 * @return If user exists returns an object user with all the information otherwise it returns null
	 */
	public static User getUserInfoFromID(int id) {
		User u = null;
		String query = "SELECT u.id, u.name, u.login, u.email, ur.id_roles FROM USERS u, USERS_ROLES ur WHERE u.id = " + 
					   id + " AND u.id = ur.id_user;";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				while (result.next())
				{
					if(result.getInt("id") == id){
						int idUser = result.getInt("id");
						String name = result.getString("name");
						String login = result.getString("login");
						String email = result.getString("email");
						int idRole = result.getInt("id_roles");
						u = new User(idUser, name, login, email, "", idRole);
						break;
					}
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getUserInfoFromID: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
		
		return u;
	}
	
	/**
	 * Get all users that exist in user table
	 * @return Array of user objects.
	 */
	public static ArrayList<User> getAllUsers(){
		ArrayList<User> users = new ArrayList<User>(10);
		String query = "SELECT id, name, login, email FROM USERS";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				int idUser = -1;
				String name = "";
				String login = "";
				String email = "";
				while (result.next())
				{
					idUser = result.getInt("id");
					name = result.getString("name");
					login = result.getString("login");
					email = result.getString("email");
					
					User u = new User(idUser, name, login, email, "");
					users.add(u);
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getAllUsers: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
		
		return users;
	}
	
	/**
	 * Get all users that haven't been assigned to the specific task with id.
	 * @return Array of user objects.
	 */
	public static ArrayList<User> getMembersNotAssignedToTaskWithId(int taskId){
		ArrayList<User> users = new ArrayList<User>(10);
		String query = "SELECT users.id, users.name, users.login, users.email "
				+ "FROM users, roles, users_roles "
				+ "WHERE cast(users.id as int) NOT IN (SELECT cast(tasks_users.id_user as int) FROM tasks_users WHERE tasks_users.id_task = '" + taskId + "')"
				+ "AND roles.name = 'member'"
				+ "AND roles.id = users_roles.id_roles "
				+ "AND users_roles.id_user = users.id";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		
		DbClass db = null;
		try {
			db = new DbClass();
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				int idUser = -1;
				String name = "";
				String login = "";
				String email = "";
				while (result.next())
				{
					//if(result.getInt("id") == taskId){
					
					idUser = result.getInt("id");
					name = result.getString("name");
					login = result.getString("login");
					email = result.getString("email");
					
					User u = new User(idUser, name, login, email, "");
					users.add(u);
					
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getAllUsers: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
		
		return users;
	}
	
	private static void connectDB(){
		//creates a DB object
		
		try {
			db = new DbClass();
		} catch (Exception e) {
			logger.log( Level.SEVERE, e.toString(), e);
		} finally {
			db.closeConnection();
		}
	}
	
	/**
	 * Add user to given taskId
	 * @return nada.
	 */
	public static void addUserToTask(int userId, int taskId){
		String query = "INSERT INTO tasks_users (id_user, id_task) VALUES (" + userId + ", " + taskId + ");";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		connectDB();
		int nbRecord = -1; 
		try {
			nbRecord = db.executeStmt(query);
			if (nbRecord ==  1){
				logger.log(Level.INFO, "Added user to task!");
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in addUserToTask: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
	}
	/**
	 * This method will get all members from db
	 * @author Eugeniu Josan 6632882 (Modified 2014/7/15)
	 * @param  
	 * @return ArrayList<User> - a list of users
	 */
	public static ArrayList<User> getAllMembers(){
		ArrayList<User> users = new ArrayList<User>(10);
		String query = "SELECT users.id, users.name, users.login, users.email "
				+ "FROM users, roles, users_roles "
				+ "WHERE roles.name = 'member'"
				+ "AND roles.id = users_roles.id_roles "
				+ "AND users_roles.id_user = users.id";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				int idUser = -1;
				String name = "";
				String login = "";
				String email = "";
				while (result.next())
				{
					idUser = result.getInt("id");
					name = result.getString("name");
					login = result.getString("login");
					email = result.getString("email");
					
					User u = new User(idUser, name, login, email, "");
					users.add(u);
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getAllUsers: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
		
		return users;
	}
	
	/**
	 * This method will get all members assigned to task from db
	 * @author Eugeniu Josan 6632882 (Modified 2014/7/15)
	 * @param  
	 * @return ArrayList<User> - a list of users
	 */
	public static ArrayList<User> getMembersAssignedToTask(int idTask){
		ArrayList<User> users = new ArrayList<User>(10);
		String query =  "SELECT users.id, users.name, users.login, users.email "
				+ "FROM users, roles, users_roles, tasks, tasks_users "
				+ "WHERE roles.name = 'member' "
				+ "AND roles.id = users_roles.id_roles "
				+ "AND users_roles.id_user = users.id "
				+ "AND tasks_users.id_task = tasks.id "
				+ "AND tasks_users.id_user = users.id "
				+ "AND tasks.id ="+ idTask+";";
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		DbClass db = null;
		try {
			db = new DbClass();
			if (db.execute(query)) {
				ResultSet result = db.executeQry(query);
				int idUser = -1;
				String name = "";
				String login = "";
				String email = "";
				while (result.next())
				{
					idUser = result.getInt("id");
					name = result.getString("name");
					login = result.getString("login");
					email = result.getString("email");
					
					User u = new User(idUser, name, login, email, "");
					users.add(u);
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getAllMembersAssignedToTask: " + e.toString(), e);
		}
		finally {
			db.closeConnection();
		}
		
		return users;
	}
}

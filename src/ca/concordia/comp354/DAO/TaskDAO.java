package ca.concordia.comp354.DAO;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.concordia.comp354.authenticate.Login;
import ca.concordia.comp354.database.DbClass;
import ca.concordia.comp354.domain.Task;


/**
 * TaskDAO is used to query the database for basic commands, and to extract tuples/sets of data that are used to populate
 * the Task objects.
 * @author Eugeniu Josan
 * 
 */
public class TaskDAO {
	
	private static final Logger logger = Logger.getLogger( Login.class.getName() );
	private static DbClass myDB = null;

	
	private static void connectDB(){
		//creates a DB object
		try {
			myDB = new DbClass();
		} catch (Exception e) {
			logger.log( Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
	}
	
	
	/**
	 * Create a new task from a Task object.
	 * @param user The user to add into the DB.
	 * @return The ID of the new task.
	 * @throws Exception
	 */
	public static int createTask(Task task, int projectId) throws Exception {
		if (!task.getDescription().isEmpty() && task.getStart_Date() != null && task.getTime_Required() > 0  && 
			task.getStatus_Id() > 0) {
			
			int id = createNewTask(task.getDescription(), task.getStart_Date(), task.getEnd_Date(), task.getTime_Required(), 
					task.getTime_Spent(), task.getStatus_Id(), task.getOptimistic_duration(), task.getPessimistic_duration(), 
					task.getMostLikely_duration(), task.getPV(), task.getAC());
			assignTaskToProject(projectId, id);
			return id;
		}
		return -1;
	}

	private static int createNewTask(String description, Date start_Date, 
			Date end_Date, float time_Required, float time_Spent, int status_Id, 
			int optimistic_duration, int pessimistic_duration, int mostLikely_duration, 
			float planned_value, float actual_cost) {
		
		connectDB();
		
		//quick note:  We must format the Date object as a string to insert in database
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		String start_date = dateFormat.format(start_Date);
		String end_date = dateFormat.format(end_Date);
		
		int id = -1;
		String query = "INSERT INTO tasks (description, start_date, end_date, time_required, time_spent, "
				+ "status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, planned_value, actual_cost) "
						+ "VALUES ('" + description + "', '" + start_date + "', '" 
						+ end_date + "', '" + time_Required +  "', '" + time_Spent +  "', '"
						+ status_Id +  "', '" + optimistic_duration 
						+  "', '" + pessimistic_duration + "', '" + mostLikely_duration
						 + "', '" + planned_value + "', '" +actual_cost+"');" ;
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 
		
		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Task inserted into database");
				ResultSet rs = myDB.getStatement().getGeneratedKeys();
				while (rs.next()){
					id = rs.getInt(1);
				}
			} else {
				logger.warning("There was an error inserting new task into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
		
		return id;
	}
	
	/**
	 * Get all tasks 
	 * @return Array of user objects.
	 */
	
	public static ArrayList<Task> getAllTasks(){
		ArrayList<Task> tasks = new ArrayList<Task>();
		String query = "SELECT tasks.id, description, start_date, end_date, time_required, time_spent,"
				+ " status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, id_project, planned_value, actual_cost "
				+ "FROM tasks, tasks_projects WHERE tasks.id = tasks_projects.id_task;";
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		
		try {
			myDB  = new DbClass();
			if (myDB.execute(query)) {
				ResultSet result = myDB.executeQry(query);
				int idTask = -1;
				String description = "";
				String start_date = "";
				String end_date = "";
				float time_required = 0;
				float time_spent = 0;
				int status_id = 0;
				int optimistic_duration = 0;
				int pessimistic_duration = 0;
				int mostLikely_duration = 0;
				int projectId = 0;
				ArrayList<Integer> parentTask;

				float planned_value = 0;
				float actual_cost = 0;
				
				while (result.next()){
					idTask = result.getInt("id");
					description = result.getString("description");
					start_date = result.getString("start_date");
					end_date = result.getString("end_date");
					time_required = result.getInt("time_required");
					time_spent = result.getInt("time_spent");
					status_id = result.getInt("status_id");
					optimistic_duration = result.getInt("optimistic_duration");
					pessimistic_duration = result.getInt("pessimistic_duration");
					mostLikely_duration = result.getInt("mostLikely_duration");
					planned_value = result.getInt("planned_value");
					actual_cost = result.getInt("actual_cost");					
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					Date start_Date = dateFormat.parse(start_date);
					Date end_Date = dateFormat.parse(end_date);
					
					projectId = result.getInt("id_project");
					
					parentTask = TaskDAO.getParentTasks(idTask);
		
					Task task = new Task(idTask, description, start_Date, end_Date, time_required, time_spent, 
									status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, projectId, parentTask, planned_value, actual_cost);
					tasks.add(task);
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getAllTasks: " + e.toString(), e);
		}
		finally {
			myDB.closeConnection();
		}
		
		return tasks;
	}
	
	
	/**
	 * This method will return a list of Task objects, and depending on key it can return the following list of tasks:
	 * key = 1 - returns a list of all tasks of a specific project
	 * key = 2 - returns a list of all tasks assigned to the specific member
	 * @param value - can be either project id or user id
	 * @param key - selects the type of list to be returned
	 * @return the list of task objects
	 */
	
	public static ArrayList<Task> getListOfTasks(int value, int key){
		ArrayList<Task> tasks = new ArrayList<Task>(10);
		String query = "";
		
		if (key == 1){
			query = "SELECT tasks.id, description, start_date, end_date, time_required, time_spent,"
					+ " status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, id_project, planned_value, actual_cost "
					+ "FROM tasks, tasks_projects "
					+ "WHERE tasks_projects.id_task = tasks.id "
					+ "AND tasks_projects.id_project =" + value +";";
		}
		if (key == 2){
			query = "SELECT tasks.id, description, start_date, end_date, time_required, "
					+ "time_spent, status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, id_project, planned_value, actual_cost "
					+ "FROM tasks, tasks_users, users, tasks_projects "
					+ "WHERE users.id = tasks_users.id_user "
					+ "AND tasks_users.id_task = tasks.id "
					+ " AND tasks_projects.id_task = tasks.id"
					+ " AND users.id = " + value + " ;";
		
		}
		
		//connectDB();
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		try {
			myDB  = new DbClass();
			if (myDB.execute(query)) {
				ResultSet result = myDB.executeQry(query);
				int idTask = -1;
				String description = "";
				String start_date = "";
				String end_date = "";
				float time_required = 0;
				float time_spent = 0;
				int status_id = 0;
				int optimistic_duration = 0;
				int pessimistic_duration = 0;
				int mostLikely_duration = 0;
				int projectId = 0;
				ArrayList<Integer> parentTask;

				float planned_value = 0;
				float actual_cost = 0;

				while (result.next())
				{
					idTask = result.getInt("id");
					description = result.getString("description");
					start_date = result.getString("start_date");
					end_date = result.getString("end_date");
					time_required = result.getInt("time_required");
					time_spent = result.getInt("time_spent");
					status_id = result.getInt("status_id");
					optimistic_duration = result.getInt("optimistic_duration");
					pessimistic_duration = result.getInt("pessimistic_duration");
					mostLikely_duration = result.getInt("mostLikely_duration");
					planned_value = result.getInt("planned_value");
					actual_cost = result.getInt("actual_cost");		
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					Date start_Date = dateFormat.parse(start_date);
					Date end_Date = dateFormat.parse(end_date);
					
					projectId = result.getInt("id_project");
					
					parentTask = TaskDAO.getParentTasks(idTask);
					
		
					Task task = new Task(idTask, description, start_Date, end_Date, time_required, time_spent, 
									status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, projectId, parentTask, planned_value, actual_cost);
					tasks.add(task);
				}
			}
			else {
				logger.log(Level.SEVERE, "Unable to execute query " + query + ".");
				throw new Exception("Unable to execute query: " + query);
			}
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in getListOfTasks: " + e.toString(), e);
			logger.log(Level.SEVERE,"Accessing DB with query: " + query + ".");
			
		}
		finally {
			myDB.closeConnection();
		}
	
		return tasks;
	}
	
	/**
	 * This method will grab a task from the database for the given id and
	 * populate the object parameters with the extracted values
	 * 
	 * @param id
	 */
	public static Task getTaskFromDB(int id){
		String query = "SELECT tasks.id, description, start_date, end_date,time_required, time_spent,status_id,"
				+ "optimistic_duration, pessimistic_duration, mostlikely_duration, planned_value, actual_cost, "
				+ "id_project FROM tasks, tasks_projects WHERE tasks.id = id_task AND tasks.id = " + id + ";";
		ResultSet result = null;
		Task task = null;
		
		connectDB();
		
		try {
			if (myDB.execute(query)){
				result = myDB.executeQry(query);
				int idTask = -1;
				String description = "";
				String start_date = "";
				String end_date = "";
				float time_required = 0;
				float time_spent = 0;
				int status_id = 0;
				int optimistic_duration = 0;
				int pessimistic_duration = 0;
				int mostLikely_duration = 0;
				int projectId = 0;
				ArrayList<Integer> parentTask;

				float planned_value = 0;
				float actual_cost = 0;

				while (result.next())
				{
					idTask = result.getInt("id");
					description = result.getString("description");
					start_date = result.getString("start_date");
					end_date = result.getString("end_date");
					time_required = result.getInt("time_required");
					time_spent = result.getInt("time_spent");
					status_id = result.getInt("status_id");
					optimistic_duration = result.getInt("optimistic_duration");
					pessimistic_duration = result.getInt("pessimistic_duration");
					mostLikely_duration = result.getInt("mostLikely_duration");
					planned_value = result.getInt("planned_value");
					actual_cost = result.getInt("actual_cost");		
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					Date start_Date = dateFormat.parse(start_date);
					Date end_Date = dateFormat.parse(end_date);
					
					projectId = result.getInt("id_project");

					parentTask = TaskDAO.getParentTasks(idTask);					
		
					task = new Task(idTask, description, start_Date, end_Date, time_required, time_spent, 
									status_id, optimistic_duration, pessimistic_duration, mostLikely_duration, projectId, parentTask, planned_value, actual_cost);
				}
				
			} else {
				logger.warning("NO such task exists in the db with id " + id);
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
		return task;	
	}
	
	/**
	 * Delete a task in the DB by using its id
	 * @param id The id of the task
	 * @param db
	 */
	public static void deleteTaskFromID(int id) {
		String query = "DELETE FROM tasks WHERE id = " + id + ";";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		
		connectDB();
		
		try {
			myDB.executeStmt(query);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Exception in deleteUserFromID: " + e.toString(), e);
		}
		finally {
			myDB.closeConnection();
		}
	}
	
	/**
	 * Update a task by using the id. 
	 * @param id The id of the task to modify
	 * @param name The new description of the task.
	 * @param start_date The new start date of the task.
	 * @param end_date. The new end date of the task.
	 * @param time_required. The new time_required of the task.
	 * @param time_spent. The new time_spent of the task.
	 * @param status_id. The new status id of the task.
	 * @param optimistic_duration. The new optimistic duration of the task.
	 * @param pessimistic_duration. The new pessimistic duration of the task.
	 */
	public static void updateTaskFromID(int id, String description, Date start_date, Date end_date, float time_required,
			float time_spent, int status_id, int optimistic_duration, int pessimistic_duration, int mostLikely_duration,
			float planned_value, float actual_cost) {
		
		String tempQuery = "UPDATE tasks SET ";
		if (description != null){
			tempQuery += "description = '"+ description +"'";
		}if (start_date != null){
			tempQuery += " start_date = '"+ start_date +"'";
		}if (end_date != null){
			tempQuery += " end_date = '"+ end_date +"'";
		}if (time_required != 0){
			tempQuery += " time_required = '"+ time_required +"'";
		}if (time_spent != 0){
			tempQuery += " time_spent = '"+ time_spent +"'";
		}if (status_id != 0){
			tempQuery += " status_id = '"+ status_id +"'";
		}if (optimistic_duration != 0){
			tempQuery += " optimistic_duration = '"+ optimistic_duration +"'";
		}if (pessimistic_duration != 0){
			tempQuery += " pessimistic_duration = '"+ pessimistic_duration +"'";
		}if (mostLikely_duration != 0){
			tempQuery += " mostLikely_duration = '"+ mostLikely_duration +"'";
		}if (planned_value != 0){
			tempQuery += " planned_value = '"+ planned_value +"'";
		}if (actual_cost != 0){
			tempQuery += " actual_cost = '"+ actual_cost +"'";
		}
		tempQuery += " WHERE id = " + id + ";";
	
		
		logger.log(Level.INFO, "Accessing DB with query: " + tempQuery + ".");
		
		connectDB();
		try {
			myDB.executeStmt(tempQuery);
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, tempQuery + "\nException in updateTaskFromID: " + e.toString(), e);
			myDB.closeConnection();
		}
	}
	
	private static void assignTaskToProject(int projectID, int taskID){
		String query = "INSERT INTO tasks_projects (id_project, id_task) "
				+ " VALUES( " + projectID + ", " + taskID + ");" ;
		
		logger.fine("Creating task/Project record for " + taskID + " with " + projectID);
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 
		connectDB();
		
		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Task assigned to Project in DB");
				
			} else {
				logger.warning("There was an error updating the  project into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
	}
	
	public void assignParentTask(int parentTaskID, int childTaskID){
		String query = "INSERT INTO related_tasks (parent_task_id, child_task_id) "
				+ " VALUES( " + parentTaskID + ", " + childTaskID + ");" ;
		
		logger.fine("Creating parent/child task record for " + childTaskID + " with " + parentTaskID);
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 
		connectDB();

		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Task relationship created in DB");
			} else {
				logger.warning("There was an error updating the tasks into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
	}
	
	/**
	 * Queries the database to find a Task's parent task IDs and returns them in a ArrayList if any are found
	 * @param taskID The ID number of the Task for which the method will query the database for parent Tasks
	 * @return an ArrayList of the parent Tasks' ID numbers (integers)
	 */
	public static ArrayList<Integer> getParentTasks(int taskID){
		String query = "SELECT parent_task_id from related_tasks WHERE child_task_id = " + taskID;
		ArrayList<Integer> list = new ArrayList<Integer>();
		connectDB();
		
		try {
			if (myDB.execute(query)){
				ResultSet result = myDB.executeQry(query);
				while (result.next())
				{
					list.add(result.getInt(1));					
				}
			}
		} catch (Exception e) {
			logger.severe(e.toString());
		} finally {
			myDB.closeConnection();
		}
		return list;	
	}
	
	public ArrayList<Task> getChildTasks(int taskID){
		String query = "SELECT child_task_id from related_tasks WHERE parent_task_id = " + taskID;
		ResultSet result;
		ArrayList<Task> list = new ArrayList<Task>(50);
		try {
			if (myDB.execute(query)){
				result = myDB.executeQry(query);
				while (result.next())
				{
					list.add(TaskDAO.getTaskFromDB(result.getInt(1)));					
				}
			}
		} catch (Exception e) {
			logger.severe(e.toString());
		} finally {
			myDB.closeConnection();
		}
	
		return list;
	}
	
	
	
	
	public static void removeParentTask(int childId, int parentId){
		String query = "DELETE FROM related_tasks WHERE child_task_id = "+ childId+
				"AND parent_task_id = "+ parentId +";";
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 
		connectDB();

		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Task relationship deleted in DB");
			} else {
				logger.warning("There was an error deleting the relation tasks from DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
	}	
}

package ca.concordia.comp354.domain;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.concordia.comp354.authenticate.Login;
import ca.concordia.comp354.database.DbClass;

/**
 * Current Status values:
 * started(1); in-progress(2); Completed(3); duplicate(4); abandoned(5); assigned(6);
 * won't do(7); resolved(8); 
 * @author Amit
 *
 */
public class Status {

	private static final Logger logger = Logger.getLogger( Login.class.getName() );

	private int id;
	private String name;
	private String description;
	private DbClass myDB = null;

	/**
	 * Constructor, takes a description only 
	 * @param id
	 * @param description
	 */
	public Status(String name, String description) {
		this.name = name;
		this.description = description;
		this.connectDB();
		this.createStatus(this.name, this.description);
		setLoggerLevel("");
	}
	
	/**
	 * Will create a status object from the database using the given ID;
	 * @param id
	 */
	public Status(int id){
		this.connectDB();
		this.getStatusFromDB(id);
		setLoggerLevel("");
	}
	
	/**
	 * Empty construtor to quickly create an empty status object
	 */
	public Status(){
		this.connectDB();
		setLoggerLevel("");
	}
	
	private static void setLoggerLevel(String s){
		if (s.equals("fine")){
			logger.setLevel(Level.FINE);
		} else if (s.equals("info")){
			logger.setLevel(Level.INFO);
		} else {
			logger.setLevel(Level.SEVERE);
		}
	}

	private void connectDB(){
		try {
			this.myDB = new DbClass();
		} catch (Exception e) {
			logger.log( Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}

	}
	
	public void deleteStatus(int statusId){
		
		String query = "DELETE FROM status WHERE id = " + statusId + ";";

		try {
			if (this.myDB.execute(query))
			{
				this.myDB.executeQry(query);				
			} 
			else
			{
				logger.warning("NO such status exists in the db with id " + id);
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			this.myDB.closeConnection();
		}
	}
	
	/**
	 * This method creates the status in the database
	 * @param name
	 * @param desc
	 */
	public int createStatus(String name, String desc){
		
		String query = "INSERT INTO status (status_name, status_description) "
						+ "VALUES ('" + name + "', '" + desc + "');" ;
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1;
		
		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Status inserted into database");
				//return the key of the latest generated 
				ResultSet rs = myDB.getStatement().getGeneratedKeys();
				while (rs.next()){
					this.id = rs.getInt(1);
				}
			} else {
				logger.warning("There was an error inserting new status into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
		
		return this.id;
		
	}
	
	/**
	 * This method will grab a status from the database for the given id 
	 * 
	 * @param id
	 */
	public void getStatusFromDB(int id){
		String query = "SELECT * FROM status where id = " + id + ";";
		logger.info("Sending query " + query);
		ResultSet result = null;
		try {
			if (this.myDB.execute(query)){
				logger.info("Query will be now executed");
				result = this.myDB.executeQry(query);
				while (result.next())
				{
					logger.info("Populating object");
					this.id = result.getInt("id");
					this.name = result.getString("status_name");
					this.description = result.getString("status_description");
				}
				
			} else {
				logger.warning("NO such status exists in the db with id " + id);
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			this.myDB.closeConnection();
		}	
	}

	
	/**
	 * This method returns a ResultSet object that can be used to read rows of data from the database
	 *
	 * A resultset can be read easily using a while loop.  Here's an example:
     * 
     *  ResultSet rs = statement.executeQuery("select * from person");
	 * 	while(rs.next()) {
	 *       // read the result set
	 *       System.out.println("name = " + rs.getString("name"));
	 *       System.out.println("id = " + rs.getInt("id"));
	 *  }

	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getStatusList(){
		logger.fine("Getting status list");
		String query = "SELECT * FROM status;";
		
		ArrayList list = null;
		ResultSet result = null;
		
		try {
			if (this.myDB.execute(query)){
				result = this.myDB.executeQry(query);
				list = (ArrayList) this.resultSetToArrayList(result);
			}
		} catch (Exception e) {
			logger.severe(e.toString());
		} finally {
			this.myDB.closeConnection();
		}
		
		return list;
	}
	
	
	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("rawtypes")
	public List resultSetToArrayList(ResultSet rs) throws SQLException{
		  ResultSetMetaData md = rs.getMetaData();
		  int columns = md.getColumnCount();
		ArrayList list = new ArrayList(50);
		  while (rs.next()){
		     HashMap row = new HashMap(columns);
		     for(int i=1; i<=columns; ++i){           
		      row.put(md.getColumnName(i),rs.getObject(i));
		     }
		      list.add(row);
		  }

		 return list;
		}
	
	/**
	 * This method will update the Status object in the database with the given column/value pairs
	 * in the provided HashMap of String (key), String (value).  It formulates a query in the format
	 * UPDATE table_name SET column1 = value1,..,columnN = valueN WHERE id = this.id and updates
	 * the status in the database with the provided update values. 
	 * 
	 * @param column_values HashMap<String, String>
	 * @return  true if successful 
	 */
	public boolean updateStatus(HashMap<String, String> column_values){
		String table_name = "status";
		String set_query = "SET" ;
		int size = column_values.size();
		int i = size;
		
		//iterate over the HashMap and pull the values out in the SET part of the query
		for(Map.Entry<String, String> entry : column_values.entrySet()){
			if (i == 1){
				set_query = set_query + " " + entry.getKey() + " = '" + entry.getValue() + "'";
			} else {
				set_query = set_query + " " + entry.getKey() + " = '" + entry.getValue() + "',";
				i--;
			}
		}
		
		String where_query = "WHERE id = " + this.id ;
		String query = "UPDATE " + table_name + " " + set_query + " " + where_query + ";" ;
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 

		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Status updated into database");
				return true;
			} else {
				logger.warning("There was an error updating the  status into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
				return false;
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
			return false;
		} finally {
			myDB.closeConnection();
		}
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}
	

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
		HashMap<String, String> column_values = new HashMap<String, String>();
		logger.fine("Changing description of the status " + this.id + " to " + this.description );
		column_values.put("status_description", this.description);
		if (this.updateStatus(column_values)){
			logger.info("Status Description updated with " + description);
		} else {
			logger.warning ("There was a problem updating the Status description, please see logs");
		}
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
		HashMap<String, String> column_values = new HashMap<String, String>();
		logger.fine("Changing name of the status " + this.id + " to " + this.name );
		column_values.put("status_name", this.name);
		if (this.updateStatus(column_values)){
			logger.info("Status Name updated with " + description);
		} else {
			logger.warning ("There was a problem updating the Status name, please see logs");
		}

	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Status [id=" + this.id + ", name=" + this.name + ", description=" + this.description + "]";
	}
	
	
}

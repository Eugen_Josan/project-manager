package ca.concordia.comp354.domain;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.concordia.comp354.authenticate.Login;
import ca.concordia.comp354.database.DbClass;
import ca.concordia.comp354.domain.Task;

public class Project
{
	private static final Logger logger = Logger.getLogger( Login.class.getName() );
	private int	id = 0;
	private String title;
	private Date start_Date = new Date();
	private Date end_Date = new Date();
	private int	status_Id;
	private DbClass myDB = null;
	private Status status = null;
	
	
	/**
	 *  Quick constructor to start an empty project object to be populated as required. 
	 */
	public Project(){
		this.connectDB();
		setLoggerLevel("");
	}
	
	/**
	 * Quick constructor that will take an ID from the database and find it in the database
	 * and populate the object with the info from the database
	 * @param id
	 */
	public Project(int id){
		this.connectDB();
		this.getProjectFromDB(id);
		setLoggerLevel("");
	}
	
	/**
	 * 
	 * @param title
	 * @param start_Date
	 * @param end_Date
	 */
	public Project(String title, Date start_Date, Date end_Date)
	{
		logger.fine("Creating a new project");
		this.title = title;
		this.start_Date = start_Date;
		this.end_Date = end_Date;
		this.status_Id = 1; //set initial status_id as 0 (started)
		this.connectDB();
		this.createProject(this.title, this.start_Date, this.end_Date);
		setLoggerLevel("");
	}
	
	private static void setLoggerLevel(String s){
		if (s.equals("fine")){
			logger.setLevel(Level.FINE);
		} else if (s.equals("info")){
			logger.setLevel(Level.INFO);
		} else {
			logger.setLevel(Level.SEVERE);
		}
	}
	
	private void connectDB(){
		//creates a DB object
		try {
			this.myDB = new DbClass();
		} catch (Exception e) {
			logger.log( Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}

	}

	
	/**
	 * Will create a project using the supplied "title", start_Date, end_Date
	 * INSERT INTO projects (title, start_date, end_date) 
	 * VALUES('my crappy project', '2014-05-30 01:05:910.910', '2014-05-31 01:05:910.911');
	 * @param title
	 * @param start_Date
	 * @param end_Date
	 */
	public void createProject(String title, Date start_Date, Date end_Date){
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDateStr = dateFormat.format(start_Date);
		String endDateStr = dateFormat.format(end_Date);
		
		
		String query = "INSERT INTO projects (title, start_date, end_date, status_id) "
						+ "VALUES ('" + title + "', '" + startDateStr + "', '" 
						+ endDateStr + "', '" + this.status_Id +  "');" ;
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 
		
		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Project inserted into database");
				//return the key of the latest generated 
				ResultSet rs = myDB.getStatement().getGeneratedKeys();
				while (rs.next()){
					this.id = rs.getInt(1);
				}
			} else {
				logger.warning("There was an error inserting new project into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
		
	}
	
	/**
	 * This method returns a ResultSet object that can be used to read rows of data from the database
	 *
	 * A resultset can be read easily using a while loop.  Here's an example:
     * 
     *  ResultSet rs = statement.executeQuery("select * from person");
	 * 	while(rs.next()) {
	 *       // read the result set
	 *       System.out.println("name = " + rs.getString("name"));
	 *       System.out.println("id = " + rs.getInt("id"));
	 *  }
	 *  This resultset contains the database row with:
	 *  id

	 * @return
	 */
	public List getProjectList(){
		String query = "SELECT * FROM projects;";
		ResultSet result = null;
		ArrayList list = null;
		try {
			if (this.myDB.execute(query)){
				result = this.myDB.executeQry(query);
				list = (ArrayList) this.resultSetToArrayList(result);
			}
		} catch (Exception e) {
			logger.severe(e.toString());
		} finally {
			this.myDB.closeConnection();
		}
		
		return list;
	}
	
	/**
	 * This takes an SQL resultset and creates an array of Hashmaps, where each
	 * record of the array is a row of the database represented as a hashmap, with 
	 * column names as keys and the values in the database as hashmap values. 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	public List resultSetToArrayList(ResultSet rs) throws SQLException{
		  ResultSetMetaData md = rs.getMetaData();
		  int columns = md.getColumnCount();
		  ArrayList list = new ArrayList(50);
		  while (rs.next()){
		     HashMap row = new HashMap(columns);
		     for(int i=1; i<=columns; ++i){           
		      row.put(md.getColumnName(i),rs.getObject(i));
		     }
		      list.add(row);
		  }

		 return list;
		}

	
	/**
	 * This method will grab a project from the database for the given id 
	 * 
	 * @param id
	 */
	public void getProjectFromDB(int id){
		String query = "SELECT * FROM projects where id = " + id + ";";
		ResultSet result = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDate, endDate;
		try {
			if (this.myDB.execute(query)){
				result = this.myDB.executeQry(query);
				while (result.next())
				{
					this.id = result.getInt("id");
					this.title = result.getString("title");
					startDate = result.getString("start_date");
					this.start_Date = dateFormat.parse(startDate);
					endDate = result.getString("end_date");
					this.end_Date = dateFormat.parse(endDate);
					this.status_Id = result.getInt("status_id");
				}
				
			} else {
				logger.warning("NO such project exists in the db with id " + id);
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			this.myDB.closeConnection();
		}	
	}
	
	/*Delete Method to allow removal of Projects
	 * 
	 */
	public void deleteProject(int projectId){
		String query = "DELETE FROM projects WHERE id = " + projectId + ";";

		try {
			if (this.myDB.execute(query))
			{
				this.myDB.executeQry(query);				
			} 
			else
			{
				logger.warning("NO such project exists in the db with id " + id);
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			this.myDB.closeConnection();
		}	
	}
	
	/**
	 * This method will update the project object in the database with the given column/value pairs
	 * in the provided HashMap of String (key), String (value).  It formulates a query in the format
	 * UPDATE table_name SET column1 = value1,..,columnN = valueN WHERE id = this.id and updates
	 * the project in the database with the provided update values. 
	 * 
	 * @param column_values HashMap<String, String>
	 * @return  true if successful 
	 */
	public boolean updateProject(HashMap<String, String> column_values){
		String table_name = "projects";
		String set_query = "SET" ;
		int size = column_values.size();
		int i = size;
		
		//iterate over the HashMap and pull the values out in the SET part of the query
		for(Map.Entry<String, String> entry : column_values.entrySet()){
			if (i == 1){
				set_query = set_query + " " + entry.getKey() + " = '" + entry.getValue() + "'";
			} else {
				set_query = set_query + " " + entry.getKey() + " = '" + entry.getValue() + "',";
				i--;
			}
		}
		
		String where_query = "WHERE id = " + this.id ;
		String query = "UPDATE " + table_name + " " + set_query + " " + where_query + ";" ;
		
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 

		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Project updated into database");
				
				this.title = column_values.get("title");
				Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS.SSS").parse(column_values.get("start_date"));
				Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS.SSS").parse(column_values.get("end_date"));
				this.start_Date = startDate;
				this.end_Date = endDate;
				this.status_Id = Integer.parseInt(column_values.get("status_id"));
				
				return true;
			} else {
				logger.warning("There was an error updating the  project into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
				return false;
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
			return false;
		} finally {
			myDB.closeConnection();
		}
	}
	
	/** 
	 * This will return the ID of the project object
	 * Note, there is no "setID" method as ID is auto incremented in the database
	 * @return
	 */
	public int getId()
	{
		return this.id;
	}
	
	public int getIdFromDB(){
		
		return this.id;
	}
	
	/**
	 * This will return the Title of the project object
	 * @return String
	 */
	public String getTitle()
	{
		return this.title;
	}

	/**
	 * This method will modify the title of the Project Object and the project entry in the database
	 * with the given title.
	 * @param title (string)
	 */
	public void setTitle(String title)
	{
		this.title = title;
		HashMap<String, String> column_values = new HashMap<String, String>();
		logger.fine("Changing title of the project " + this.id + " to " + this.title );
		column_values.put("title", this.title);
		if (this.updateProject(column_values)){
			logger.info("Project Title updated with " + title);
		} else {
			logger.warning ("There was a problem updating the project title, please see logs");
		}
	}
	
	/**
	 * This method will return the start date of the project object
	 * @return Date object
	 */
	public Date getstart_Date()
	{
		return start_Date;
	}
	
	/**
	 * This method will update the start Date of the project object with the given Date
	 * @param start_Date Date object
	 */
	public void setstart_Date(Date start_Date)
	{
		this.start_Date = start_Date;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS.SSS");
		String dateStr = dateFormat.format(start_Date);
		
		HashMap<String, String> column_values = new HashMap<String, String>();
		
		logger.fine("Changing Start Date of the project " + this.id + " to " + dateStr );
		column_values.put("start_date", dateStr);
		if (this.updateProject(column_values)){
			logger.info("Project Start Date updated with " + dateStr);
		} else {
			logger.warning ("There was a problem updating the project Start Date, please see logs");
		}

	}

	/**
	 * This method returns the end date of the project
	 * @return
	 */
	public Date getend_Date()
	{
		return end_Date;
	}

	/**
	 * This method will update the end Date of the project object with the given Date
	 * @param end_Date Date object
	 */
	public void setend_Date(Date end_Date)
	{
		this.end_Date = end_Date;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dateFormat.format(end_Date);
		
		HashMap<String, String> column_values = new HashMap<String, String>();
		
		logger.fine("Changing End Date of the project " + this.id + " to " + dateStr );
		column_values.put("end_date", dateStr);
		if (this.updateProject(column_values)){
			logger.info("Project End Date updated with " + dateStr);
		} else {
			logger.warning ("There was a problem updating the project End Date, please see logs");
		}
	}

	/**
	 * This method returns the status ID of the project
	 * @return
	 */
	public int getstatus_Id()
	{
		return status_Id;
	}

	/**
	 * This method sets the status_Id of the project object in the database to the given id
	 * @param status_Id
	 */
	public void setstatus_Id(int status_Id)
	{
		this.status_Id = status_Id;
		
		//UPDATE table_name SET column1 = value1,..,columnN = valueN WHERE id = this.id and updates
		String query = "UPDATE projects SET status_id = " + this.status_Id + " where id = " + this.id;
		logger.fine("Changing Status of the project " + this.id + " to " + this.status_Id );
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 

		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("Project updated into database");
			} else {
				logger.warning("There was an error updating the  project into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
		
	}
	
	/**
	 * This method assigns the given userID to the current project Object
	 * @param userID
	 */
	public void assignUserToProject(int userID){
		String query = "INSERT INTO users_projects (id_user, id_project) "
				+ " VALUES( " + userID + ", " + this.id + ");" ;
		
		logger.fine("Creating User/Project record for " + this.id + " with " + userID);
		logger.log(Level.INFO, "Accessing DB with query: " + query + ".");
		int nbRecord = -1; 

		try {
			nbRecord = myDB.executeStmt(query);
			if (nbRecord ==  1){
				logger.info("User Assgned to Project in DB");
			} else {
				logger.warning("There was an error updating the  project into DB");
				logger.warning("Query execution returned " + nbRecord + " modified");
			}
		} catch (Exception e){
			logger.log(Level.SEVERE, e.toString(), e);
		} finally {
			myDB.closeConnection();
		}
		
	}
	
	/**
	 * This method returns an ArrayList of user IDs for users who are part of the current project team
	 * @return ArrayList<Integer>
	 */
	public ArrayList<Integer> getAssignedUsers(){
		String query = "SELECT id_user from users_projects WHERE id_project = " + this.id;
		ResultSet result;
		ArrayList<Integer> list = new ArrayList<Integer>(50);
		try {
			if (this.myDB.execute(query)){
				result = this.myDB.executeQry(query);
				while (result.next())
				{
					list.add(result.getInt(1));					
				}
			}
		} catch (Exception e) {
			logger.severe(e.toString());
		} finally {
			this.myDB.closeConnection();
		}
		
		return list;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Project [id=" + id + ", title=" + title + ", start_Date="
				+ start_Date + ", end_Date=" + end_Date + ", status_Id="
				+ status_Id + ", myDB=" + myDB + ", status=" + status + "]";
	}	
}

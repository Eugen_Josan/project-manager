package ca.concordia.comp354.domain;
import ca.concordia.comp354.domain.User;

/**
 * Singleton class used to log the user it keeps the user information
 * and changes the menu deppendin on which type of user has logged in.
 * It uses Lazy implementation method, but it is not thread safe.
**/
public class LoginUserSingleton {

	private User loggedUser;
	private static LoginUserSingleton instance;
	
	private LoginUserSingleton() {}
	
	/**
	 * 
	 * @return the instance of the object
	 */
	public static LoginUserSingleton getInstance(){
		
		if(instance == null){
			
			instance =  new LoginUserSingleton();
		}
		return instance;
	}
	/**
	 * Sets the attribute User of the singleton class
	 * @param oUser the user that has successfully been logged in
	 */
	public void setUser(User oUser){
		loggedUser = oUser;
	}
	
	/**
	 * 
	 * @return the logged in user id
	 */
	public int getUserId(){
		return loggedUser.getId();
	}
	
	/**
	 * 
	 * @return the logged in user login name
	 */
	public String getUserLogin(){
		return loggedUser.getLogin();
	}
	
	/**
	 * 
	 * @return the logged in user role id
	 */
	public int getUserRole(){
		return loggedUser.getRoleId();
	}
	
	/**
	 * 
	 * @return true if user is admin, otherwise false.
	 */
	public boolean isAdmin(){
		
		if(this.getUserRole() == 1){
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return true if user is project manager, otherwise false.
	 */
	public boolean isProjectManager(){
	
		if(this.getUserRole() == 2){
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return true if user is project member, otherwise false.
	 */
	public boolean isProjectMember(){
		
		if(this.getUserRole() == 3){
			return true;
		}
		return false;
	}
}

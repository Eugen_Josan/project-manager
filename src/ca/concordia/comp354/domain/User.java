package ca.concordia.comp354.domain;

import java.util.List;

import ca.concordia.comp354.DAO.UserDAO;
import ca.concordia.comp354.domain.Task;

public class User {

	private int id;
	private String name;
	private List<Task> tasks;
	private String login;
	private String email;
	private int roleId;
	private String password;
	
	public User(){
		
	}
	
	/**
	 * 
	 * @param name
	 * @param login
	 * @param email
	 * @param password
	 * @param role: For now: Admin = 1, Project Manager = 2, Project Member(user) = 3
	 */
	public User(String name, String login, String email, String password, int role) {
		this.name = name;
		this.login = login;
		this.email = email;
		this.password = password;
		this.roleId = role;
	}
	
	public User(int id, String name, String login, String email, String password, int role) {
		this.id = id;
		this.name = name;
		this.login = login;
		this.email = email;
		this.password = password;
		this.roleId = role;
	}
	
	public User(int id, String name, String login, String email, String password) {
		this.id = id;
		this.name = name;
		this.login = login;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the login
	 */
	
	
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTasks(List<Task> string) {
		this.tasks = string;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}

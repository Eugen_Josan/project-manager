package ca.concordia.comp354.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import ca.concordia.comp354.DAO.TaskDAO;
import ca.concordia.comp354.DAO.UserDAO;

/**
 * Task object class used to hold the data from database, and manage it more easily.
 * @author Eugeniu Josan
 * 
 */
public class Task {
	private static TaskDAO taskDAO = new TaskDAO ();
	private int	id;
	private String	description;
	private Date start_Date = new Date();
	private Date end_Date = new Date();
	private float time_Required;
	private float time_Spent;
	private int	status_Id;
	private int optimistic_duration;
	private int pessimistic_duration;
	private int mostLikely_duration;
	private float planned_value;
	private float actual_cost;
	private int projectId;
	private ArrayList<Integer> parentTasks;
	private static ArrayList<Task> tasks = TaskDAO.getAllTasks();
	
	private double z_value = 0;

	//Constructor used to create an object with data exported from DB
	public Task (int id, String description, Date start_Date, Date end_Date, float time_Required, 
			float time_Spent, int status_Id, int optimistic_duration, int pessimistic_duration,
			int mostLikely_duration, int projectId, ArrayList <Integer> parentTasks, 
			float planned_value, float actual_cost){
		
		this.id = id;
		this.description = description;
		this.start_Date = start_Date;
		this.end_Date = end_Date;
		this.time_Required = time_Required;
		this.time_Spent = time_Spent;
		this.status_Id = status_Id;
		this.optimistic_duration = optimistic_duration;
		this.pessimistic_duration = pessimistic_duration;
		this.mostLikely_duration = mostLikely_duration;
		this.projectId = projectId;
		this.parentTasks = parentTasks;
		this.planned_value = planned_value;
		this.actual_cost = actual_cost;
		
	}

	//Constructor used to create new object of type task for importing it into DB
	public Task (String description, Date start_Date, float time_Required, 
			float time_Spent, int status_Id, int optimistic_duration, int pessimistic_duration, 
			int mostLikely_duration, float planned_value, float actual_cost){

		this.description = description;
		this.start_Date = start_Date;
		this.end_Date = endDate(start_Date, time_Required);
		this.time_Required = time_Required;
		this.time_Spent = time_Spent;
		this.status_Id = status_Id;
		this.optimistic_duration = optimistic_duration;
		this.pessimistic_duration = pessimistic_duration;
		this.mostLikely_duration = mostLikely_duration;
		this.planned_value = planned_value;
		this.actual_cost = actual_cost;
	}
	
	public void addTaskToDb(int projectId) throws Exception{
		setId(TaskDAO.createTask(this, projectId));
		update();
	}
	
	private static void update(){
		tasks.clear();
		tasks = TaskDAO.getAllTasks();
	}
	/**
	 * This method will return a list of Task objects, and depending on key it can return the following list of tasks:
	 * key = 0 - returns all tasks from DB, the first variable is then not important so it could be any number.
	 * key = 1 - returns a list of all tasks of a specific project
	 * key = 2 - returns a list of all tasks assigned to the specific member
	 * @param value - can be either project id or user id
	 * @param key - selects the type of list to be returned
	 * @return the list of task objects
	 */
	public static ArrayList<Task> getAllTasksByCondition(int value, int key){
		if (key == 0) return tasks;
		else return TaskDAO.getListOfTasks(value, key);
	}

	public static ArrayList<Task>getAllTasks(){
		return tasks;
	}

	/**
	 * This method will calculate the end Date of a task by adding working days
	 * to start Date of a task
	 * @param start_Date the date object to which working days must be added
	 * @param time_Required the number of days to be added
	 * @return Date - the end_Date
	 */
	public static Date endDate(Date start_Date, float time_Required){
		Calendar cal = Calendar.getInstance();
	    cal.setTime(start_Date);
	    
	    if(time_Required <= 0){  //we don't need to do anything if time_required is 0!
	    	return cal.getTime();
	    }
	    
	    //adding required time to the start day; required represents the number of working days 
	    //including start and end date as well, so the interval is [0, days]
	    for (int i = 0; i != time_Required; ++i){
	    	if ((cal.get(Calendar.DAY_OF_WEEK)==1)||
	    		(cal.get(Calendar.DAY_OF_WEEK)==7)){			//check if it is Sunday and if yes increment by one more day the duration
	    		cal.add(Calendar.DATE, 1);						//skipping weekends
	    		--i;
	    	}else{	    	
	    		cal.add(Calendar.DATE, 1);						//adding one business day
	    	}
	    }
	    
	    return cal.getTime();
	}
	
	
	public int getId() {
		return id;
	}


	private void setId(int newId) {
		this.id = newId;
	}

	public float getAC() {
		return actual_cost;
	}


	public void setAC(float ac) {
		this.actual_cost = ac;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, 0, 0, 0, 0, 0, 0, ac);
	}

	public float getPV() {
		return planned_value;
	}


	public void setPV(float pv) {
		this.planned_value = pv;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, 0, 0, 0, 0, 0, pv, 0);
	}
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
		TaskDAO.updateTaskFromID(this.id, description, null, null, 0, 0, 0, 0, 0, 0, 0, 0);
	}


	public Date getStart_Date() {
		return start_Date;
	}


	public void setStart_Date(Date newStart_Date) throws ParseException {
		this.start_Date = newStart_Date;
	
		TaskDAO.updateTaskFromID(this.id, null, newStart_Date, null, 0, 0, 0, 0, 0, 0, 0, 0);
	}


	public Date getEnd_Date() {
		return end_Date;
	}

	
	public float getTime_Required() {
		return time_Required;
	}


	public void setTime_Required(float new_time_Required) {
		this.time_Required = new_time_Required;
		TaskDAO.updateTaskFromID(this.id, null, null, endDate(this.getStart_Date(),
				this.getTime_Required()), new_time_Required, 0, 0, 0, 0, 0, 0, 0);
		update();
	}
  

	public float getTime_Spent() {
		return time_Spent;
	}


	public void setTime_Spent(float time_Spent) {
		this.time_Spent = time_Spent;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, time_Spent, 0, 0, 0, 0, 0, 0);
	}


	public int getStatus_Id() {
		return status_Id;
	}


	public void setStatus_Id(int status_Id) {
		this.status_Id = status_Id;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, 0, status_Id, 0, 0, 0, 0, 0);

	}


	public int getOptimistic_duration() {
		return optimistic_duration;
	}


	public void setOptimistic_duration(int optimistic_duration) {
		this.optimistic_duration = optimistic_duration;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, 0, 0, optimistic_duration, 0, 0, 0, 0);
	}


	public int getPessimistic_duration() {
		return pessimistic_duration;
	}


	public void setPessimistic_duration(int pessimistic_duration) {
		this.pessimistic_duration = pessimistic_duration;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, 0, 0, 0, pessimistic_duration, 0, 0, 0);
	}

	public int getMostLikely_duration() {
		return mostLikely_duration;
	}


	public void setMostLikely_duration(int mostLikely_duration) {
		this.mostLikely_duration = mostLikely_duration;
		TaskDAO.updateTaskFromID(this.id, null, null, null, 0, 0, 0, 0, 0, mostLikely_duration, 0, 0);
	}
	
	
	
	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}

	/**
	 * @return the parentTask
	 */
	public ArrayList<Integer> getParentTask() {
		return this.parentTasks;
	}

	
	public void addParentTaskRelation(int parentId){
		if (this.parentTasks != null){
			this.parentTasks.add(parentId);
		}else{
			this.parentTasks = new ArrayList<Integer>();
			this.parentTasks.add(parentId);
		}
		
		taskDAO.assignParentTask(parentId, this.id);
		//tasks = TaskDAO.getAllTasks();
	}
	
	public void removeParentTask(int parentId){
		//removing relation from Task object or we can just re-assign the tasks variable
		for(int i = 0; i != tasks.size(); ++i){
			ArrayList<Integer> idTaskParents = tasks.get(i).getParentTask();
			for(int j = 0; j != idTaskParents.size(); ++j){
				if(parentId==idTaskParents.get(j)){
					idTaskParents.remove(j);
				}
			}
		}		
		
		//removing the relation from database
		TaskDAO.removeParentTask(this.id, parentId);
	}
	
	/**
	 * @return the task object
	 */
	
	public static Task getTaskById (int taskId){
		for (int i = 0; i!= tasks.size(); ++i){
			if (taskId == tasks.get(i).getId()){
				return tasks.get(i);
			}
		}
		return null;
	}
	
	public ArrayList<Task> getChild(){
		ArrayList<Task> list = taskDAO.getChildTasks(this.getId());
		return list;
	}
	
	/**
	 * @return the standard_deviation_attribute
	 */
	public double getStandard_deviation_attribute() {
		return ((double)(pessimistic_duration - optimistic_duration))/6;
	}

	/**
	 * @return the expected_time_attribute
	 */
	public double getExpected_time_attribute() {
		return ((double)(optimistic_duration + (4 * mostLikely_duration) + pessimistic_duration))/6;
	}

	
	/**
	 * @return the z_value
	 */
	public double getZ_value() {
		return z_value;
	}
	
	/**
	 * @param z_value
	 */
	public void setZ_value(double z_value) {
		this.z_value = z_value;
	}
	
}
